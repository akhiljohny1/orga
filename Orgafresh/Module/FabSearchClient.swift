//
//  Services.swift
//  GokulMoyaSample
//
//  Created by Innovation Incubator on 26/03/19.
//  Copyright © 2019 Innovation Incubator. All rights reserved.
//

import Foundation
import Moya
import Alamofire

enum ElasticApi {
    
    case getClothBrands(id            : Int)
    case postCalendarDatas(parameter  : Parameters)
    case clothLists(parameter         : Parameters)
    
    
    ///////////********************* ORGA  ****************************** ////////////////////
    
    case getappkey(parameter          : Parameters)
    case login(parameter              : Parameters)
    case register(parameter           : Parameters)
    case smsverification(phoneNumer   : Int, otp: Int)
    case smsverification2(parameter   : Parameters)
    case sociallogin(parameter        : Parameters)
    case gethome
    case getprodbycat(id              : Int)
    case getcategory
    
    case gethomecat(id : Int, type    : Int)
    case getcleanearn
    case getreferral

    case addfeedback(parameter        : Parameters)
    case addsugesstion(parameter      : Parameters)
    case sellOnOrgafresh(parameter      : Parameters)
    case otplogin(parameter           : Parameters)
    case saveaddress(parameter        : Parameters)
    case editemail(parameter        : Parameters)
    case editname(parameter        : Parameters)
    case editphonenumber(parameter        : Parameters)
    case getuserprofile(parameter        : Parameters)
    case placeclearnearnrequest(parameter        : Parameters)
    case getordrbyorderid(parameter        : Parameters)
    case payment(parameter        : Parameters)
    case placeorder(parameter        : Parameters)
    
    case registerdevice(parameter        : Parameters)

    case getalladdress
    case getallfaq
    case getallrecipe
    case getwallet
    case getalloffer
    case getallorder
    case offervalidate(offer        : Int)

    case getProducts

}

extension ElasticApi : TargetType {
    
    var commonURl: String {
        
        switch NetworkManager.environment {
            
        case .production   : return "http://orgafresh.online/api/"
        case .development  : return "http://test.orgafresh.online/api/"
            
        }
    }
    
    var ipBaseUrl: String {
        
        switch NetworkManager.environment {
            
        case .production     : return "http://127.0.0.1:8000/api/"
        case .development    : return "http://127.0.0.1:8000/api/"
            
        }
    }
    
    var baseURL: URL {
        
        switch self {
            
        case .getClothBrands:
            
            guard let url = URL(string: ipBaseUrl) else { fatalError("User Switch URL could not be configured.")}
            
            return url
            
        default:
            
            guard let url = URL(string: commonURl) else { fatalError("Common URL could not be configured.")}
            return url
        }
    }
    
    var path: String {
        
        switch  self {
            
        case .getClothBrands:
            return "/getClothBrand"
            
        case .postCalendarDatas:
            return "/saveCalendarData"
            
        case .clothLists:
            return "/getNewClothlist"
            
        case .getappkey:
            return "getappkey"
            
        case .register:
            return "register"
            
        case .login:
            return "login"
            
        case .smsverification(let phoneNumer, let otp):
            print("\(commonURl)smsverification?phone_number=\(phoneNumer)&otp=\(otp)")
            return "smsverification?phone_number=\(phoneNumer)&otp=\(otp)"
            
        case .smsverification2:
            return "smsverification"
            
        case .gethome:
            return "gethome"
            
        case .getallfaq:
            return "getfaq"
            
        case .getprodbycat(let id):
            return "getprodbycat/\(id)"
            
        case .gethomecat(let id, let type):
            return "gethomecat/\(id)/\(type)"
            
        case .getalladdress:
            return "getalladdress"
            
        case .getcategory:
            return "getcategory"
            
        case .getallrecipe:
            return "getallrecipe"
            
        case .getProducts:
            return "get-products"
            
        case .getcleanearn:
            return "cleanearnitems"
            
        case .getreferral:
            return "getreferral"
            
        case .sociallogin:
            return "sociallogin"
        case .addfeedback:
            return "addfeedback"
        case .addsugesstion:
            return "suggestproduct"
            
        case .sellOnOrgafresh:
            return "sellOnOrgafresh"
            
        case .otplogin:
            return "otplogin"
            
        case .saveaddress:
            return "saveaddress"
        case .editemail:
            return "editemail"
        case .editphonenumber:
            return "editphonenumber"
        
        case .getuserprofile:
            return "getuserprofile"
            
        case .editname:
            return "editname"
            
        case .placeclearnearnrequest:
            return "place-clearnearn-request"
            
        case .getordrbyorderid:
            return "getordrbyorderid"
            
        case .payment:
            return "paymentV2"
            
        case .placeorder:
            return "placeorder"
            
        case .registerdevice:
            return "registerdevice"
            
        case .offervalidate(let offer):
            return "offervalidate/\(offer)"
            
        case .getwallet:
            return "getwallet"
            
        case .getalloffer:
            return "getalloffer"
        case .getallorder:
            return "getallorder"
        }
        
    }
    
    var method: Moya.Method {
        
        switch self {
            
        case .clothLists, .postCalendarDatas, .login, .register, .getappkey, .smsverification, .sociallogin, .addfeedback,.otplogin, .addsugesstion,.sellOnOrgafresh, .smsverification2, .saveaddress,.editemail, .placeclearnearnrequest,.getreferral, .editname, .editphonenumber, .getuserprofile, .getalloffer, .getallorder, .getordrbyorderid, .payment, .placeorder, .registerdevice:
            
            return .post
        default:
            return .get
        }
    }
    
    var sampleData: Data {
        
        return Data()
    }
    
    var task: Task {
        
        switch self {
            
        case .getClothBrands(let id):
            return .requestParameters(parameters: ["user_id" : id], encoding: URLEncoding.queryString)
            
        case .postCalendarDatas(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .clothLists(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .login(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .register(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .smsverification2(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .getappkey(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .smsverification://(let parameter):
            return .requestPlain//.requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .gethome:
            return .requestParameters(parameters: [:], encoding: URLEncoding.queryString)
   
            
        case .sociallogin(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .addfeedback(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .addsugesstion(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .sellOnOrgafresh(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .otplogin(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .saveaddress(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .editemail(let parameter):
                     return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .editphonenumber(let parameter):
                    return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .getuserprofile(let parameter):
                    return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .editname(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .placeclearnearnrequest(let parameter):
                    return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .getordrbyorderid(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .offervalidate:
            return .requestPlain
            
        case .payment(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .placeorder(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        
        case .registerdevice(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        
        case .getprodbycat:
            return .requestPlain
            
        case .gethomecat:
            return .requestPlain
        
        case .getalladdress:
            return .requestPlain
            
        case .getcategory:
            return .requestPlain
        
        case .getallrecipe:
            return .requestPlain
            
        case .getProducts:
            return .requestPlain
            
        case .getwallet:
            return .requestPlain
        
        case .getcleanearn:
            return .requestPlain
            
        case .getreferral:
            return .requestPlain
            
        case .getallfaq:
            return .requestPlain
         
        case .getalloffer:
            return .requestPlain
            
        case .getallorder:
            return .requestPlain
            
        }
    }
    
    var headers: [String : String]? {
        
        switch self {
            
            
        case .clothLists, .register, .login, .smsverification, .gethome, .getprodbycat, .gethomecat, .getcategory, .getcleanearn,.sociallogin,  .addfeedback, .addsugesstion,.sellOnOrgafresh, .otplogin, .smsverification2, .saveaddress, .getalladdress,.getallfaq, .getallrecipe, .getProducts, .editemail, .getwallet, .placeclearnearnrequest,.getreferral, .editname, .editphonenumber, .getuserprofile, .getalloffer, .getallorder, .getordrbyorderid, .offervalidate, .payment, .placeorder, .registerdevice:
            
            let token = UserDefaults.standard.string(forKey: "access_token") ?? "not saved"
            
            //            print("Header token : \(token)")
            if( token != ""){
                var httpheader = [String:String]()
                
                httpheader = ["content-type":"application/json",
                              "Accept" : "application/json","Authorization" : token]
                
                return httpheader
            }
            else
            {
                let header: [String : String] = ["content-type":"application/json",
                                                 "Accept" : "application/json","Authorization" : token]
                return header
            }
            
        default:
            let header: [String : String] = ["Content-Type":"application/json",
                                             "device" : "ios"]
            return header
        }
    }
    
}

