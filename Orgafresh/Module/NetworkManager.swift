//
//  ApiManager.swift
//  GokulMoyaSample
//
//  Created by Innovation Incubator on 26/03/19.
//  Copyright © 2019 Innovation Incubator. All rights reserved.
//

import Foundation
import Moya

enum APIEnvironment {
    
    case production
    case development
}

struct NetworkManager {

    static let environment: APIEnvironment = .development

}
    
