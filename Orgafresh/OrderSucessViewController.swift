//
//  OrderSucessViewController.swift
//  Orgafresh
//
//  Copyright © 2020 Innovation Incubator. All rights reserved.
//

import UIKit

class OrderSucessViewController: UIViewController {
    
    @IBOutlet weak var orderIDLabel: UILabel!
    var place_orderid = Int()
    override func viewDidLoad() {
        super.viewDidLoad()
        orderIDLabel.text = "Your Order has been Placed Successfully. And is being Processed. Order ID : \(place_orderid)"
        // Do any additional setup after loading the view.
    }
    

    @IBAction func OkAction(_ sender: Any) {
        let storyBoard          = UIStoryboard(name: "Main", bundle: nil)
               let HomeVC = storyBoard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
               self.navigationController?.pushViewController(HomeVC, animated: false)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
