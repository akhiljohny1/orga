//
//  MoreRecipeViewController.swift
//  Orgafresh
//
//  Created by Innovation Incubator on 28/09/20.
//  Copyright © 2020 Innovation Incubator. All rights reserved.
//

import UIKit
import Alamofire
import Moya
import SwiftyJSON
import SVProgressHUD
import SDWebImage

class MoreRecipeViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    
    @IBOutlet weak var collVw             : UICollectionView!
    
    var recipeVals                         = JSON()
    
    @IBOutlet weak var gradientVwTopConstrain    : NSLayoutConstraint!
    @IBOutlet weak var gradientVwHeightConstrain : NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()

        if DeviceType.IS_IPHONE_X || DeviceType.IS_IPHONE_XP{
            
            gradientVwTopConstrain.constant = 0
            gradientVwHeightConstrain.constant = 55
            
        }else{
            
            gradientVwTopConstrain.constant = 0
            gradientVwHeightConstrain.constant = 55
        }
        
        callGetRecipe()
        
        // Do any additional setup after loading the view.
    }

    //MARK: - Call Get Home
    
    func callGetRecipe() {
        
        SVProgressHUD.show(withStatus: "Loading...")
        
        networkProvider.request(.getallrecipe, completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                SVProgressHUD.dismiss()
                
                let jsonVal = JSON(response.data)
                
                print("Recipe vals : \(jsonVal)")
                
                if response.statusCode == 200{
                    
                    if jsonVal["status"].stringValue == "success"{
                        
                        self.recipeVals = jsonVal["recipe"]
                        
                    }else{
                        
                        GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                    }
                }else if response.statusCode == 401{
                    
                    GlobalClass.callGetApiKey(viewController: self)
                    GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                    
                }else if jsonVal["message"].stringValue.contains("token"){
                    
                    GlobalClass.callGetApiKey(viewController: self)
                }
                else if response.statusCode == 500{
                    
                    GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                }
                else{
                    
                    GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                }
                
                DispatchQueue.main.async {
                                        
                    self.collVw.reloadData()
                    
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                    
                    self.collVw.reloadData()
                })
                
            case .failure( _):
                
                GlobalClass.showAlertDialog(message: "Please try again.", target: self)
                
                SVProgressHUD.dismiss()
                
                break
            }
        })
    }

    //MARK: - Collection View
       
       func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
           
           return recipeVals.count
       }
       
       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
           
           
           let cell3 = collVw.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
           
           let lblName = cell3.viewWithTag(1) as! UILabel
           let image   = cell3.viewWithTag(2) as! UIImageView
           
           lblName.text = recipeVals[indexPath.row]["title"].stringValue.firstUppercased
           
           image.sd_setImage(with: URL(string: imageBaseUrl + recipeVals[indexPath.row]["image"].stringValue), placeholderImage: #imageLiteral(resourceName: "loginVector"))
           
           return cell3
       }
       
       func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
           
           let storyBoard          = UIStoryboard(name: "Main", bundle: nil)
           let SelectedCategoryVC  = storyBoard.instantiateViewController(withIdentifier: "RecipeDetailViewController") as! RecipeDetailViewController
           SelectedCategoryVC.values = recipeVals[indexPath.row]
           self.navigationController?.pushViewController(SelectedCategoryVC, animated: true)
           
       }
       
       func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,sizeForItemAt indexPath: IndexPath) -> CGSize {
           
           let numberOfCellInRow : Int        = 2
           let padding : Int                  = 2
           let collectionCellWidth : CGFloat  = (self.collVw.frame.size.width/CGFloat(numberOfCellInRow)) - CGFloat(padding)
           
           return CGSize(width: collectionCellWidth, height: collectionCellWidth)
       }
    
    //MARK: - Notification Action
    
    @IBAction func notificationAction(_ sender: AnyObject){
        
        let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
        let NotificationViewController  = storyBoard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(NotificationViewController, animated: true)
    }
    
    @IBAction func MyCartAction(_ sender: Any) {
        if CartCount == "0"
                  {
                   GlobalClass.showAlertDialog(message: "You dont have any item on your cart", target: self)

               }
               else{
        let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
        let MyOrderViewController  = storyBoard.instantiateViewController(withIdentifier: "CartListViewController") as! CartListViewController
        self.navigationController?.pushViewController(MyOrderViewController, animated: true)
        }
    }
       
       //MARK: - Button Action
       
       @IBAction func backAction(_ sender: Any) {
           
           self.navigationController?.popViewController(animated: true)
       }

}
