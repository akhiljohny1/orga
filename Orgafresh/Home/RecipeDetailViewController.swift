//
//  RecipeDetailViewController.swift
//  Orgafresh
//
//  Created by Innovation Incubator on 24/09/20.
//  Copyright © 2020 Innovation Incubator. All rights reserved.
//

import UIKit
import SwiftyJSON

class RecipeDetailViewController: UIViewController {

    @IBOutlet weak var titleLbl             : UILabel!
    @IBOutlet weak var headingLbl           : UILabel!
    
    @IBOutlet weak var txtVw                : UITextView!
    
    @IBOutlet weak var imgVw                : UIImageView!
    
    var values                              = JSON()
    
    @IBOutlet weak var gradientVwTopConstrain    : NSLayoutConstraint!
    @IBOutlet weak var gradientVwHeightConstrain : NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if DeviceType.IS_IPHONE_X || DeviceType.IS_IPHONE_XP{
            
            gradientVwTopConstrain.constant = 40
            gradientVwHeightConstrain.constant = 55
            
        }else{
            
            gradientVwTopConstrain.constant = 20
            gradientVwHeightConstrain.constant = 55
        }
        
        titleLbl.text     = values["title"].stringValue.firstUppercased
        headingLbl.text   = values["title"].stringValue.firstUppercased
        
        txtVw.text        = values["description"].stringValue.firstUppercased

        imgVw.sd_setImage(with: URL(string: imageBaseUrl + values["image"].stringValue), placeholderImage: #imageLiteral(resourceName: "loginVector"))
        
        
        // Do any additional setup after loading the view.
    }
    
    //MARK: - Notification Action
    
    @IBAction func notificationAction(_ sender: AnyObject){
        
        let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
        let NotificationViewController  = storyBoard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(NotificationViewController, animated: true)
    }
    
    @IBAction func MyCartAction(_ sender: Any) {
        if CartCount == "0"
                  {
                   GlobalClass.showAlertDialog(message: "You dont have any item on your cart", target: self)

               }
               else{
        let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
        let MyOrderViewController  = storyBoard.instantiateViewController(withIdentifier: "CartListViewController") as! CartListViewController
        self.navigationController?.pushViewController(MyOrderViewController, animated: true)
        }
    }
    
    @IBAction func backButton(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
