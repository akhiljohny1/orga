//
//  HomeViewController.swift
//  Orgafresh
//
//  Created by Innovation Incubator on 01/08/20.
//  Copyright © 2020 Innovation Incubator. All rights reserved.
//

import UIKit
import Alamofire
import Moya
import SwiftyJSON
import SVProgressHUD
import SDWebImage
import AVFoundation
import AVKit
import YoutubePlayer_in_WKWebView
import CoreLocation
import FBSDKLoginKit
import Kingfisher
//import youtube_ios_player_helper

var CartCount                     = "0"

class HomeCollectionCell: UICollectionViewCell{
    
    
    @IBOutlet weak var whiteView            : UIView!
    @IBOutlet weak var greenView            : UIView!
    
    @IBOutlet weak var imgView              : ScaledHeightImageView!
    @IBOutlet weak var nameLbl              : UILabel!
    @IBOutlet weak var availabilityLabel: UILabel!
    @IBOutlet weak var burView: UIView!
    @IBOutlet weak var sellingpriceLabel: UILabel!
    
    @IBOutlet weak var unitgramsLabel: UILabel!
    @IBOutlet weak var normalPriceLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        greenView.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 20)
        greenView.clipsToBounds = true
    }
    
}

class VideoCollectionCell: UICollectionViewCell{
    
    @IBOutlet weak var videoView            : WKYTPlayerView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
}

class HomeViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate,UITableViewDataSource,UITableViewDelegate, CLLocationManagerDelegate {
    @IBOutlet weak var mainMenuTableView: UITableView!
    
    @IBOutlet weak var CartButton: SSBadgeButton!
    @IBOutlet weak var topView               : GradientView!
    @IBOutlet weak var collVw                : UICollectionView!
    @IBOutlet weak var collVwHeight          : NSLayoutConstraint!
    
    @IBOutlet weak var bannerCollVw          : UICollectionView!
    
    @IBOutlet weak var recipeCollVw          : UICollectionView!
    @IBOutlet weak var recipeCollHeight      : NSLayoutConstraint!
    
    @IBOutlet weak var productCollVw         : UICollectionView!
    @IBOutlet weak var productCollHeight     : NSLayoutConstraint!
    
    @IBOutlet weak var videoCollVw           : UICollectionView!
    @IBOutlet weak var videosCollHeight      : NSLayoutConstraint!
    
    @IBOutlet weak var backVwHeight          : NSLayoutConstraint!
    
    @IBOutlet weak var menuButtTop           : NSLayoutConstraint!
    
    var homeValues                           = JSON()
    
    @IBOutlet weak var menuTransparentView   : UIView!
    @IBOutlet weak var menuBlackVw           : UIView!
    @IBOutlet weak var menuBlackVwWidth      : NSLayoutConstraint!
    
    @IBOutlet weak var nameLbl               : UILabel!
    @IBOutlet weak var loginTimeLbl          : UILabel!
    
    @IBOutlet weak var commentsTblHeight     : NSLayoutConstraint!
    
    var locationManager                      : CLLocationManager!
    
    var menuArr                              = ["Login", "Home", "Recycle Bank","Refer and Earn", "My Balance","My Order", "My Favorite", "Join Our Social Media", "Suggest A Product", "Our Vision","Help/Feedback","FAQ","Contact Us","About Us"]
    
    @IBOutlet weak var commentTbl            : UITableView!
    
    var menuImgArr                           = [#imageLiteral(resourceName: "login"),#imageLiteral(resourceName: "home"),#imageLiteral(resourceName: "clean"),#imageLiteral(resourceName: "refer"),#imageLiteral(resourceName: "balance"),#imageLiteral(resourceName: "cart"),#imageLiteral(resourceName: "favorite"),#imageLiteral(resourceName: "join"),#imageLiteral(resourceName: "prodSuggest"),#imageLiteral(resourceName: "vision"),#imageLiteral(resourceName: "help"),#imageLiteral(resourceName: "help"),#imageLiteral(resourceName: "help"),#imageLiteral(resourceName: "help")]
    
    
    @IBOutlet weak var placeTxtField         : UITextField!
    @IBOutlet weak var commentHightView: UIView!
    @IBOutlet weak var coomentGrayVwWidth: NSLayoutConstraint!
    
    @IBOutlet weak var commentImageHLImage: UIImageView!
    @IBOutlet weak var nameHLLabel: UILabel!
    @IBOutlet weak var ratingHLImage: UIImageView!
    @IBOutlet weak var commentHLLabel: UILabel!
    
    @IBOutlet weak var ratingLbl: UILabel!
    
    @IBOutlet weak var sellImageVw: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("menu count", menuArr.count)
        print("img count", menuImgArr.count)
        
//        SVProgressHUD.
        
        loginArrayFunc()
        DispatchQueue.main.async {
            
            self.topView.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 15)
            self.topView.clipsToBounds = true
        }
        
        if DeviceType.IS_IPHONE_X || DeviceType.IS_IPHONE_XP{
            
            menuButtTop.constant = 50
        }
        
        menuBlackVw.roundCorners(corners: [.topRight, .bottomRight], radius: 50)
        menuBlackVw.clipsToBounds = true
        
        menuBlackVwWidth.constant = 0
        menuTransparentView.isHidden = true
        
        let leftView1                          = UIView(frame: CGRect(x: 5, y: 0, width: 15, height: 26))
        leftView1.backgroundColor              = .clear
        placeTxtField.leftView                 = leftView1
        placeTxtField.leftViewMode             = .always
        placeTxtField.contentVerticalAlignment = .center
        
        loginTimeLbl.text                      = appDelegate.loginTime
        
        nameLbl.text                           = "Hello, User"
        
        if appDelegate.isLogined == true{
            
            callGetHome()
        }else{
            
            callGetApiKey()
            registerDevice()
        }
        
        //        callGetHome()
        // Do any additional setup after loading the view.
    }
    func loginArrayFunc(){
        if appDelegate.isLogined == true{
            
            menuArr = ["Logout", "Home", "Recycle Bank","Refer and Earn", "My Balance","My Order", "My Favorite", "Join Our Social Media", "Suggest A Product", "Our Vision","Help/Feedback","FAQ","Contact Us","About Us","My Profile"]
            
            menuImgArr = [#imageLiteral(resourceName: "logout"),#imageLiteral(resourceName: "home"),#imageLiteral(resourceName: "clean"),#imageLiteral(resourceName: "refer"),#imageLiteral(resourceName: "balance"),#imageLiteral(resourceName: "cart"),#imageLiteral(resourceName: "favorite"),#imageLiteral(resourceName: "join"),#imageLiteral(resourceName: "prodSuggest"),#imageLiteral(resourceName: "vision"),#imageLiteral(resourceName: "help"),#imageLiteral(resourceName: "help"),#imageLiteral(resourceName: "help"),#imageLiteral(resourceName: "help"),#imageLiteral(resourceName: "help")]
            
        }else{
            
            menuArr = ["Login", "Home", "Recycle Bank","Refer and Earn", "My Balance","My Order", "My Favorite", "Join Our Social Media", "Suggest A Product", "Our Vision","Help/Feedback","FAQ","Contact Us","About Us"]
            
            menuImgArr = [#imageLiteral(resourceName: "login"),#imageLiteral(resourceName: "home"),#imageLiteral(resourceName: "clean"),#imageLiteral(resourceName: "refer"),#imageLiteral(resourceName: "balance"),#imageLiteral(resourceName: "cart"),#imageLiteral(resourceName: "favorite"),#imageLiteral(resourceName: "join"),#imageLiteral(resourceName: "prodSuggest"),#imageLiteral(resourceName: "vision"),#imageLiteral(resourceName: "help"),#imageLiteral(resourceName: "help"),#imageLiteral(resourceName: "help"),#imageLiteral(resourceName: "help")]
        }
        self.mainMenuTableView.reloadData()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.commentHightView.isHidden = true
        
        if CartCount == "0"
        {
            CartButton.badge = ""
            
            CartButton.badgeLabel.backgroundColor = UIColor.clear
        }
        else
        {
            CartButton.badge = "\(CartCount)"
        }
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestLocation()
        
        if CLLocationManager.locationServicesEnabled(){
            locationManager.startUpdatingLocation()
        }else{
            
            locationManager.requestLocation()
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3, execute: {
            
            if CLLocationManager.locationServicesEnabled(){
                self.locationManager.startUpdatingLocation()
            }else{
                
                self.locationManager.requestLocation()
            }
        })
        loginArrayFunc()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        SVProgressHUD.dismiss()
        
    }
    
    
    //MARK: - Get Api Key
    
    func callGetApiKey(){
        
        if Connectivity.isConnectedToInternet {
             print("Connected")
         } else {
             print("No Internet")
            
            GlobalClass.showAlertDialog(message: "Please check your internet connection and try again.", target: self)
            
            return
        }
        
        SVProgressHUD.show(withStatus: "Loading...")
        
        let parameter: Parameters = ["grant_type" : "client_credentials", "client_id" : 1, "client_secret" : "tKhCdAFdtA6JNObaomrk2gzXgQ0Q9N2MiLQh2Ua2"]
        
        // print("noti params : \(parameter)")
        
        networkProvider.request(.getappkey(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                let jsonVal = JSON(response.data)
                
                //   print("Json val : \(jsonVal)")
                
                // print(jsonVal["status"].stringValue)
                
                if jsonVal["status"].stringValue == "success"{
                    
                    appDelegate.access_token = jsonVal["token"].stringValue
                    
                    UserDefaults.standard.setValue(jsonVal["token"].stringValue, forKey: "access_token")
                    
                    UserDefaults.standard.synchronize()
                    
                    self.callGetHome()
                    
                }
                
            case .failure( _):
                
                break
            }
        })
    }
    
    //MARK: - Register Device
    
    func registerDevice(){
        
        let parameter: Parameters = ["device" : "ios", "deviceid" : UserDefaults.standard.string(forKey: "PushToken") ?? "user rejected"]
        
        // print("noti params : \(parameter)")
        
        networkProvider.request(.registerdevice(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                let jsonVal = JSON(response.data)
                
                print("Register val : \(jsonVal)")
                
            case .failure( _):
                
                break
            }
        })
    }
    
    
    //MARK: - Call Get Home
    
    func callGetHome() {
        
        if Connectivity.isConnectedToInternet {
             print("Connected")
         } else {
             print("No Internet")
            
            GlobalClass.showAlertDialog(message: "Please check your internet connection and try again.", target: self)
            
            return
        }
        
        SVProgressHUD.show(withStatus: "Loading...")
        
        networkProvider.request(.gethome, completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                SVProgressHUD.dismiss()
                
                let jsonVal = JSON(response.data)
                
                //   print("Home vals : \(jsonVal)")
                //
                //                print("Home testimonial : \(jsonVal["testimonial"][1]["comments"].stringValue)")
                
                if response.statusCode == 200{
                    
                    if jsonVal["status"].stringValue == "success"{
                        
                        self.homeValues = jsonVal
                        
                    }else{
                        
                        GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                    }
                }else if response.statusCode == 401{
                    
                    GlobalClass.callGetApiKey(viewController: self)
                    GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                    
                }else if jsonVal["message"].stringValue.contains("token"){
                    
                    GlobalClass.callGetApiKey(viewController: self)
                }
                else if response.statusCode == 500{
                    
                    GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                }
                else{
                    
                    GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                }
                
                DispatchQueue.main.async {
                    
                    self.ratingLbl.text = "\(self.homeValues["testimonial"].count) Customer ratings"
                    
                    self.bannerCollVw.reloadData()
                    
                    self.collVw.reloadData()
                    
                    self.commentTbl.reloadData()
                    
                    self.recipeCollVw.reloadData()
                    
                    self.videoCollVw.reloadData()
                    
                    self.productCollVw.reloadData()
                    
                    self.commentsTblHeight.constant = CGFloat(self.homeValues["testimonial"].count * 100)
                    
                    let newHeight = self.collVw.collectionViewLayout.collectionViewContentSize.height
                    
                    let recipeHeight = self.recipeCollVw.collectionViewLayout.collectionViewContentSize.height
                    
                    let videoHeight = self.videoCollVw.collectionViewLayout.collectionViewContentSize.height
                    
                    let productHeight = self.productCollVw.collectionViewLayout.collectionViewContentSize.height
                    
                    //       print("self.homeValues :", self.homeValues)
                    
                    //    print("New height : \(newHeight)")
                    
                    self.collVwHeight.constant = newHeight + 250
                    
                    self.recipeCollHeight.constant  = recipeHeight
                    
                    self.videosCollHeight.constant  = videoHeight
                    
                    self.productCollHeight.constant = productHeight
                    
                    let heightAll = newHeight + 540 + self.commentsTblHeight.constant + recipeHeight
                    
                    self.backVwHeight.constant = heightAll + videoHeight + productHeight + 250
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                    
                    self.collVw.reloadData()
                    
                    self.sellImageVw.clipsToBounds = true
                    
                    self.sellImageVw.layer.cornerRadius = 10
                })
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                    
                    self.productCollVw.reloadData()
                    self.startTimer()
                    
                })
                
            case .failure( _):
                
                GlobalClass.showAlertDialog(message: "Please try again.", target: self)
                
                SVProgressHUD.dismiss()
                
                break
            }
        })
    }
    func startTimer() {
        
        _ =  Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(self.scrollAutomatically), userInfo: nil, repeats: true)
    }
    
    
    @objc func scrollAutomatically(_ timer1: Timer) {
        
        if let coll  = bannerCollVw {
            for cell in coll.visibleCells {
                let indexPath: IndexPath? = coll.indexPath(for: cell)
                if ((indexPath?.row)! < homeValues["banner"].count - 1){
                    let indexPath1: IndexPath?
                    indexPath1 = IndexPath.init(row: (indexPath?.row)! + 1, section: (indexPath?.section)!)
                    
                    coll.scrollToItem(at: indexPath1!, at: .right, animated: true)
                }
                else{
                    let indexPath1: IndexPath?
                    indexPath1 = IndexPath.init(row: 0, section: (indexPath?.section)!)
                    coll.scrollToItem(at: indexPath1!, at: .left, animated: true)
                }
                
            }
        }
    }
    
    //MARK: - Collection View
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == bannerCollVw{
            
            return homeValues["banner"].count
        }
        
        if collectionView == productCollVw{
            
            return homeValues["products"].count
        }
        
        if collectionView == recipeCollVw{
            
            return homeValues["recipe"].count
        }
        
        if collectionView == videoCollVw{
            
            return homeValues["videoadvt"].count
        }
        
        return homeValues["homecategories"].count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == bannerCollVw{
            
            let cell2 = bannerCollVw.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
            
            let imgVw = cell2.viewWithTag(1) as! UIImageView
            
            //imgVw.sd_setImage(with: URL(string: imageBaseUrl + homeValues["banner"][indexPath.row]["img_name"].stringValue), placeholderImage: #imageLiteral(resourceName: "loginVector"))
            
            let str = imageBaseUrl + homeValues["banner"][indexPath.row]["img_name"].stringValue
            
            let size = imgVw.bounds.size
            let processor = ResizingImageProcessor(referenceSize: size, mode: .aspectFill)
            imgVw.kf.setImage(with: URL(string: str), options: [.processor(processor), .scaleFactor(UIScreen.main.scale)])
            
            return cell2
        }
        
        if collectionView == recipeCollVw{
            
            let cell3 = recipeCollVw.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
            
            let lblName = cell3.viewWithTag(1) as! UILabel
            let image   = cell3.viewWithTag(2) as! UIImageView
            
            lblName.text = homeValues["recipe"][indexPath.row]["title"].stringValue.firstUppercased
            
            var str = imageBaseUrl + homeValues["recipe"][indexPath.row]["image"].stringValue
            
            str = str.replacingOccurrences(of: " ", with: "%20")
            
            image.sd_setImage(with: URL(string: str), placeholderImage: #imageLiteral(resourceName: "loginVector"))
            
            return cell3
        }
        
        if collectionView == videoCollVw{
            
            let cell3 = videoCollVw.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! VideoCollectionCell
            
            let lblName = cell3.viewWithTag(1) as! UILabel
            //            let image   = cell3.viewWithTag(z2) as! UIImageView
            
            lblName.text = homeValues["videoadvt"][indexPath.row]["title"].stringValue.firstUppercased
            
            cell3.videoView.load(withVideoId: homeValues["videoadvt"][indexPath.row]["video_url"].stringValue.components(separatedBy: "=").last!)
            
            
            //   print("Video urls : ", homeValues["videoadvt"][indexPath.row]["video_url"].stringValue)
            
            return cell3
        }
        
        if collectionView == productCollVw{
            
            let cell = productCollVw.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! HomeCollectionCell
            
            cell.greenView.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 5)
            cell.greenView.clipsToBounds = true
            
            cell.nameLbl.text = homeValues["products"][indexPath.row]["product_name"].stringValue.firstUppercased
            cell.normalPriceLabel.text = "₹" + "\(homeValues["products"][indexPath.row]["normal_price"].intValue)"
            cell.sellingpriceLabel.text = "₹" + "\(homeValues["products"][indexPath.row]["selling_price"].intValue)"
            cell.normalPriceLabel.attributedText = cell.normalPriceLabel.text!.strikeThrough()
            cell.unitgramsLabel.text =  "/" + "\(homeValues["products"][indexPath.row]["minimum_order_quantity"].intValue)" + " " + homeValues["products"][indexPath.row]["unit_name"].stringValue
            
            
            var str = imageBaseUrl + homeValues["products"][indexPath.row]["product_img"].stringValue
            
            str = str.replacingOccurrences(of: " ", with: "%20")
            
            cell.imgView.sd_setImage(with: URL(string: str), placeholderImage: #imageLiteral(resourceName: "loginVector"))
            
            if homeValues["products"][indexPath.row]["availability"].intValue == 0
            {
                cell.availabilityLabel.isHidden = false
                cell.burView.isHidden = false
            }
            else{
                cell.availabilityLabel.isHidden = true
                cell.burView.isHidden = true
            }
            
            return cell
        }
        
        let cell = collVw.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! HomeCollectionCell
        
        cell.greenView.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 5)
        cell.greenView.clipsToBounds = true
        
        cell.nameLbl.text = homeValues["homecategories"][indexPath.row]["title"].stringValue.firstUppercased
        
        var str = imageBaseUrl + homeValues["homecategories"][indexPath.row]["image"].stringValue
        
        str = str.replacingOccurrences(of: " ", with: "%20")
        
        cell.imgView.sd_setImage(with: URL(string: str), placeholderImage: #imageLiteral(resourceName: "loginVector"))
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        //  print(indexPath.row)
        
        if collectionView == bannerCollVw{
            
        }else if collectionView == recipeCollVw{
            
            let storyBoard          = UIStoryboard(name: "Main", bundle: nil)
            let SelectedCategoryVC  = storyBoard.instantiateViewController(withIdentifier: "RecipeDetailViewController") as! RecipeDetailViewController
            SelectedCategoryVC.values = homeValues["recipe"][indexPath.row]
            self.navigationController?.pushViewController(SelectedCategoryVC, animated: true)
            
        }else if collectionView == videoCollVw{
            
            //  print("video url ", homeValues["videoadvt"][indexPath.row]["video_url"].stringValue)
            
            //            let storyBoard          = UIStoryboard(name: "Main", bundle: nil)
            //            let SelectedCategoryVC  = storyBoard.instantiateViewController(withIdentifier: "VideoPlayerViewController") as! VideoPlayerViewController
            //            SelectedCategoryVC.str = homeValues["videoadvt"][indexPath.row]["video_url"].stringValue.components(separatedBy: "embed/").last!
            //            self.present(SelectedCategoryVC, animated: true, completion: nil)
            
        }else if collectionView == productCollVw{
            
            if homeValues["products"][indexPath.row]["availability"].intValue != 0
            {
                let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
                let ProductDetailsVC  = storyBoard.instantiateViewController(withIdentifier: "ProductDetailsViewController") as! ProductDetailsViewController
                
                ProductDetailsVC.productValues = homeValues["products"][indexPath.row]
                self.navigationController?.pushViewController(ProductDetailsVC, animated: true)
            }
            else{
                GlobalClass.showAlertDialog(message: "Product is out of stock", target: self)
            }
        }
        else{
            
            let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
            let SelectedCategoryVC  = storyBoard.instantiateViewController(withIdentifier: "SelectedCategoryViewController") as! SelectedCategoryViewController
            SelectedCategoryVC.values = homeValues["homecategories"][indexPath.row]
            self.navigationController?.pushViewController(SelectedCategoryVC, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == bannerCollVw{
            
            return CGSize(width: self.bannerCollVw.frame.width, height: self.bannerCollVw.frame.height)
        }
        
        if collectionView == recipeCollVw{
            
            let numberOfCellInRow : Int        = 2
            let padding : Int                  = 5
            let collectionCellWidth : CGFloat  = (self.recipeCollVw.frame.size.width/CGFloat(numberOfCellInRow)) - CGFloat(padding)
            
            return CGSize(width: collectionCellWidth, height: collectionCellWidth + 10)
            
        }
        
        if collectionView == videoCollVw{
            
            let numberOfCellInRow : Int        = 2
            let padding : Int                  = 5
            let collectionCellWidth : CGFloat  = (self.videoCollVw.frame.size.width/CGFloat(numberOfCellInRow)) - CGFloat(padding)
            
            return CGSize(width: collectionCellWidth, height: collectionCellWidth + 10)
            
        }
        
        if collectionView == productCollVw{
            
            let numberOfCellInRow : Int        = 2
            let padding : Int                  = 5
            let collectionCellWidth : CGFloat  = (self.productCollVw.frame.size.width/CGFloat(numberOfCellInRow)) - CGFloat(padding)
            
            return CGSize(width: collectionCellWidth, height: collectionCellWidth + 10)
            
        }
        
        let numberOfCellInRow : Int        = 2
        let padding : Int                  = 5
        let collectionCellWidth : CGFloat  = (self.collVw.frame.size.width/CGFloat(numberOfCellInRow)) - CGFloat(padding)
        
        return CGSize(width: collectionCellWidth, height: collectionCellWidth + 10)
    }
    
    
    //MARK: - Table View
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == commentTbl{
            
            return homeValues["testimonial"].count
        }
        
        return menuArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell       = tableView.dequeueReusableCell(withIdentifier: "cell")
        
        if tableView == commentTbl{
            
            let cell1       = commentTbl.dequeueReusableCell(withIdentifier: "cell")
            
            let lbl2       = cell1?.viewWithTag(2) as! UILabel
            let lbl3       = cell1?.viewWithTag(3) as! UILabel
            let img        = cell1?.viewWithTag(1) as! UIImageView
            let rating     = cell1?.viewWithTag(4) as! UIImageView
            
            lbl3.text      = homeValues["testimonial"][indexPath.row]["comments"].stringValue.firstUppercased
            lbl2.text      = homeValues["testimonial"][indexPath.row]["name"].stringValue.firstUppercased
            
            var str = imageBaseUrl + homeValues["testimonial"][indexPath.row]["image"].stringValue
            str = str.replacingOccurrences(of: " ", with: "%20")
            img.sd_setImage(with: URL(string: str), placeholderImage: #imageLiteral(resourceName: "loginVector"))
            
            //   print("Rating : ", homeValues["testimonial"][indexPath.row]["rating"].stringValue)
            
            let rat = homeValues["testimonial"][indexPath.row]["rating"].stringValue
            
            if rat == "2"{
                
                rating.image = UIImage(named: "rating_2")
                
            }else if rat.contains("2."){
                
                rating.image = UIImage(named: "rating_2.5")
                
            }else if rat == "3"{
                
                rating.image = UIImage(named: "rating_3")
                
            }else if rat.contains("3."){
                
                rating.image = UIImage(named: "rating_3.5")
                
            }else if rat == "4"{
                
                rating.image = UIImage(named: "rating_4")
                
            }else if rat.contains("4."){
                
                rating.image = UIImage(named: "rating_4.5")
                
            }else if rat == "5"{
                
                rating.image = UIImage(named: "rating_5")
            }
            
            
            return cell1!
            
        }else{
            
            let lbl        = cell?.viewWithTag(1) as! UILabel
            let img        = cell?.viewWithTag(2) as! UIImageView
            
            lbl.text       = menuArr[indexPath.row]
            img.image      = menuImgArr[indexPath.row]
        }
        
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == commentTbl{
            self.commentHightView.isHidden = false
            commentHLLabel.text      = homeValues["testimonial"][indexPath.row]["comments"].stringValue.firstUppercased
            nameHLLabel.text      = homeValues["testimonial"][indexPath.row]["name"].stringValue.firstUppercased
            var str = imageBaseUrl + homeValues["testimonial"][indexPath.row]["image"].stringValue
            str = str.replacingOccurrences(of: " ", with: "%20")
            commentImageHLImage.sd_setImage(with: URL(string: str), placeholderImage: #imageLiteral(resourceName: "loginVector"))
            let rat = homeValues["testimonial"][indexPath.row]["rating"].stringValue
            
            if rat == "2"{
                
                ratingHLImage.image = UIImage(named: "rating_2")
                
            }else if rat.contains("2."){
                
                ratingHLImage.image = UIImage(named: "rating_2.5")
                
            }else if rat == "3"{
                
                ratingHLImage.image = UIImage(named: "rating_3")
                
            }else if rat.contains("3."){
                
                ratingHLImage.image = UIImage(named: "rating_3.5")
                
            }else if rat == "4"{
                
                ratingHLImage.image = UIImage(named: "rating_4")
                
            }else if rat.contains("4."){
                
                ratingHLImage.image = UIImage(named: "rating_4.5")
                
            }else if rat == "5"{
                
                ratingHLImage.image = UIImage(named: "rating_5")
            }
        }else{
            
            menuHideAction(self)
            
            if indexPath.row == 0
            {
                //                appDelegate.isLogined = false
                //
                //                UserDefaults.standard.set("false", forKey: "isUserLogined")
                //                UserDefaults.standard.synchronize()
                logOut()
            }
                
            else if indexPath.row == 2
            {
                if appDelegate.isLogined == true
                {
                    let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
                    let Clean_EarnViewControllerVC  = storyBoard.instantiateViewController(withIdentifier: "CleanEarnViewController") as! Clean_EarnViewController
                    self.navigationController?.pushViewController(Clean_EarnViewControllerVC, animated: true)
                }else
                {
                    gotoLogin()
                }
            }
            else if indexPath.row == 3
            {
                if appDelegate.isLogined == true{
                    let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
                    let GetRewardsVC  = storyBoard.instantiateViewController(withIdentifier: "GetRewardsViewController") as! GetRewardsViewController
                    self.navigationController?.pushViewController(GetRewardsVC, animated: true)
                }else{
                    gotoLogin()
                }
            }
            else if indexPath.row == 4
            {
                if appDelegate.isLogined == true{
                    let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
                    let myBalanceVC  = storyBoard.instantiateViewController(withIdentifier: "myBalanceViewController") as! myBalanceViewController
                    self.navigationController?.pushViewController(myBalanceVC, animated: true)
                }else{
                    gotoLogin()
                }
            }
            else if indexPath.row == 5
            {
                if appDelegate.isLogined == true{
                    let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
                    let MyOrderVC  = storyBoard.instantiateViewController(withIdentifier: "MyOrderViewController") as! MyOrderViewController
                    self.navigationController?.pushViewController(MyOrderVC, animated: true)
                }
                else{
                    gotoLogin()
                }
            }
            else if indexPath.row == 6
            {
                let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
                let MyFavouriteVC  = storyBoard.instantiateViewController(withIdentifier: "MyFavouriteViewController") as! MyFavouriteViewController
                self.navigationController?.pushViewController(MyFavouriteVC, animated: true)
            }
            else if indexPath.row == 7{
                
            }
                
            else if indexPath.row == 8{
                if appDelegate.isLogined == true{
                    
                    let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
                    let SuggestionVC  = storyBoard.instantiateViewController(withIdentifier: "SuggestionViewController") as! SuggestionViewController
                    self.navigationController?.pushViewController(SuggestionVC, animated: true)
                }
                else
                {
                    gotoLogin()
                    
                }
            }
            else if indexPath.row == 9
            {
                let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
                let OurVisionVC  = storyBoard.instantiateViewController(withIdentifier: "OurVisionViewController") as! OurVisionViewController
                self.navigationController?.pushViewController(OurVisionVC, animated: true)
            }
                
            else if indexPath.row == 10
            {
                if appDelegate.isLogined == true{
                    
                    let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
                    let FeedbackVC  = storyBoard.instantiateViewController(withIdentifier: "FeedbackViewController") as! FeedbackViewController
                    self.navigationController?.pushViewController(FeedbackVC, animated: true)
                    
                }
                else {
                    gotoLogin()
                }
            }
            else if indexPath.row == 11
            {
                let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
                let FAQViewControllerVC  = storyBoard.instantiateViewController(withIdentifier: "FAQViewController") as! FAQViewController
                self.navigationController?.pushViewController(FAQViewControllerVC, animated: true)
            }
            else if indexPath.row == 12{
                let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
                let contactUsVC  = storyBoard.instantiateViewController(withIdentifier: "contactUsViewController") as! contactUsViewController
                self.navigationController?.pushViewController(contactUsVC, animated: true)
            }
            else if indexPath.row == 13
            {
                let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
                let aboutAsVC  = storyBoard.instantiateViewController(withIdentifier: "AboutUsViewController") as! AboutUsViewController
                self.navigationController?.pushViewController(aboutAsVC, animated: true)
            }
            if indexPath.row == 14
            {
                let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
                let MyProfileVC  = storyBoard.instantiateViewController(withIdentifier: "MyProfileViewController") as! MyProfileViewController
                self.navigationController?.pushViewController(MyProfileVC, animated: true)
            }
            else
            {
                
                
            }
        }
    }
    func logOut()
    {
        if appDelegate.isLogined == true
        {
            let alert = UIAlertController(title: "Logout", message: "Are you sure want to logout", preferredStyle: UIAlertController.Style.alert)
            
            // add the actions (buttons)
            
            alert.addAction(UIAlertAction(title: "CANCEL", style: UIAlertAction.Style.cancel, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
            
            alert.addAction(UIAlertAction(title: "LOGOUT", style: UIAlertAction.Style.default, handler: { action in
                
                let loginManager = LoginManager()
                if let _ = AccessToken.current {
                    loginManager.logOut()
                }
                appDelegate.isLogined = false
                appDelegate.UserName = ""
                appDelegate.loginMobileNumber = ""
                appDelegate.loginEmailAddress = ""
                UserDefaults.standard.set("false4", forKey: "isUserLogined")
                UserDefaults.standard.set(nil, forKey: "notiArray")
                UserDefaults.standard.synchronize()
                self.loginArrayFunc()
            }))
        }
        else{
            let storyBoard          = UIStoryboard(name: "Main", bundle: nil)
            let LoginViewControllerVC  = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.navigationController?.pushViewController(LoginViewControllerVC, animated: true)
        }
        
    }
    
    func gotoLogin(){
        let storyBoard          = UIStoryboard(name: "Main", bundle: nil)
        let LoginVC  = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(LoginVC, animated: true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == commentTbl{
            
            return 100
        }
        
        return 50
    }
    //MARK: - Hightlight Action
    
    @IBAction func commentHightLightAction(_ sender: Any) {
        self.commentHightView.isHidden = true
        
        coomentGrayVwWidth.constant = self.view.frame.width - 40
        
    }
    
    
    //MARK: - More Action
    
    @IBAction func menuAction(_ sender: AnyObject){
        loginArrayFunc()
        let width = self.view.frame.width - 80
        
        menuTransparentView.isHidden = false
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            
            self.menuBlackVwWidth.constant = width
            
            UIView.animate(withDuration: 0.5) {
                
                self.view.layoutIfNeeded()
            }
        })
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.7, execute: {
            
            self.menuBlackVw.roundCorners(corners: [.topRight, .bottomRight], radius: 50)
            self.menuBlackVw.clipsToBounds = true
        })
        
        
    }
    
    @IBAction func menuHideAction(_ sender: AnyObject){
        
        self.menuBlackVwWidth.constant = 0
        
        UIView.animate(withDuration: 0.5) {
            
            self.view.layoutIfNeeded()
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
            
            self.menuTransparentView.isHidden = true
        })
        
    }
    
    //MARK: - More Action
    
    @IBAction func moreAction(_ sender: AnyObject){
        
        let storyBoard          = UIStoryboard(name: "Main", bundle: nil)
        let MoreCategoryViewController  = storyBoard.instantiateViewController(withIdentifier: "MoreCategoryViewController") as! MoreCategoryViewController
        self.navigationController?.pushViewController(MoreCategoryViewController, animated: true)
    }
    
    @IBAction func moreRecipeAction(_ sender: AnyObject){
        
        let storyBoard          = UIStoryboard(name: "Main", bundle: nil)
        let MoreCategoryViewController  = storyBoard.instantiateViewController(withIdentifier: "MoreRecipeViewController") as! MoreRecipeViewController
        self.navigationController?.pushViewController(MoreCategoryViewController, animated: true)
    }
    
    @IBAction func moreProducts(_ sender: AnyObject){
        
        let storyBoard          = UIStoryboard(name: "Main", bundle: nil)
        let MoreCategoryViewController  = storyBoard.instantiateViewController(withIdentifier: "MoreProductsViewController") as! MoreProductsViewController
        self.navigationController?.pushViewController(MoreCategoryViewController, animated: true)
    }
    
    //MARK: - Recycle Action
    
    @IBAction func recycleAction(_ sender: AnyObject){
        
        if appDelegate.isLogined == true
        {
            let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
            let Clean_EarnViewControllerVC  = storyBoard.instantiateViewController(withIdentifier: "CleanEarnViewController") as! Clean_EarnViewController
            self.navigationController?.pushViewController(Clean_EarnViewControllerVC, animated: true)
        }else
        {
            gotoLogin()
        }
    }
    
    //MARK: - Recycle Action
    
    @IBAction func sellProductAction(_ sender: AnyObject){
        
        if appDelegate.isLogined == true
        {
            let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
            let SellProductViewController  = storyBoard.instantiateViewController(withIdentifier: "SellProductViewController") as! SellProductViewController
            self.navigationController?.pushViewController(SellProductViewController, animated: true)
        }else
        {
            gotoLogin()
        }
    }
    
    //MARK: - Notification Action
    
    @IBAction func notificationAction(_ sender: AnyObject){
        
        let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
        let NotificationViewController  = storyBoard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(NotificationViewController, animated: true)
    }
    
    @IBAction func MyCartAction(_ sender: Any) {
        if CartCount == "0"
        {
            GlobalClass.showAlertDialog(message: "You dont have any item on your cart", target: self)
            
        }
        else{
            let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
            let MyOrderViewController  = storyBoard.instantiateViewController(withIdentifier: "CartListViewController") as! CartListViewController
            self.navigationController?.pushViewController(MyOrderViewController, animated: true)
            
            //            let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
            //            let HomeViewController  = storyBoard.instantiateViewController(withIdentifier: "ProceedViewController") as! ProceedViewController
            //            self.navigationController?.pushViewController(HomeViewController, animated: true)
        }
    }
    
    @IBAction func nameAction(_ sender: Any) {
        
    }
    
    //AARK: - Location Manger
    
    func locationManager(_ manager: CLLocationManager,didFailWithError error: Error) {
        //  print(error)
    }
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("locationManager update")
        let newLocation = locations.last
        
        //  let latitude = String(format: "%.4f", newLocation!.coordinate.latitude)
        //  let longitude = String(format: "%.4f", newLocation!.coordinate.longitude)
        
        
        Currentlatitude = newLocation!.coordinate.latitude
        Currentlongitude = newLocation!.coordinate.longitude
        
        GeoCode()
    }
    
    var Currentlatitude = 0.0
    var Currentlongitude = 0.0
    
    func GeoCode(){
        
        CLGeocoder().reverseGeocodeLocation(CLLocation.init(latitude: Currentlatitude, longitude:Currentlongitude)) { (places, error) in
            if error == nil{
                let placeMark = places! as [CLPlacemark]
                
                if placeMark.count > 0 {
                    let placeMark = places![0]
                    var addressString : String = ""
                    
                    //                    if placeMark.subThoroughfare != nil {
                    //                        addressString = addressString + placeMark.subThoroughfare! + ", "
                    //                    }
                    //                    if placeMark.thoroughfare != nil {
                    //                        addressString = addressString + placeMark.thoroughfare! + ", "
                    //                    }
                    //                    if placeMark.subLocality != nil {
                    //                        addressString = addressString + placeMark.subLocality! + ", "
                    //                    }
                    
                    if placeMark.locality != nil {
                        addressString = addressString + placeMark.locality! + ""
                    }
                    //                    if placeMark.administrativeArea != nil {
                    //                        addressString = addressString + placeMark.administrativeArea! + ", "
                    //                    }
                    //                    if placeMark.country != nil {
                    //                        addressString = addressString + placeMark.country! + ", "
                    //                    }
                    //                    if placeMark.postalCode != nil {
                    //                        addressString = addressString + placeMark.postalCode! + " "
                    //                    }
                    
                    //     print("addressString" , addressString)
                    
                    self.placeTxtField.text = addressString
                    
                }
            }
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
