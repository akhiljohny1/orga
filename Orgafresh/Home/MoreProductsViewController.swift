//
//  MoreProductsViewController.swift
//  Orgafresh
//
//  Created by Innovation Incubator on 09/10/20.
//  Copyright © 2020 Innovation Incubator. All rights reserved.
//

import UIKit
import Alamofire
import Moya
import SwiftyJSON
import SVProgressHUD
import SDWebImage

class MoreProductsViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UITableViewDataSource, UITableViewDelegate {
    
    var productVals                       = JSON()
    
    var searchedProductVals               = [JSON]()
    
    @IBOutlet weak var gradientVwTopConstrain    : NSLayoutConstraint!
    @IBOutlet weak var gradientVwHeightConstrain : NSLayoutConstraint!
    
    @IBOutlet weak var CartButton: SSBadgeButton!
    
    @IBOutlet weak var TitleLabel: UILabel!
    var categoryValues                      = [JSON]()
    var values                              = JSON()
    
    var arr2 = JSON()
    var vals2 = [JSON]()
    
    @IBOutlet weak var collectionVBottomConstarint: NSLayoutConstraint!
    
    var productNameArr                 = [String]()
    
    var categoryNameArr                = [String]()
    
    var productNameAccendingOrder      = [String]()
    var productNameDessendingOrder     = [String]()
    var categoryNameAccendingOrder     = [String]()
    var categoryNameDessendingOrder    = [String]()
    
    var selectedIndex                  = 0
    
    @IBOutlet weak var filterBlackButt : UIButton!
    @IBOutlet weak var filterVw         : UIView!
    
    let listArr                        = ["Product Name A-Z", "Product Name Z-A", "Category Name A-Z","Category Name Z-A"]
    
    @IBOutlet weak var CategoryCollectionView: UICollectionView!
    
    @IBOutlet weak var searchTextField      : UITextField!
    
    @IBOutlet weak var resetButt            : UIButton!
    
    var isFilteredCategory                  = false
    
    var TempCartValues = [JSON]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if DeviceType.IS_IPHONE_X || DeviceType.IS_IPHONE_XP{
            
            gradientVwTopConstrain.constant = 0
            gradientVwHeightConstrain.constant = 55
            
        }else{
            
            gradientVwTopConstrain.constant = 0
            gradientVwHeightConstrain.constant = 55
            collectionVBottomConstarint.constant = 260
            
        }
        
        let leftView1                            = UIView(frame: CGRect(x: 5, y: 0, width: 37, height: 26))
        leftView1.backgroundColor                = .clear
        searchTextField.leftView                 = leftView1
        searchTextField.leftViewMode             = .always
        searchTextField.contentVerticalAlignment = .center
        
        searchTextField.addTarget(self, action: #selector(calculate), for: .editingChanged)
        searchTextField.text = ""
        
        resetButt.isHidden = true
        
        callGetProducts()
        
        filterVw.isHidden = true
        filterBlackButt.isHidden = true
        
        if UserDefaults.standard.value(forKey: "myfavourite") != nil{
            
            let vals = UserDefaults.standard.value(forKey: "myfavourite") as Any
            
            let retrivedVals = JSON(vals)
            
            for i in 0 ..< retrivedVals.count{
                
                vals2.append(retrivedVals[i])
            }
        }
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if CartCount == "0"
        {
            CartButton.badge = ""
            CartButton.badgeLabel.backgroundColor = UIColor.clear
        }
        else
        {
            CartButton.badge = "\(CartCount)"
        }
        
        reteriveTempData()
    }
    
    //MARK: - Calculate Posts
    
    @objc func calculate(){
        
        let updatedText = searchTextField.text!
        
        var filterdTerms = [String]()
        
        if isFilteredCategory == false{
            
            func filterContentForSearchText(searchText: String) {
                filterdTerms = productNameArr.filter { term in
                    return term.lowercased().contains(searchText.lowercased())
                }
            }
            
            filterContentForSearchText(searchText: updatedText)
            print(filterdTerms)
            
            searchedProductVals.removeAll()
            
            if filterdTerms.count > 0{
                
                for i in filterdTerms{
                    
                    for j in 0 ..< categoryValues.count{
                        
                        let val = categoryValues[j]
                        
                        if val["product_name"].stringValue.contains(i){
                            
                            searchedProductVals.append(val)
                        }
                    }
                    
                }
            }
        }else{
            
            func filterContentForSearchText(searchText: String) {
                filterdTerms = categoryNameArr.filter { term in
                    return term.lowercased().contains(searchText.lowercased())
                }
            }
            
            filterContentForSearchText(searchText: updatedText)
            print(filterdTerms)
            
            searchedProductVals.removeAll()
            
            if filterdTerms.count > 0{
                
                for i in filterdTerms{
                    
                    for j in 0 ..< categoryValues.count{
                        
                        let val = categoryValues[j]
                        
                        if val["category_name"].stringValue.contains(i){
                            
                            searchedProductVals.append(val)
                        }
                    }
                    
                }
            }
        }
        
        
        
        if updatedText == ""{
            
            resetButt.isHidden = true
            searchedProductVals.removeAll()
        }else{
            resetButt.isHidden = false
        }
        
        CategoryCollectionView.reloadData()
    }
    
    
    //MARK: - Collection View
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if searchedProductVals.count > 0{
            
            return searchedProductVals.count
        }
        
        if searchTextField.text != ""{
            
            return 0
        }
        
        return categoryValues.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let categoryCell = CategoryCollectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCell", for: indexPath) as! CategoryCell
        
        var selectedCategoryVals = JSON()
        
        if searchedProductVals.count > 0{
            
            selectedCategoryVals = searchedProductVals[indexPath.row]
        }else{
            
            selectedCategoryVals = categoryValues[indexPath.row]
        }
        
        
        
        categoryCell.favoriteButton.tag = indexPath.row
        categoryCell.addCartButton.tag = indexPath.row
        
        categoryCell.PriceLabel.text =  "₹" + "\(selectedCategoryVals["normal_price"].intValue)"
        categoryCell.DescriptionLabel.text =   selectedCategoryVals["product_name"].stringValue
        categoryCell.sellingPriceLabel.text =  "₹" + "\(selectedCategoryVals["selling_price"].intValue)"
        categoryCell.GramsLabel.text =  "/" + "\(selectedCategoryVals["minimum_order_quantity"].intValue)" + " " + selectedCategoryVals["unit_name"].stringValue
        
        categoryCell.PriceLabel.attributedText = categoryCell.PriceLabel.text!.strikeThrough()
        
        var str = imageBaseUrl + selectedCategoryVals["product_img"].stringValue
        
        str = str.replacingOccurrences(of: " ", with: "%20")
        
        categoryCell.CartImageView.sd_setImage(with: URL(string: str), placeholderImage: nil)
        
        for i in 0 ..< vals2.count
        {
            
            if selectedCategoryVals["product_name"].stringValue == vals2[i]["product_name"].stringValue
            {
                categoryCell.favoriteButton.isSelected = true;
                categoryCell.favoriteButton.setImage(UIImage(named: "like"), for: .selected)
                
            }
        }
        
        if selectedCategoryVals["availability"].intValue == 0
        {
            categoryCell.availabilityLabel.isHidden = false
            categoryCell.burView.isHidden = false
            
            let blurEffect = UIBlurEffect(style: .light)
            
            let blurredEffectView = UIVisualEffectView(effect: blurEffect)
            blurredEffectView.frame = categoryCell.burView.frame
            print(blurredEffectView.frame)
            
            categoryCell.burView.addSubview(blurredEffectView)
            
        }
        else{
            categoryCell.availabilityLabel.isHidden = true
            categoryCell.burView.isHidden = true
        }
        return categoryCell
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let numberOfCellInRow : Int        = 2
        let padding : Int                  = 2
        let collectionCellWidth : CGFloat  = (self.CategoryCollectionView.frame.size.width/CGFloat(numberOfCellInRow)) - CGFloat(padding)
        
        return CGSize(width: collectionCellWidth, height: collectionCellWidth)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        var selectedCategoryVals = JSON()
        
        if searchedProductVals.count > 0{
            
            selectedCategoryVals = searchedProductVals[indexPath.row]
        }else{
            
            selectedCategoryVals = categoryValues[indexPath.row]
        }
        
        if selectedCategoryVals["availability"].intValue != 0
        {
            let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
            let ProductDetailsVC  = storyBoard.instantiateViewController(withIdentifier: "ProductDetailsViewController") as! ProductDetailsViewController
            
            ProductDetailsVC.productValues = selectedCategoryVals
            self.navigationController?.pushViewController(ProductDetailsVC, animated: true)
        }
        else{
            GlobalClass.showAlertDialog(message: "Product is out of stock", target: self)
        }
    }
    
    //MARK: - Table View
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return listArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        
        let lbl = cell!.viewWithTag(1) as! UILabel
        
        lbl.text = listArr[indexPath.row]
        
        let img = cell!.viewWithTag(2) as! UIImageView
        
        if selectedIndex == indexPath.row{
            
            img.image = #imageLiteral(resourceName: "radioOn")
        }else{
            
            img.image = #imageLiteral(resourceName: "Circlebutton")
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print(indexPath.row)
        
        selectedIndex = indexPath.row
        
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 56
    }
    
    @IBAction func okAction(_ sender: UIButton) {
        
        productNameAccendingOrder.removeAll()
        productNameDessendingOrder.removeAll()
        categoryNameAccendingOrder.removeAll()
        categoryNameDessendingOrder.removeAll()
        searchedProductVals.removeAll()
        
        if selectedIndex == 0 || selectedIndex == 1{
            
            isFilteredCategory = false
        }
        
        if selectedIndex == 2 || selectedIndex == 3{
            
            isFilteredCategory = true
        }
        
        if selectedIndex == 0{
            
            let sortedNames = self.productNameArr.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
            
            print("sorted arr :", sortedNames)
            productNameAccendingOrder = sortedNames
            
            if productNameAccendingOrder.count > 0{
                
                for i in productNameAccendingOrder{
                    
                    for j in 0 ..< categoryValues.count{
                        
                        let val = categoryValues[j]
                        
                        if val["product_name"].stringValue.contains(i){
                            
                            searchedProductVals.append(val)
                        }
                    }
                    
                }
            }
            
            CategoryCollectionView.reloadData()
            
        }else if selectedIndex == 1{
            
            let sortedNames = self.productNameArr.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedDescending }
            
            print("sorted arr :", sortedNames)
            productNameDessendingOrder = sortedNames
            
            if productNameDessendingOrder.count > 0{
                
                for i in productNameDessendingOrder{
                    
                    for j in 0 ..< categoryValues.count{
                        
                        let val = categoryValues[j]
                        
                        if val["product_name"].stringValue.contains(i){
                            
                            searchedProductVals.append(val)
                        }
                    }
                    
                }
            }
            
            CategoryCollectionView.reloadData()
            
        }else if selectedIndex == 2{
            
            let sortedNames = self.categoryNameArr.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
            
            print("sorted arr :", sortedNames)
            
            categoryNameAccendingOrder = sortedNames
            
            if categoryNameAccendingOrder.count > 0{
                
                for i in categoryNameAccendingOrder{
                    
                    for j in 0 ..< categoryValues.count{
                        
                        let val = categoryValues[j]
                        
                        if val["product_name"].stringValue.contains(i){
                            
                            searchedProductVals.append(val)
                        }
                    }
                    
                }
            }
            
            CategoryCollectionView.reloadData()
            
        }else if selectedIndex == 3{
            
            let sortedNames = self.categoryNameArr.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedDescending }
            
            print("sorted arr :", sortedNames)
            
            categoryNameDessendingOrder = sortedNames
            
            if categoryNameDessendingOrder.count > 0{
                
                for i in categoryNameDessendingOrder{
                    
                    for j in 0 ..< categoryValues.count{
                        
                        let val = categoryValues[j]
                        
                        if val["product_name"].stringValue.contains(i){
                            
                            searchedProductVals.append(val)
                        }
                    }
                    
                }
            }
            
            CategoryCollectionView.reloadData()
        }
        
        filterHideAction(UIButton())
    }
    
    @IBAction func filterAction(_ sender: UIButton) {
        self.view.endEditing(true)
        filterVw.isHidden = false
        filterBlackButt.isHidden = false
    }
    
    @IBAction func filterHideAction(_ sender: UIButton) {
        
        filterVw.isHidden = true
        filterBlackButt.isHidden = true
        
    }
    
    //MARK: - Call Product
    
    func callGetProducts() {
        
        DispatchQueue.main.async {
            
            SVProgressHUD.show(withStatus: "Loading...")
        }
        
        networkProvider.request(.getProducts, completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                SVProgressHUD.dismiss()
                
                let jsonVal = JSON(response.data)
                
                print("Product vals : \(jsonVal)")
                
                if response.statusCode == 200{
                    
                    if jsonVal["status"].stringValue == "success"{
                        
                        self.categoryValues.removeAll()
                        
                        for i in 0 ..< jsonVal["products"].count{
                            
                            self.categoryValues.append(jsonVal["products"][i])
                        }
                        
                        //   self.categoryValues = jsonVal["products"]
                        
                        print("Values : ", self.categoryValues)
                        
                        for i in 0 ..< self.categoryValues.count{
                            
                            self.productNameArr.append(self.categoryValues[i]["product_name"].stringValue.trimmingCharacters(in: .whitespacesAndNewlines))
                            
                            self.categoryNameArr.append(self.categoryValues[i]["category_name"].stringValue.trimmingCharacters(in: .whitespacesAndNewlines))
                            
                        }
                        
                        print("Product name : ", self.productNameArr)
                        //                        print("Category name : ", self.categoryNameArr)
                        
                        //                        let sortedNames = self.productNameArr.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
                        //
                        //                        print("sorted arr :", sortedNames)
                        
                    }else{
                        
                        GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                    }
                }else if response.statusCode == 401{
                    
                    GlobalClass.callGetApiKey(viewController: self)
                    GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                    
                }else if jsonVal["message"].stringValue.contains("token"){
                    
                    GlobalClass.callGetApiKey(viewController: self)
                }
                else if response.statusCode == 500{
                    
                    GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                }
                else{
                    
                    GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                }
                
                DispatchQueue.main.async {
                    
                    self.CategoryCollectionView.reloadData()
                    
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
                    
                    self.CategoryCollectionView.reloadData()
                })
                
            case .failure( _):
                
                GlobalClass.showAlertDialog(message: "Please try again.", target: self)
                
                SVProgressHUD.dismiss()
                
                break
            }
        })
    }
    
    
    //MARK: - Notification Action
    
    @IBAction func notificationAction(_ sender: AnyObject){
        
        let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
        let NotificationViewController  = storyBoard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(NotificationViewController, animated: true)
    }
    
    @IBAction func MyCartAction(_ sender: Any) {
        if CartCount == "0"
        {
            GlobalClass.showAlertDialog(message: "You dont have any item on your cart", target: self)
            
        }
        else{
            let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
            let MyOrderViewController  = storyBoard.instantiateViewController(withIdentifier: "CartListViewController") as! CartListViewController
            self.navigationController?.pushViewController(MyOrderViewController, animated: true)
        }
    }
    
    //MARK: - Button Action
    var categoryaddcartIndex = Int()
    @IBAction func AddCartValues(_ sender: UIButton) {
        categoryaddcartIndex = sender.tag
        
        if searchedProductVals.count > 0{
        
            if searchedProductVals[categoryaddcartIndex]["availability"].intValue == 0
            {
                GlobalClass.showAlertDialog(message: "Product is out of stock", target: self)
                
            }
            else{
                cartaddSearched()
            }
        }else{
        
        if categoryValues[categoryaddcartIndex]["availability"].intValue == 0
        {
            GlobalClass.showAlertDialog(message: "Product is out of stock", target: self)
            
        }
        else{
            cartadd()
        }
        }
    }
    
    func cartadd()
    {
        
        CartArray["product_name"] = JSON(categoryValues[categoryaddcartIndex]["product_name"].stringValue)
        CartArray["normal_price"] = JSON(categoryValues[categoryaddcartIndex]["normal_price"].stringValue)
        CartArray["selling_price"] = JSON(categoryValues[categoryaddcartIndex]["selling_price"].stringValue)
        CartArray["quantity"] = JSON(1)
        
        CartArray["minimum_order_quantity"] = JSON(categoryValues[categoryaddcartIndex]["minimum_order_quantity"].stringValue)
        CartArray["unit_name"] = JSON(categoryValues[categoryaddcartIndex]["unit_name"].stringValue)
        CartArray["product_img"] = JSON(categoryValues[categoryaddcartIndex]["product_img"].stringValue)
        
        CartArray["id"] = JSON(categoryValues[categoryaddcartIndex]["id"].stringValue)
        var valsVal = -1
        
        for i in 0 ..< self.vals2.count{
            
            let val = self.vals2[i]
            
            if val["id"].stringValue == CartArray["id"].stringValue{
                
                valsVal = i
                
            }
        }
        if valsVal != -1{
            
            self.TempCartValues.remove(at: valsVal)
        }
        
        GlobalClass.showAlertDialog(message: "Your Item has been added to Cart.", target: self)
        
        self.TempCartValues.append(CartArray)
        print(self.TempCartValues)
        UserDefaults.standard.setValue(JSON(self.TempCartValues).rawValue, forKey: "TempCartValues")
        UserDefaults.standard.synchronize()
        CartCount = "\(self.TempCartValues.count)"
        CartButton.badge = "\(self.TempCartValues.count)"
        
    }
    
    func cartaddSearched()
    {
        
        CartArray["product_name"] = JSON(searchedProductVals[categoryaddcartIndex]["product_name"].stringValue)
        CartArray["normal_price"] = JSON(searchedProductVals[categoryaddcartIndex]["normal_price"].stringValue)
        CartArray["selling_price"] = JSON(searchedProductVals[categoryaddcartIndex]["selling_price"].stringValue)
        CartArray["quantity"] = JSON(1)
        
        CartArray["minimum_order_quantity"] = JSON(searchedProductVals[categoryaddcartIndex]["minimum_order_quantity"].stringValue)
        CartArray["unit_name"] = JSON(searchedProductVals[categoryaddcartIndex]["unit_name"].stringValue)
        CartArray["product_img"] = JSON(searchedProductVals[categoryaddcartIndex]["product_img"].stringValue)
        
        CartArray["id"] = JSON(searchedProductVals[categoryaddcartIndex]["id"].stringValue)
        var valsVal = -1
        
        for i in 0 ..< self.vals2.count{
            
            let val = self.vals2[i]
            
            if val["id"].stringValue == CartArray["id"].stringValue{
                
                valsVal = i
                
            }
        }
        if valsVal != -1{
            
            self.TempCartValues.remove(at: valsVal)
        }
        
        GlobalClass.showAlertDialog(message: "Your Item has been added to Cart.", target: self)
        
        self.TempCartValues.append(CartArray)
        print(self.TempCartValues)
        UserDefaults.standard.setValue(JSON(self.TempCartValues).rawValue, forKey: "TempCartValues")
        UserDefaults.standard.synchronize()
        CartCount = "\(self.TempCartValues.count)"
        CartButton.badge = "\(self.TempCartValues.count)"
        
    }
    
    
    func reteriveTempData()
    {
        
        let vals = UserDefaults.standard.value(forKey: "TempCartValues") as Any
        
        let retrivedVals = JSON(vals)
        
        TempCartValues.removeAll()
        
        for i in 0 ..< retrivedVals.count{
            TempCartValues.append(retrivedVals[i])
        }
        if TempCartValues.count == 0
        {
            CartButton.badge = ""
            CartButton.badgeLabel.backgroundColor = UIColor.clear
        }
        else
        {
            CartButton.badge = "\(TempCartValues.count)"
        }
        CartCount = "\(TempCartValues.count)"
        UserDefaults.standard.setValue(JSON("\(TempCartValues.count)").rawValue, forKey: "CartCount")
        UserDefaults.standard.synchronize()
        
    }
    
    //MARK: - Button Action
    
    @IBAction func backAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func resetAction(_ sender: Any) {
        
        searchTextField.text = ""
        resetButt.isHidden = true
        searchedProductVals.removeAll()
        searchTextField.resignFirstResponder()
        
        CategoryCollectionView.reloadData()
        
    }
    
    @IBAction func FavoriteAction(_ sender: UIButton) {
        
        print(sender.tag)
        
        if let favoriteButton = sender as? UIButton {
            
            if searchedProductVals.count > 0{
                
                if favoriteButton.isSelected {
                    // set deselected
                    favoriteButton.isSelected = false
                    print("not selected")
                    // favoriteButton.tintColor = UIColor.lightGray
                    
                    print("categoryValues :", searchedProductVals[sender.tag])
                    
                    print("all values :",vals2)
                    
                    var valsVal = -1
                    
                    for i in 0 ..< vals2.count{
                        
                        print("vals \(i):", vals2[i])
                        
                        if Int(vals2[i]["id"].stringValue) == searchedProductVals[sender.tag]["id"].intValue{
                            
                            // vals2.remove(at: i)
                            valsVal = i
                        }
                    }
                    
                    if valsVal != -1{
                        
                        vals2.remove(at: valsVal)
                    }
                    
                    UserDefaults.standard.setValue(JSON(vals2).rawValue, forKey: "myfavourite")
                    UserDefaults.standard.synchronize()
                    
                } else {
                    // set selected
                    favoriteButton.isSelected = true
                    print("selected")
                    //   favoriteButton.tintColor = #colorLiteral(red: 0.2, green: 0.8, blue: 1, alpha: 1)
                    
                    arr2["product_name"] = JSON(searchedProductVals[sender.tag]["product_name"].stringValue)
                    arr2["normal_price"] = JSON(searchedProductVals[sender.tag]["normal_price"].stringValue)
                    arr2["selling_price"] = JSON(searchedProductVals[sender.tag]["selling_price"].stringValue)
                    arr2["minimum_order_quantity"] = JSON(searchedProductVals[sender.tag]["minimum_order_quantity"].stringValue)
                    arr2["unit_name"] = JSON(searchedProductVals[sender.tag]["unit_name"].stringValue)
                    arr2["product_img"] = JSON(searchedProductVals[sender.tag]["product_img"].stringValue)
                    
                    arr2["product_desc"] = JSON(searchedProductVals[sender.tag]["product_desc"].stringValue)
                    arr2["id"] = JSON(searchedProductVals[sender.tag]["id"].stringValue)
                    vals2.append(arr2)
                    print(vals2)
                    
                    UserDefaults.standard.setValue(JSON(vals2).rawValue, forKey: "myfavourite")
                    UserDefaults.standard.synchronize()
                    
                }
            }else{
                
                if favoriteButton.isSelected {
                    // set deselected
                    favoriteButton.isSelected = false
                    print("not selected")
                    // favoriteButton.tintColor = UIColor.lightGray
                    
                    
                    print("categoryValues :", categoryValues[sender.tag])
                    
                    print("all values :",vals2)
                    
                    var valsVal = -1
                    
                    for i in 0 ..< vals2.count{
                        
                        print("vals \(i):", vals2[i])
                        
                        if Int(vals2[i]["id"].stringValue) == categoryValues[sender.tag]["id"].intValue{
                            
                            // vals2.remove(at: i)
                            valsVal = i
                        }
                    }
                    
                    if valsVal != -1{
                        
                        vals2.remove(at: valsVal)
                    }
                    
                    UserDefaults.standard.setValue(JSON(vals2).rawValue, forKey: "myfavourite")
                    UserDefaults.standard.synchronize()
                    
                } else {
                    // set selected
                    favoriteButton.isSelected = true
                    print("selected")
                    //   favoriteButton.tintColor = #colorLiteral(red: 0.2, green: 0.8, blue: 1, alpha: 1)
                    
                    arr2["product_name"] = JSON(categoryValues[sender.tag]["product_name"].stringValue)
                    arr2["normal_price"] = JSON(categoryValues[sender.tag]["normal_price"].stringValue)
                    arr2["selling_price"] = JSON(categoryValues[sender.tag]["selling_price"].stringValue)
                    arr2["minimum_order_quantity"] = JSON(categoryValues[sender.tag]["minimum_order_quantity"].stringValue)
                    arr2["unit_name"] = JSON(categoryValues[sender.tag]["unit_name"].stringValue)
                    arr2["product_img"] = JSON(categoryValues[sender.tag]["product_img"].stringValue)
                    
                    arr2["product_desc"] = JSON(categoryValues[sender.tag]["product_desc"].stringValue)
                    arr2["id"] = JSON(categoryValues[sender.tag]["id"].stringValue)
                    vals2.append(arr2)
                    print(vals2)
                    
                    UserDefaults.standard.setValue(JSON(vals2).rawValue, forKey: "myfavourite")
                    UserDefaults.standard.synchronize()
                    
                    
                }
            }
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
