//
//  PromoOffersViewController.swift
//  Orgafresh
//
//  Copyright © 2020 Innovation Incubator. All rights reserved.
//

import UIKit
import SVProgressHUD
import SwiftyJSON
import Alamofire

class offerCell: UITableViewCell{
    
    @IBOutlet weak var offer_percentage: UILabel!
    @IBOutlet weak var coupon_code: UILabel!
    @IBOutlet weak var offer_name: UILabel!
  //  @IBOutlet weak var description: UILabel!
    @IBOutlet weak var offer_minimum_value: UILabel!
    @IBOutlet weak var end_date: UILabel!
    //@IBOutlet var description: UILabel!
    
    @IBOutlet weak var Description: UILabel!
    @IBOutlet weak var ApplyButton: UIButton!
    

    
    
}

class PromoOffersViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
 @IBOutlet var offersTableView: UITableView!

    var offerId = String()
    @IBOutlet weak var CartButton: SSBadgeButton!

    @IBOutlet weak var gradientVwTopConstrain    : NSLayoutConstraint!
    @IBOutlet weak var gradientVwHeightConstrain : NSLayoutConstraint!
    @IBOutlet weak var tableViewBottomConstarint: NSLayoutConstraint!
    var getOffersValues = JSON()
    @IBOutlet weak var ValiudateTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if DeviceType.IS_IPHONE_X || DeviceType.IS_IPHONE_XP{
            
            gradientVwTopConstrain.constant = 0
            gradientVwHeightConstrain.constant = 55
            
        }else{
            
            gradientVwTopConstrain.constant = 0
            gradientVwHeightConstrain.constant = 55
            tableViewBottomConstarint.constant = 270
        }
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
          
          if CartCount == "0"
                  {
                      CartButton.badge = ""
                      CartButton.badgeLabel.backgroundColor = UIColor.clear
                  }
                  else
                  {
            CartButton.badge = "\(CartCount)"
            }
        getalloffer()
      }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.getOffersValues.count
     }
     
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let offer_cell = offersTableView.dequeueReusableCell(withIdentifier: "offerCell", for: indexPath) as! offerCell
        offer_cell.offer_percentage.text = getOffersValues[indexPath.row]["offer_percentage"].stringValue + "%"
        offer_cell.coupon_code.text = getOffersValues[indexPath.row]["coupon_code"].stringValue
        offer_cell.offer_name.text = "•" + getOffersValues[indexPath.row]["offer_name"].stringValue
        offer_cell.Description.text = "•" + getOffersValues[indexPath.row]["description"].stringValue
        offer_cell.offer_minimum_value.text = "•" + "Min Amount : " +  getOffersValues[indexPath.row]["offer_minimum_value"].stringValue
        let str = convertDateFormater(date: getOffersValues[indexPath.row]["end_date"].stringValue)

        offer_cell.end_date.text = "End on " + str
        offer_cell.ApplyButton.tag = indexPath.row



        return offer_cell
     }
    
    func convertDateFormater(date: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?

        guard let date = dateFormatter.date(from: date) else {
            assert(false, "no date from string")
            return ""
        }

        dateFormatter.dateFormat = "MMM dd,yyyy"
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        let timeStamp = dateFormatter.string(from: date)

        return timeStamp
    }
    @IBAction func applyAvaliableOfferAction(_ sender: UIButton) {
        
        offerId = getOffersValues[sender.tag]["id"].stringValue
        ValiudateTextField.text = "  " + getOffersValues[sender.tag]["coupon_code"].stringValue
        
    }
    @IBAction func ValidateButtonAction(_ sender: Any) {
        offervalidate()

    }
    
    @IBAction func BackAction(_ sender: Any) {
          
          self.navigationController?.popViewController(animated: true)
      }
    @IBAction func AddCartAction(_ sender: Any) {
              if CartCount == "0"
                        {
                         GlobalClass.showAlertDialog(message: "You dont have any item on your cart", target: self)

                     }
                     else{
              let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
              let MyOrderViewController  = storyBoard.instantiateViewController(withIdentifier: "CartListViewController") as! CartListViewController
          self.navigationController?.pushViewController(MyOrderViewController, animated: false)
        }
          }
        @IBAction func NotificationAction(_ sender: Any) {
               let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
                      let NotificationViewController  = storyBoard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
                      self.navigationController?.pushViewController(NotificationViewController, animated: false)
           }
    func getalloffer(){
             
         SVProgressHUD.show(withStatus: "Loading...")
                 
                 networkProvider.request(.getalloffer, completion: {(result) in
                     
                     switch result {
                         
                     case .success(let response):
                         
                         SVProgressHUD.dismiss()
                         
                         let jsonVal = JSON(response.data)
                         
                         print("Json val : \(jsonVal)")
                     
                     print(jsonVal["status"].stringValue)
                     
                     if jsonVal["status"].stringValue == "success"{
                         
                        self.getOffersValues = jsonVal["alloffer"]
    
//                   let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
//                   let ProceedViewControlle  = storyBoard.instantiateViewController(withIdentifier: "ProceedViewController") as! ProceedViewController
//                   self.navigationController?.pushViewController(ProceedViewControlle, animated: true)
                        self.offersTableView.reloadData()
                     }
                     
                 case .failure( _):
                     
                     break
                 }
             })
         }
    
    func offervalidate(){
    
              //   SVProgressHUD.show(withStatus: "Loading...")
    
        let parameter: Parameters = ["offerId" : Int(offerId)!]
                  print("offervalidate params : \(parameter)")
    
                 networkProvider.request(.offervalidate(offer: Int(offerId)!), completion: {(result) in
    
                     switch result {
    
                     case .success(let response):
    
                         let jsonVal = JSON(response.data)
    
                            print("Json val : \(jsonVal)")
    
                         print(jsonVal["status"].stringValue)
    
                         if jsonVal["status"].stringValue == "success"{
                
                            
                GlobalClass.showAlertDialog(message: jsonVal["offervalidation"].stringValue, target: self)
                            
                            
                         }
    
                     case .failure( _):
    
                         break
                     }
                 })
             }
}
