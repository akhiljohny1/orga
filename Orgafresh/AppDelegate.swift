//
//  AppDelegate.swift
//  Orgafresh
//
//  Created by Innovation Incubator on 20/06/20.
//  Copyright © 2020 Innovation Incubator. All rights reserved.
//

import UIKit
import CoreData
import Firebase
import IQKeyboardManagerSwift
import Alamofire
import Moya
import SwiftyJSON
import FirebaseMessaging
import UserNotifications
import FBSDKCoreKit
import GoogleSignIn
import GoogleMaps
import GooglePlaces
import Razorpay
import FirebaseAuth

let appDelegate                   = UIApplication.shared.delegate as! AppDelegate
var networkProvider               = MoyaProvider<ElasticApi>()

var imageBaseUrl                  = String()
var baseUrl                       = String()

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, MessagingDelegate, UNUserNotificationCenterDelegate, GIDSignInDelegate{
    
    var window: UIWindow?
    let gcmMessageIDKey = "AIzaSyA179EjXLFZThFAzwvF_6LHl3erBWZJEKw"
    
    var networkManager                      = NetworkManager.self
    
    var isCalledHome                        = false
    var isLogined                           = false
    var whichtypeLogin                      = ""
    
    var loginTime                           = String()
    var publicUserID                        = Int()
    var loginMobileNumber                   = String()
    var otherMobileNumeber                  = String()
    var loginEmailAddress                   = ""
    var UserName                            = ""
    var access_token                        = ""
    var profilePicture                      = String()
    var grandToatalValue                    = String()
    
    var TotalBalance                        = Double()
    var totalCredit_                        = Double()
    var totalDebit_                         = Double()
    
    var order_id                            = String()
    var order_status = String()
    var order_response = String()
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        if #available(iOS 13.0, *) {
            window?.overrideUserInterfaceStyle = .light
        }
        
        DispatchQueue.main.async {
        
            self.checkUserAlreadyLoggedIn()
        }
        
        let dateFormatterPrint = DateFormatter()
        
        dateFormatterPrint.dateFormat = "MMM dd, hh:mm a"
        dateFormatterPrint.timeZone = NSTimeZone.local
        
        var dat = Date().toGlobalTime()
        
        dat = dat.adding(hour: 5, minutes: 30)
        
        let str = dateFormatterPrint.string(from: dat)
        
        print("Str:", str)
        
        if UserDefaults.standard.value(forKey: "SavedTime") != nil{
            
            let time = UserDefaults.standard.value(forKey: "SavedTime") as! String
            
            let now  = str.components(separatedBy: ",").first!
            
            if time != now{
                
                UserDefaults.standard.set(nil, forKey: "TempCartValues")
                UserDefaults.standard.set(nil, forKey: "CartCount")
                
                UserDefaults.standard.set(str.components(separatedBy: ",").first!, forKey: "SavedTime")
                
                UserDefaults.standard.synchronize()
            }
        }else{
            
            UserDefaults.standard.set(str.components(separatedBy: ",").first!, forKey: "SavedTime")
            UserDefaults.standard.synchronize()
        }
        
        loginTime = str
        
        GMSServices.provideAPIKey("AIzaSyAzCoqt5X_7XgFpk6jmCXigLuJnKj1QC8U")
        GMSPlacesClient.provideAPIKey("AIzaSyAzCoqt5X_7XgFpk6jmCXigLuJnKj1QC8U")
        
        
        // Override point for customization after application launch.
        if(UserDefaults.standard.object(forKey: "CartCount") == nil)
        {
            CartCount = "0"
        }
        else
        {
            CartCount = UserDefaults.standard.value(forKey: "CartCount") as! String
        }
        
        if #available(iOS 13.0, *) {
            window?.overrideUserInterfaceStyle = .light
        }
        
        FirebaseApp.configure()
        Fabric.sharedSDK().debug = true
        
        
        //   Crashlytics.crashlytics().setCrashlyticsCollectionEnabled(true)
        //        Crashlytics.crashlytics()
        
        // Set int_key to 100.
        //  Crashlytics.crashlytics().setCustomValue(100, forKey: "int_key")
        
        // Set str_key to "hello".
        // Crashlytics.crashlytics().setCustomValue("hello", forKey: "str_key")
        
        //  Crashlytics.crashlytics().log("Higgs-Boson detected! Bailing out…, \(attributesDict)")
        
        //  Crashlytics.crashlytics().setUserID("123456789")
        
        Messaging.messaging().delegate = self
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            Messaging.messaging().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        application.applicationIconBadgeNumber = 0
        
        if networkManager.environment == .development{
            
            imageBaseUrl = "http://test.orgafresh.online/storage/images/"
            baseUrl      = "http://test.orgafresh.online/api/"
            
        }else{
            
            imageBaseUrl = "http://orgafresh.online/storage/images/"
            baseUrl      = "http://orgafresh.online/api/"
        }
        
        IQKeyboardManager.shared.enable = true
        
        ApplicationDelegate.shared.application(
            application,
            didFinishLaunchingWithOptions: launchOptions
        )
        GIDSignIn.sharedInstance().clientID = "1051925893076-c60tpfdotoo2vuhu38ups1r6cc9ekkms.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self
        
        return true
    }
    
    //MARK: - Check User Already Logged In
    
    func checkUserAlreadyLoggedIn(){
        
        if UserDefaults.standard.value(forKey: "isUserLogined") != nil{
            
            if UserDefaults.standard.value(forKey: "isUserLogined") as? String == "true"{
            
            if UserDefaults.standard.value(forKey: "publicUserID") != nil{
                
//                if UserDefaults.standard.value(forKey: "loginMobileNumber") != nil{
                    
                    isLogined         = true
                    publicUserID      = UserDefaults.standard.value(forKey: "publicUserID") as! Int
                    
                    if let _ = UserDefaults.standard.value(forKey: "loginMobileNumber") as? String{
                    
                        loginMobileNumber = UserDefaults.standard.value(forKey: "loginMobileNumber") as! String
                        
                    }else{
                        
                        loginMobileNumber = ""
                    }
//                }
            }
            }
        }
        
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
                print("The user has not signed in before or they have since signed out.")
            } else {
                print("\(error.localizedDescription)")
            }
            return
        }
        // Perform any operations on signed in user here.
        let userId = user.userID                  // For client-side use only!
        let idToken = user.authentication.idToken // Safe to send to the server
        let fullName = user.profile.name
        let givenName = user.profile.givenName
        // let familyName = user.profile.familyName
        let email = user.profile.email
        print(givenName)
        print(email)
        print(userId)
        // ...
        
    }
    
    @available(iOS 9.0, *)
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        ApplicationDelegate.shared.application(
            app,
            open: url,
            sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
            annotation: options[UIApplication.OpenURLOptionsKey.annotation]
        )
        return GIDSignIn.sharedInstance().handle(url)
    }
    func application(_ application: UIApplication,
                     open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return GIDSignIn.sharedInstance().handle(url)
    }
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
        print("didDisconnectWith GIDGoogleUser")
        // Perform any operations when the user disconnects from app here.
        // ...
    }
    
    //MARK: - Firebase Token
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("Firebase registration token: \(fcmToken)")
        
        UserDefaults.standard.set(fcmToken, forKey: "PushToken")
        UserDefaults.standard.synchronize()
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        Messaging.messaging().apnsToken = deviceToken as Data
        print("APNs token retrieved: \(deviceToken)")
        
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
        print("Failed to register for notifications: \(error.localizedDescription)")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        // Print full message.
        //        NotificationCenter.default.post(name: NSNotification.Name("PushNotificationRedirect"), object: self, userInfo: userInfo)
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        print("push received completion handler \(userInfo)")
        completionHandler(UIBackgroundFetchResult.newData)
        
    }
    
    // MARK: - delegates
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        
        self.saveContext()
    }
    
    
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Orgafresh")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
}
@available(iOS 10, *)
extension AppDelegate  {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print("Will present :", userInfo)
        
        let json = JSON(userInfo)
        
        print("In json 1 :", json)
        
        let title = json["title"].stringValue
        let body  = json["body"].stringValue
        
        if UserDefaults.standard.value(forKey: "notiArray") != nil{
            
            let arr = UserDefaults.standard.value(forKey: "notiArray") as! NSArray
            
            let mutableArr = NSMutableArray(array: arr)
            
            let dict = NSMutableDictionary()
                        
            dict["body"] = body
            dict["title"] = title
            
            mutableArr.insert(dict, at: 0)
            
            let arr2 = mutableArr as NSArray
            
            UserDefaults.standard.set(arr2, forKey: "notiArray")
            UserDefaults.standard.synchronize()
            
        }else{
       
            let arr = NSMutableArray()
            let dict = NSMutableDictionary()
            
            dict["body"] = body
            dict["title"] = title
                   
            arr.add(dict)
            
            let arr2 = arr as NSArray
            
            UserDefaults.standard.set(arr2, forKey: "notiArray")
            UserDefaults.standard.synchronize()
        }
       
        let arrr = UserDefaults.standard.value(forKey: "notiArray") as! NSArray
        
        print("arrr :", arrr)
        
        // Change this to your preferred presentation option
        completionHandler([[.alert, .sound]])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print full message.
        print("Did receive nottification :", userInfo)
        
        let json = JSON(userInfo)
        
        print("In json 2 :", json)
        
        let title = json["title"].stringValue
         let body  = json["body"].stringValue
         
         if UserDefaults.standard.value(forKey: "notiArray") != nil{
             
             let arr = UserDefaults.standard.value(forKey: "notiArray") as! NSArray
             
             let mutableArr = NSMutableArray(array: arr)
             
             let dict = NSMutableDictionary()
                         
             dict["body"] = body
             dict["title"] = title
             
             mutableArr.insert(dict, at: 0)
             
             let arr2 = mutableArr as NSArray
             
             UserDefaults.standard.set(arr2, forKey: "notiArray")
             UserDefaults.standard.synchronize()
             
         }else{
        
             let arr = NSMutableArray()
             let dict = NSMutableDictionary()
             
             dict["body"] = body
             dict["title"] = title
                    
             arr.add(dict)
             
             let arr2 = arr as NSArray
             
             UserDefaults.standard.set(arr2, forKey: "notiArray")
             UserDefaults.standard.synchronize()
         }
        
         let arrr = UserDefaults.standard.value(forKey: "notiArray") as! NSArray
         
         print("arrr :", arrr)
        
        completionHandler()
    }
}
