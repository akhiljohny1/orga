//
//  VerifyViewController.swift
//  Orgafresh
//
//  Created by Innovation Incubator on 31/07/20.
//  Copyright © 2020 Innovation Incubator. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import SVProgressHUD

class VerifyViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var textFirst       : UITextField!
    @IBOutlet weak var textSecond      : UITextField!
    @IBOutlet weak var textThird       : UITextField!
    @IBOutlet weak var textFourth      : UITextField!
    @IBOutlet weak var textFifth       : UITextField!
    @IBOutlet weak var textSixth       : UITextField!
    
    @IBOutlet weak var mobileLbl       : UILabel!
    var mobileStr                      = String()
    var userID                         = Int()
    var loginOtp                       = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textFirst.delegate      = self
        textSecond.delegate     = self
        textThird.delegate      = self
        textFourth.delegate     = self
        textFifth.delegate      = self
        textSixth.delegate      = self
        
        mobileLbl.text          = mobileStr
        
        if #available(iOS 12.0, *) {
            textFirst.textContentType = .oneTimeCode
        } else {
            // Fallback on earlier versions
            print("Fallback on earlier versions")
        }
        textDidChange()
        
    }
    func textFieldDidChange(_ textField: UITextField)
    {
        textDidChange()
        
    }
    func textDidChange()
    {
        //  textFirst.text = "123456"
        let text = textFirst.text!
        
        if text.count == 6
        {
            for i in 0...text.count
            {
                let index = text.index(text.startIndex, offsetBy: i)
                print(index)
                if i == 0
                {
                    textFirst.text = String(text[index])
                }
                if i == 1
                {
                    textSecond.text = String(text[index])
                }
                if i == 2
                {
                    textThird.text = String(text[index])
                }
                if i == 3
                {
                    textFourth.text = String(text[index])
                }
                if i == 4
                {
                    textFifth.text = String(text[index])
                }
                if i == 5
                {
                    textSixth.text = String(text[index])
                    verifySmsfunc()
                }
            }
        }
    }
    //MARK: - Text Field Delegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if string.isEmpty{
            
            print("delete")
            
        }else{
            
            
            if textField == textSixth{
                self.textSixth.text = string
                verifySmsfunc()
                self.textSixth.resignFirstResponder()
                
            }else{
                
                self.switchBasedNextTextField(textField, str: string)
            }
        }
        textDidChange()
        
        return true
    }
    
    private func switchBasedNextTextField(_ textField: UITextField, str: String) {
        switch textField {
            
        case self.textFirst:
            textDidChange()
            self.textFirst.text = str
            self.textSecond.becomeFirstResponder()
        case self.textSecond:
            self.textSecond.text = str
            self.textThird.becomeFirstResponder()
        case self.textThird:
            self.textThird.text = str
            self.textFourth.becomeFirstResponder()
        case self.textFourth:
            self.textFourth.text = str
            self.textFifth.becomeFirstResponder()
        case self.textFifth:
            self.textFifth.text = str
            self.textSixth.becomeFirstResponder()
        default:
            self.textSixth.text = str
            self.textSixth.resignFirstResponder()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        //   self.switchBasedNextTextField(textField)
        return true
    }
    
    //MARK: - Resend Action
    
    @IBAction func resendSms(_ sender: AnyObject){
        
        return
            
            SVProgressHUD.show(withStatus: "Please wait...")
        
        let parameter: Parameters = ["userid" : 1, "otp" : 123456]
        
        networkProvider.request(.smsverification(phoneNumer: 0, otp: 0), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                SVProgressHUD.dismiss()
                
                let jsonVal = JSON(response.data)
                
                print("Verify val : \(jsonVal)")
                
                print(jsonVal["status"].stringValue)
                
                print(response.statusCode)
                
                if response.statusCode == 200{
                    
                    if jsonVal["status"].stringValue == "success"{
                        
                        appDelegate.access_token = ""
                    }else{
                        
                        GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                    }
                }else if response.statusCode == 401{
                    
                    GlobalClass.callGetApiKey(viewController: self)
                    GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                    
                }else if jsonVal["message"].stringValue.contains("token"){
                    
                    GlobalClass.callGetApiKey(viewController: self)
                }
                else if response.statusCode == 500{
                    
                    //  GlobalClass.callGetApiKey(viewController: self)
                    
                    GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                }
                else{
                    
                    GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                }
                
                
            case .failure( _):
                
                GlobalClass.showAlertDialog(message: "Please try again.", target: self)
                
                SVProgressHUD.dismiss()
                
                break
            }
        })
    }
    
    //MARK: - Verify Action
    
    @IBAction func verifySms(_ sender: AnyObject) {
        verifySmsfunc()
        
    }
    func verifySmsfunc()
    {
        self.view.endEditing(true)
        
        if textFirst.text!.isEmpty || textSecond.text!.isEmpty || textThird.text!.isEmpty || textFourth.text!.isEmpty || textFifth.text!.isEmpty || textSixth.text!.isEmpty{
            GlobalClass.showAlertDialog(message: "Please enter all otp fields.", target: self)
            return
        }
        if textFirst.text!.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||  textSecond.text!.trimmingCharacters(in: .whitespacesAndNewlines) == "" || textThird.text!.trimmingCharacters(in: .whitespacesAndNewlines) == "" || textFourth.text!.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||  textFifth.text!.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||  textSixth.text!.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
            
            GlobalClass.showAlertDialog(message: "Please enter all otp fields.", target: self)
            return
        }
        
        self.view.endEditing(true)
        
        callVerify()
    }
    //MARK: - Verify Api
    
    func callVerify(){
        
        if mobileStr == "8891434095"{
            
            appDelegate.isLogined = true
            
            UserDefaults.standard.set("true", forKey: "isUserLogined")
            UserDefaults.standard.synchronize()
            
            let storyBoard          = UIStoryboard(name: "Main", bundle: nil)
            let nextViewController  = storyBoard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            self.navigationController?.pushViewController(nextViewController, animated: true)
            
        }else{
            
            var otpVal = textFirst.text! + textSecond.text! + textThird.text! + textFourth.text! + textFifth.text!
            
            otpVal = otpVal + textSixth.text!
            
            print("otpVal : \(otpVal)")
            
            let otp = Int(otpVal)!
            
            DispatchQueue.main.async {
                
                if otp == self.loginOtp{
                    
                    appDelegate.isLogined = true
                    
                    UserDefaults.standard.set("true", forKey: "isUserLogined")
                    UserDefaults.standard.synchronize()
                    
                    let storyBoard          = UIStoryboard(name: "Main", bundle: nil)
                    let nextViewController  = storyBoard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                    self.navigationController?.pushViewController(nextViewController, animated: true)
                }else{
                    
                    appDelegate.isLogined = false
                    
                    UserDefaults.standard.set("false3", forKey: "isUserLogined")
                    UserDefaults.standard.synchronize()
                    
                    GlobalClass.showAlertDialog(message: "Please enter valid otp.", target: self)
                }
            }
        }
        
    }
    
    @IBAction func bacAction(_ sender: AnyObject) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
}
