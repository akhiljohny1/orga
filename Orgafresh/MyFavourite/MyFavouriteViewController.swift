//
//  MyFavouriteViewController.swift
//  Orgafresh
//
//  Copyright © 2020 Innovation Incubator. All rights reserved.
//

import UIKit
import SwiftyJSON

class FavouriteCell: UICollectionViewCell{
    
    @IBOutlet weak var PriceLabel: UILabel!
    @IBOutlet weak var DescriptionLabel: UILabel!
    
    @IBOutlet weak var gradientVwTopConstrain    : NSLayoutConstraint!
    @IBOutlet weak var gradientVwHeightConstrain : NSLayoutConstraint!
    
    @IBOutlet weak var sellingPriceLabel: UILabel!
    @IBOutlet weak var CloseButton: UIButton!
    @IBOutlet weak var unitLabel: UILabel!
    @IBOutlet weak var burView: UIView!
    @IBOutlet weak var availabilityLabel: UILabel!

    
}
class MyFavouriteViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var CartButton: SSBadgeButton!
    
    @IBOutlet weak var TitleLabel: UILabel!
    
    @IBOutlet weak var FavoriteCollectionView: UICollectionView!
    
    var vals2 = [JSON]()
    
    @IBOutlet weak var collectionVBottomConstarint: NSLayoutConstraint!
    @IBOutlet weak var gradientVwTopConstrain    : NSLayoutConstraint!
    @IBOutlet weak var gradientVwHeightConstrain : NSLayoutConstraint!
    @IBOutlet var noItemsUIView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if CartCount == "0"
                {
                    CartButton.badge = ""
                    CartButton.badgeLabel.backgroundColor = UIColor.clear
                }
                else
                {
          CartButton.badge = "\(CartCount)"
          }
        if DeviceType.IS_IPHONE_X || DeviceType.IS_IPHONE_XP{
            
            gradientVwTopConstrain.constant = 0
            gradientVwHeightConstrain.constant = 55
            
        }else{
            
            gradientVwTopConstrain.constant = 0
            gradientVwHeightConstrain.constant = 55
            collectionVBottomConstarint.constant = 260
        }
        
       retriveData()
      
itemsWarningDisplay()
        
    }
    func retriveData()
    {
    
        
        let vals = UserDefaults.standard.value(forKey: "myfavourite") as Any
               let retrivedVals = JSON(vals)
                       
               for i in 0 ..< retrivedVals.count{
                   
                   vals2.append(retrivedVals[i])
               }
               print("vals2 : \(vals2)")

    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return vals2.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let favouriteCell = FavoriteCollectionView.dequeueReusableCell(withReuseIdentifier: "FavouriteCell", for: indexPath) as! FavouriteCell
        favouriteCell.CloseButton.tag = indexPath.row
        
        favouriteCell.PriceLabel.text =  "₹" + vals2[indexPath.row]["normal_price"].stringValue
        favouriteCell.DescriptionLabel.text =   vals2[indexPath.row]["product_name"].stringValue
        favouriteCell.sellingPriceLabel.text =  "₹" + vals2[indexPath.row]["selling_price"].stringValue
         favouriteCell.unitLabel.text =  "/" + vals2[indexPath.row]["minimum_order_quantity"].stringValue + " " + vals2[indexPath.row]["unit_name"].stringValue
        
        
        favouriteCell.PriceLabel.attributedText = favouriteCell.PriceLabel.text!.strikeThrough()
        let imgVw = favouriteCell.viewWithTag(1) as! UIImageView
        var str = imageBaseUrl + vals2[indexPath.row]["product_img"].stringValue
                   
        str = str.replacingOccurrences(of: " ", with: "%20")
        imgVw.sd_setImage(with: URL(string: str), placeholderImage: nil)
        if vals2[indexPath.row]["availability"].intValue == 0
              {
                  favouriteCell.availabilityLabel.isHidden = false
                  favouriteCell.burView.isHidden = false
              }
              else{
                  favouriteCell.availabilityLabel.isHidden = true
                  favouriteCell.burView.isHidden = true
              }
        return favouriteCell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
         if vals2[indexPath.row]["availability"].intValue != 0
              {
                  let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
                  let ProductDetailsVC  = storyBoard.instantiateViewController(withIdentifier: "ProductDetailsViewController") as! ProductDetailsViewController
                  
                  ProductDetailsVC.productValues = vals2[indexPath.row]
                  self.navigationController?.pushViewController(ProductDetailsVC, animated: true)
              }
              else{
                  GlobalClass.showAlertDialog(message: "Product is out of stock", target: self)
              }
    }
    
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let numberOfCellInRow : Int        = 2
        let padding : Int                  = 2
        let collectionCellWidth : CGFloat  = (self.FavoriteCollectionView.frame.size.width/CGFloat(numberOfCellInRow)) - CGFloat(padding)
        
        return CGSize(width: collectionCellWidth, height: collectionCellWidth)
    }
    
    
    @IBAction func BackAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func AddCartAction(_ sender: Any) {
        if CartCount == "0"
                  {
                   GlobalClass.showAlertDialog(message: "You dont have any item on your cart", target: self)

               }
               else{
        let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
        let MyOrderViewController  = storyBoard.instantiateViewController(withIdentifier: "CartListViewController") as! CartListViewController
        self.navigationController?.pushViewController(MyOrderViewController, animated: false)
        }
    }
    @IBAction func NotificationAction(_ sender: Any) {
        let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
        let NotificationViewController  = storyBoard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(NotificationViewController, animated: false)
    }
    @IBAction func closeAction(_ sender: UIButton) {
        // GlobalClass.showAlertDialog(message: "Delete", target: self)
        // create the alert
        let alert = UIAlertController(title: "Delete", message: "Are you sure want to delete \(vals2[sender.tag]["product_name"].stringValue) from your favourite list", preferredStyle: UIAlertController.Style.alert)
        
        // add the actions (buttons)
        
        alert.addAction(UIAlertAction(title: "NO", style: UIAlertAction.Style.cancel, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
        
        
        alert.addAction(UIAlertAction(title: "YES", style: UIAlertAction.Style.default, handler: { action in
            
            // do something like...
            self.vals2.remove(at: sender.tag)
            UserDefaults.standard.setValue(JSON(self.vals2).rawValue, forKey: "myfavourite")
            UserDefaults.standard.synchronize()
            self.FavoriteCollectionView.reloadData()
            self.itemsWarningDisplay()

        }))
    }
    func itemsWarningDisplay()
    {
        if vals2.count == 0
              {
                  self.FavoriteCollectionView.isHidden = true
                  noItemsUIView.isHidden = false
              }
              else
              {
                  self.FavoriteCollectionView.isHidden = false
                  noItemsUIView.isHidden = true
              }
    }
    
}
