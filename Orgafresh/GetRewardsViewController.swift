//
//  GetRewardsViewController.swift
//  Orgafresh
//
//  Copyright © 2020 Innovation Incubator. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD

class GetRewardsViewController: UIViewController {
var referralurl = ""
    var referralcode = ""
    @IBOutlet var referalCodeLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

getreferral()
        
    }
    @IBAction func inviteFriendsAction(_ sender: Any) {
        self.showShareActivity(msg: referralurl, image: nil, url: nil, sourceRect: nil)

    }
    
    @IBAction func BackAction(_ sender: Any) {
             
             self.navigationController?.popViewController(animated: true)
         }
    func getreferral(){
        
        SVProgressHUD.show(withStatus: "Loading...")
        
        networkProvider.request(.getreferral, completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                SVProgressHUD.dismiss()
                
                let jsonVal = JSON(response.data)
                
                print("Json val : \(jsonVal)")
                
                print(jsonVal["status"].stringValue)
                
                if response.statusCode == 200{
                    
                    if jsonVal["status"].stringValue == "success"{
                        
                        self.referralurl = jsonVal["referralurl"].stringValue
                        self.referralcode = jsonVal["referralcode"].stringValue
                        self.referalCodeLabel.text = jsonVal["referralcode"].stringValue + "  "

                    }else{
                        
                        GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                        self.navigationController?.popViewController(animated: true)

                    }
                    
                }else if response.statusCode == 401{

                    GlobalClass.callGetApiKey(viewController: self)
                    GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                    self.navigationController?.popViewController(animated: true)

                    
                }else if jsonVal["message"].stringValue.contains("token"){

                    GlobalClass.callGetApiKey(viewController: self)
                    self.navigationController?.popViewController(animated: true)

                }
                else if response.statusCode == 500{

                    GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                    self.navigationController?.popViewController(animated: true)

                }
                else{

                    GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                    self.navigationController?.popViewController(animated: true)

                }
                
                
                
            case .failure( _):
                self.navigationController?.popViewController(animated: true)

                SVProgressHUD.dismiss()
                
                break
            }
        })
    }
    func topViewController()-> UIViewController{
        var topViewController:UIViewController = UIApplication.shared.keyWindow!.rootViewController!

        while ((topViewController.presentedViewController) != nil) {
            topViewController = topViewController.presentedViewController!;
        }

        return topViewController
    }
    func showShareActivity(msg:String?, image:UIImage?, url:String?, sourceRect:CGRect?){
        var objectsToShare = [AnyObject]()

        if let url = url {
            objectsToShare = [url as AnyObject]
        }

        if let image = image {
            objectsToShare = [image as AnyObject]
        }

        if let msg = msg {
            objectsToShare = [msg as AnyObject]
        }

        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        activityVC.modalPresentationStyle = .popover
        activityVC.popoverPresentationController?.sourceView = topViewController().view
        if let sourceRect = sourceRect {
            activityVC.popoverPresentationController?.sourceRect = sourceRect
        }

        topViewController().present(activityVC, animated: true, completion: nil)
    }
}
