//
//  FAQViewController.swift
//  Orgafresh
//
//  Copyright © 2020 Innovation Incubator. All rights reserved.
//

import UIKit
import SVProgressHUD
import SwiftyJSON


class FAQCell: UITableViewCell{
    @IBOutlet weak var QuestionLabel: UILabel!
    @IBOutlet weak var DescriptionLabel: UILabel!
    @IBOutlet weak var BackView: UIView!
    @IBOutlet weak var topdescriptionconstriant: NSLayoutConstraint!
    @IBOutlet weak var bottonViewConstaint: NSLayoutConstraint!
    
}
class FAQViewController: UIViewController, UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var FeedbackTableView: UITableView!
    @IBOutlet weak var CartButton: SSBadgeButton!
    
    @IBOutlet weak var bottomTabeConstarint: NSLayoutConstraint!
    @IBOutlet weak var gradientVwTopConstrain    : NSLayoutConstraint!
    @IBOutlet weak var gradientVwHeightConstrain : NSLayoutConstraint!
    var faqValues = JSON()
    var opened = false
    var selectedIndex = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if CartCount == "0"
        {
            CartButton.badge = ""
            CartButton.badgeLabel.backgroundColor = UIColor.clear
        }
        else
        {
            CartButton.badge = "\(CartCount)"
        }
        if DeviceType.IS_IPHONE_X || DeviceType.IS_IPHONE_XP{
            
            gradientVwTopConstrain.constant = 0
            gradientVwHeightConstrain.constant = 55
            
        }else{
            
            gradientVwTopConstrain.constant = 0
            gradientVwHeightConstrain.constant = 55
            bottomTabeConstarint.constant = 260
        }
        
        FeedbackTableView.delegate = self
        FeedbackTableView.dataSource = self
        FeedbackTableView.estimatedRowHeight = 80
        FeedbackTableView.rowHeight = UITableView.automaticDimension
        
        getFAQ()
        // Do any additional setup after loading the view.
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return faqValues.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let feedbackcell:FAQCell = FeedbackTableView.dequeueReusableCell(withIdentifier: "FAQCell") as! FAQCell
        feedbackcell.QuestionLabel.text = "  " + faqValues[indexPath.row]["question"].stringValue.firstUppercased
        
        if opened == false{
            feedbackcell.DescriptionLabel.text = ""
            feedbackcell.QuestionLabel.backgroundColor = UIColor.black
            feedbackcell.BackView.backgroundColor = UIColor.black
            feedbackcell.topdescriptionconstriant.constant = 0
            feedbackcell.bottonViewConstaint.constant = 5

        }
        else{
            if indexPath.row == selectedIndex
            {
                feedbackcell.DescriptionLabel.text = "  " +  faqValues[indexPath.row]["message"].stringValue.firstUppercased
                feedbackcell.QuestionLabel.backgroundColor = UIColor.darkGray
                feedbackcell.BackView.backgroundColor = hexStringToUIColor(hex: "#191919")
                feedbackcell.topdescriptionconstriant.constant = 10
                feedbackcell.bottonViewConstaint.constant = 10


            }
        }
        return feedbackcell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        if opened == false{
            opened = true
            
        }//opened true
        else
        {
            if selectedIndex != indexPath.row
            {
                opened = true
            }
            else
            {
                opened = false
            }
        }
        selectedIndex = indexPath.row
        
        tableView.reloadData()
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    @IBAction func BackButtonAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func AddCartAction(_ sender: Any) {
        if CartCount == "0"
        {
            GlobalClass.showAlertDialog(message: "You dont have any item on your cart", target: self)
            
        }
        else{
            let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
            let MyOrderViewController  = storyBoard.instantiateViewController(withIdentifier: "CartListViewController") as! CartListViewController
            self.navigationController?.pushViewController(MyOrderViewController, animated: false)
        }
    }
    @IBAction func NotificationAction(_ sender: Any) {
        let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
        let NotificationViewController  = storyBoard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(NotificationViewController, animated: false)
    }
    func getFAQ(){
        
        SVProgressHUD.show(withStatus: "Loading...")
        
        networkProvider.request(.getallfaq, completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                SVProgressHUD.dismiss()
                
                let jsonVal = JSON(response.data)
                
                print("Json val : \(jsonVal)")
                
                print(jsonVal["status"].stringValue)
                
                if response.statusCode == 200{
                    
                    if jsonVal["status"].stringValue == "success"{
                        
                        self.faqValues = jsonVal["faq"]
                        
                        
                    }else{
                        
                        GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                    }
                    
                }else if response.statusCode == 401{
                    
                    GlobalClass.callGetApiKey(viewController: self)
                    GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                    
                }else if jsonVal["message"].stringValue.contains("token"){
                    
                    GlobalClass.callGetApiKey(viewController: self)
                }
                else if response.statusCode == 500{
                    
                    GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                }
                else{
                    
                    GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                }
                
                
                self.FeedbackTableView.reloadData()
                
            case .failure( _):
                
                SVProgressHUD.dismiss()
                
                break
            }
        })
    }
    
}
