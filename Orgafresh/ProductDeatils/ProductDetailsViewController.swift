//
//  ProductDetailsViewController.swift
//  Orgafresh
//
//  Copyright © 2020 Innovation Incubator. All rights reserved.
//

import UIKit
import Alamofire
import Moya
import SwiftyJSON
import SVProgressHUD
import SDWebImage
var TempCartValues = [JSON]()
var CartArray = JSON()
class ProductDetailsViewController: UIViewController {
    
    @IBOutlet weak var ProductNameLabel: UILabel!
    @IBOutlet weak var DescriptionLabel: UILabel!
    
    @IBOutlet weak var SellingPriceLabel: UILabel!
    @IBOutlet weak var NormalPriceLabel: UILabel!
    @IBOutlet weak var ProductImageView: ScaledHeightImageView!
    
    @IBOutlet weak var ProductTitleLabel: UILabel!
    var productValues           = JSON()
    
    @IBOutlet weak var AddButton: UIButton!
    
    @IBOutlet weak var DecrenmentValue: UIButton!
    
    @IBOutlet weak var CartValue: UIButton!
    @IBOutlet weak var IncrenmentValue: UIButton!
    
    @IBOutlet weak var CartButton: SSBadgeButton!
    
    private var counterValue = 1
    
    @IBOutlet weak var productImageHeightConstaint: NSLayoutConstraint!
    
    @IBOutlet weak var gradientVwTopConstrain    : NSLayoutConstraint!
    @IBOutlet weak var gradientVwHeightConstrain : NSLayoutConstraint!
    
    @IBOutlet weak var backVwHeightConstrain     : NSLayoutConstraint!
    @IBOutlet weak var scrollVwHeightConstrain   : NSLayoutConstraint!
    
    @IBOutlet weak var addButtTop                : NSLayoutConstraint!
    @IBOutlet weak var viewTop                   : NSLayoutConstraint!
    @IBOutlet weak var totalPriceLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if DeviceType.IS_IPHONE_X || DeviceType.IS_IPHONE_XP{
            gradientVwTopConstrain.constant = 43
            gradientVwHeightConstrain.constant = 55
         //   productImageHeightConstaint.constant = 291


        }else{
           gradientVwTopConstrain.constant = 20
            gradientVwHeightConstrain.constant = 55
          //  productImageHeightConstaint.constant = 240
        }
        ProductTitleLabel.text = productValues["product_name"].stringValue
        ProductNameLabel.text = productValues["product_name"].stringValue
        DescriptionLabel.text = productValues["product_desc"].stringValue
        NormalPriceLabel.text = "₹" + productValues["normal_price"].stringValue + "/" + productValues["minimum_order_quantity"].stringValue + " " + productValues["unit_name"].stringValue
        totalPriceLabel.text = "(Total Price: " + productValues["selling_price"].stringValue + ")"
        
        SellingPriceLabel.text = "₹" + productValues["selling_price"].stringValue +
            "/" + productValues["minimum_order_quantity"].stringValue + " " + productValues["unit_name"].stringValue
        
        var str = imageBaseUrl + productValues["product_img"].stringValue
        str = str.replacingOccurrences(of: " ", with: "%20")
        ProductImageView.sd_setImage(with: URL(string: str), placeholderImage: #imageLiteral(resourceName: "loginVector"))
        
        
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: NormalPriceLabel.text!)
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
        
        NormalPriceLabel.attributedText = attributeString
        //  getProductByCategory()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        reteriveTempData()
    }
    
    func getProductByCategory() {
        
        SVProgressHUD.show(withStatus: "Loading...")
        
        networkProvider.request(.getprodbycat(id: productValues["cat_id"].intValue), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                SVProgressHUD.dismiss()
                
                let jsonVal = JSON(response.data)
                
                print("Json val : \(jsonVal)")
                
                print(jsonVal["status"].stringValue)
                
                if response.statusCode == 200{
                    
                    if jsonVal["status"].stringValue == "success"{
                        
                        
                    }else{
                        
                        GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                    }
                    
                }else if response.statusCode == 401{
                    
                    GlobalClass.callGetApiKey(viewController: self)
                    GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                    
                }else if jsonVal["message"].stringValue.contains("token"){
                    
                    GlobalClass.callGetApiKey(viewController: self)
                }
                else if response.statusCode == 500{
                    
                    GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                }
                else{
                    
                    GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                }
                
            case .failure( _):
                
                SVProgressHUD.dismiss()
                
                break
            }
        })
    }
    
    
    @IBAction func AddCartValues(_ sender: UIButton) {
        if productValues["availability"].intValue == 0
            {
            GlobalClass.showAlertDialog(message: "Product is out of stock", target: self)

            }
            else{
               cartadd()
            }
       
        
    }
    func cartadd()
    {
        
        CartArray["product_name"] = JSON(productValues["product_name"].stringValue)
        CartArray["normal_price"] = JSON(productValues["normal_price"].stringValue)
        CartArray["selling_price"] = JSON(productValues["selling_price"].stringValue)
        CartArray["quantity"] = JSON(counterValue)
        
        CartArray["minimum_order_quantity"] = JSON(productValues["minimum_order_quantity"].stringValue)
        CartArray["unit_name"] = JSON(productValues["unit_name"].stringValue)
        CartArray["product_img"] = JSON(productValues["product_img"].stringValue)
        
        CartArray["id"] = JSON(productValues["id"].stringValue)
        var valsVal = -1
        
        for i in 0 ..< TempCartValues.count{
            
            let val = TempCartValues[i]
            
            if val["id"].stringValue == CartArray["id"].stringValue{
                
                // TempCartValues.remove(at: i)
                valsVal = i
                
            }
        }
        if valsVal != -1{
            
            TempCartValues.remove(at: valsVal)
        }
        
        GlobalClass.showAlertDialog(message: "Your Item has been added to Cart.", target: self)
        
        TempCartValues.append(CartArray)
        print(TempCartValues)
        UserDefaults.standard.setValue(JSON(TempCartValues).rawValue, forKey: "TempCartValues")
        UserDefaults.standard.synchronize()
        
        reteriveTempData()
    }
    
    
    func reteriveTempData()
    {
        
        let vals = UserDefaults.standard.value(forKey: "TempCartValues") as Any
        
        // print("vals : \(JSON(vals))")
        let retrivedVals = JSON(vals)
        
        TempCartValues.removeAll()
        
        for i in 0 ..< retrivedVals.count{
            TempCartValues.append(retrivedVals[i])
        }
        if TempCartValues.count == 0
        {
            CartButton.badge = ""
            CartButton.badgeLabel.backgroundColor = UIColor.clear
        }
        else
        {
            CartButton.badge = "\(TempCartValues.count)"
        }
        CartCount = "\(TempCartValues.count)"
        UserDefaults.standard.setValue(JSON("\(TempCartValues.count)").rawValue, forKey: "CartCount")
        UserDefaults.standard.synchronize()
        
    }
    
    //MARK: Button Action
    
    @IBAction func BackAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func AddCartAction(_ sender: Any) {
        if CartCount == "0"
        {
            GlobalClass.showAlertDialog(message: "You dont have any item on your cart", target: self)
            
        }
        else{
            let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
            let MyOrderViewController  = storyBoard.instantiateViewController(withIdentifier: "CartListViewController") as! CartListViewController
            self.navigationController?.pushViewController(MyOrderViewController, animated: false)
        }
    }
    @IBAction func NotificationAction(_ sender: Any) {
        let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
        let NotificationViewController  = storyBoard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(NotificationViewController, animated: false)
    }
    
    @IBAction func DecrementAction(_ sender: UIButton) {
        if(counterValue != 1){
            counterValue -= 1;
        }
        print(counterValue)
        CartValue.setTitle("\(counterValue)", for: .normal)
        
        let total = (Int(productValues["selling_price"].stringValue) ?? 0) * counterValue
        totalPriceLabel.text = "(Total Price: " + "\(total)" + ")"

    }
    @IBAction func IncrenmentAction(_ sender: UIButton) {
        if counterValue >= 5
        {
            GlobalClass.showAlertDialog(message: "You have reached Maximum Limit", target: self)
        }else
        {
            counterValue += 1;
            print(counterValue)
            CartValue.setTitle("\(counterValue)", for: .normal)
            let total = (Int(productValues["selling_price"].stringValue) ?? 0) * counterValue
            totalPriceLabel.text = "(Total Price: " + "\(total)" + ")"
        }
    }
    
    
    
}
