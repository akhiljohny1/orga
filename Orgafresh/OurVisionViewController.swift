//
//  OurVisionViewController.swift
//  Orgafresh
//
//  Copyright © 2020 Innovation Incubator. All rights reserved.
//

import UIKit

class OurVisionViewController: UIViewController {
    
    @IBOutlet weak var gradientVwTopConstrain    : NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if DeviceType.IS_IPHONE_X || DeviceType.IS_IPHONE_XP{
            
            gradientVwTopConstrain.constant = 0
            
        }else{
            
            gradientVwTopConstrain.constant = 0
            
        }
        
    }
    
    
    @IBAction func BackAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
}
