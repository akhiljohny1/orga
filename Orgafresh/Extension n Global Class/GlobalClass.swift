//
//  Helpers.swift
//  RevitsoneSample
//
//  Created by Innovation Incubator on 14/05/19.
//  Copyright © 2019 Innovation Incubator. All rights reserved.
//

import UIKit
import SwiftyJSON
import Moya
import Alamofire
import SVProgressHUD
import Foundation
import SystemConfiguration

struct ScreenSize {

static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct DeviceType {
static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
static let IS_IPHONE_X          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_HEIGHT == 812.0
static let IS_IPHONE_XP         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_HEIGHT > 812.0

// static let IS_IPHONE_XR         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_HEIGHT > 896.0

static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
}

struct Connectivity {
  static let sharedInstance = NetworkReachabilityManager()!
  static var isConnectedToInternet:Bool {
      return self.sharedInstance.isReachable
    }
}

class GlobalClass: NSObject {

//MARK: - Get Api Key
class func getuserProfileFunc(viewController: UIViewController){
    
    SVProgressHUD.show(withStatus: "Loading...")
    
    let parameter: Parameters = ["Token" : appDelegate.access_token]
    print("noti params : \(parameter)")
    
    networkProvider.request(.getuserprofile(parameter: parameter), completion: {(result) in
        
        switch result {
            
        case .success(let response):
            SVProgressHUD.dismiss()
            
            let jsonVal = JSON(response.data)
            
            print("Json val : \(jsonVal)")
            
            print(jsonVal["status"].stringValue)
            
            if jsonVal["status"].stringValue == "success"{
                
                let userProfileValues = jsonVal["userprofile"]
                
                appDelegate.UserName = userProfileValues["name"].stringValue
                appDelegate.loginEmailAddress = userProfileValues["email"].stringValue
                appDelegate.loginMobileNumber = userProfileValues["phone_number"].stringValue
                
                if userProfileValues["phone_number"].stringValue.isEmpty
                {
                    return
                }
                else
                {
                    appDelegate.loginMobileNumber = userProfileValues["phone_number"].stringValue
                }
                if userProfileValues["email"].stringValue.isEmpty
                {
                    return
                }
                else
                {
                    appDelegate.loginEmailAddress = userProfileValues["email"].stringValue
                }
                //
            }
            
        case .failure( _):
            SVProgressHUD.dismiss()
            
            break
        }
    })
}




class func getwalletfunc(viewController: UIViewController){
    
    var totalDebit = Double()
    var totalCredit = Double()
    SVProgressHUD.show(withStatus: "Loading...")
    
    networkProvider.request(.getwallet, completion: {(result) in
        
        switch result {
            
        case .success(let response):
            
            SVProgressHUD.dismiss()
            
            let jsonVal = JSON(response.data)
            
            if response.statusCode == 200{
                
                appDelegate.totalCredit_  = 0
                appDelegate.totalDebit_   = 0
                appDelegate.TotalBalance  = 0
                
                if jsonVal["status"].stringValue == "success"{
                    
                    let walletValues = jsonVal["wallet"]
                    print("Wallet balance is :",walletValues)
                    for i in 0 ... walletValues.count
                    {
                        if walletValues[i]["transaction_type"].stringValue == "credit"
                        {
                            totalCredit += Double(walletValues[i]["amount"].stringValue) ?? 0
                            
                        }
                        else
                        {
                            totalDebit += Double(walletValues[i]["amount"].stringValue) ?? 0
                            
                        }
                        
                    }
                    appDelegate.totalCredit_  = totalCredit
                    appDelegate.totalDebit_   = totalDebit
                    appDelegate.TotalBalance += appDelegate.totalCredit_
                    appDelegate.TotalBalance -= appDelegate.totalDebit_
                    
                }else{
                    
                    GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: viewController)
                }
                
            }else if response.statusCode == 401{
                
                GlobalClass.callGetApiKey(viewController: viewController)
                GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: viewController)
                
            }else if jsonVal["message"].stringValue.contains("token"){
                
                GlobalClass.callGetApiKey(viewController: viewController)
            }
            else if response.statusCode == 500{
                
                GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: viewController)
            }
            else{
                
                GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: viewController)
            }
            
            
        case .failure( _):
            
            SVProgressHUD.dismiss()
            
            break
        }
    })
}





class func callGetApiKey(viewController: UIViewController){
    
    let parameter: Parameters = ["grant_type" : "client_credentials", "client_id" : 1, "client_secret" : "tKhCdAFdtA6JNObaomrk2gzXgQ0Q9N2MiLQh2Ua2"]
    
    networkProvider.request(.getappkey(parameter: parameter), completion: {(result) in
        
        switch result {
            
        case .success(let response):
            
            let jsonVal = JSON(response.data)
            
            print("Json val : \(jsonVal)")
            
            print(jsonVal["status"].stringValue)
            
            if jsonVal["status"].stringValue == "success"{
                
                UserDefaults.standard.setValue(jsonVal["token"].stringValue, forKey: "access_token")
                
                UserDefaults.standard.synchronize()
                
            }else{
                
                GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: viewController)
            }
            
        case .failure( _):
            
            SVProgressHUD.dismiss()
            
            break
        }
    })
}

class func navigationTitleLeft(txt:String)->UIBarButtonItem {
    
    let longTitleLabel = UILabel()
    longTitleLabel.text = txt
    longTitleLabel.textColor = .black
    longTitleLabel.font = UIFont.boldSystemFont(ofSize: 20)
    longTitleLabel.sizeToFit()
    
    let leftItem = UIBarButtonItem(customView: longTitleLabel)
    return leftItem
    
}

class func showAlertDialog(message: String, target: UIViewController) {
    
    if message != "Success Alert"
    {
        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertController.Style.alert)
        
        print(message)
        if message == "Token is not valid"  {
            
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: { _ -> Void in
                
            }))
        } else if message == "The Internet connection appears to be offline." || message == "The request timed out." {
            
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: { _ -> Void in
                
                
            }))
        }
        else {
            
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: {(alert: UIAlertAction!) in }))
        }
        target.present(alert, animated: true, completion: nil)
    }
    else{
        
        let alert = UIAlertController(title: NSLocalizedString("Success", comment: ""), message: "Successfully submit Feedback Form", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: { _ -> Void in
            
        }))
        
        target.present(alert, animated: true, completion: nil)
    }
}

class func validateEmail(enteredEmail:String) -> Bool {
    let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
    return emailPredicate.evaluate(with: enteredEmail)
}

class func validateText(txt:String)-> Bool{
    if txt == "" {
        
        return false
    }
    else{
        return true
    }
}

class func validatePHnumber(enteredNumber:String) -> Bool {
    let phoneNumberRegex = "^[6-9]\\d{9}$"
    let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneNumberRegex)
    let isValidPhone = phoneTest.evaluate(with: enteredNumber)
    return isValidPhone
    
}
class func setAttributedPlaceHolder(placeHolderName:String) -> NSMutableAttributedString{
    
    let yourAttributes: [NSAttributedString.Key: Any] = [.foregroundColor: UIColor.red]
    let yourOtherAttributes: [NSAttributedString.Key: Any] = [.foregroundColor: UIColor.darkGray]
    
    
    let partOne = NSMutableAttributedString(string: placeHolderName, attributes: yourOtherAttributes)
    let partTwo = NSMutableAttributedString(string: " *", attributes: yourAttributes)
    
    partOne.append(partTwo)
    print(partOne)
    return partOne
}

class func nullToNil(value : AnyObject?) -> AnyObject? {
    if value is NSNull {
        return "" as AnyObject
    } else {
        return value
    }
}
class func nullToString(value : AnyObject?) -> String? {
    if value is NSNull {
        return ""
    } else {
        return value as? String
    }
}


class func setImageRoundCorner(img :UIImageView){
    img.layer.cornerRadius = img.frame.size.height/2
    img.layer.masksToBounds = true
}

class func stylistBtnProperties(btn :UIButton){
    btn.layer.cornerRadius = 7
    btn.layer.borderColor = UIColor.lightGray.cgColor
    btn.layer.borderWidth = 0.5
    btn.setTitleColor(UIColor.black, for: .normal)
}

class func stylistBtnHightlight(selectedBtn :UIButton, otherBtn1: UIButton, otherBtn2: UIButton){
    selectedBtn.layer.shadowColor = UIColor.black.cgColor
    selectedBtn.layer.shadowOpacity = 5.3
    selectedBtn.layer.shadowOffset = CGSize.zero
    selectedBtn.layer.shadowRadius = 2
    
    otherBtn1.layer.shadowColor = UIColor.black.cgColor
    otherBtn1.layer.shadowOpacity = 0
    otherBtn1.layer.shadowOffset = CGSize.zero
    otherBtn1.layer.shadowRadius = 0
    
    otherBtn2.layer.shadowColor = UIColor.black.cgColor
    otherBtn2.layer.shadowOpacity = 0
    otherBtn2.layer.shadowOffset = CGSize.zero
    otherBtn2.layer.shadowRadius = 0
    
}
class func convertToBase64(image :UIImage) -> String{
    
    let imageData:NSData = image.pngData()! as NSData
    let strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
    
    return strBase64
}

class func shadowView(view: UIView){
    view.layer.shadowColor = UIColor.black.cgColor
    view.layer.shadowOpacity = 0.3
    view.layer.shadowOffset = CGSize.zero
    view.layer.shadowRadius = 3
    
}
class func  buttonShadow(btn :UIButton){
    btn.layer.cornerRadius = 7
    //        btn.layer.borderColor = UIColor.black.cgColor
    //        btn.layer.borderWidth = 0.5
    btn.setTitleColor(UIColor.black, for: .normal)
    btn.layer.shadowColor = UIColor.black.cgColor
    btn.layer.shadowOpacity = 0.4
    btn.layer.shadowOffset = CGSize.zero
    btn.layer.shadowRadius = 3
    
}

}
