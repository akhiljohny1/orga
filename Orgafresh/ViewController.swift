//
//  ViewController.swift
//  Orgafresh
//
//  Created by Innovation Incubator on 20/06/20.
//  Copyright © 2020 Innovation Incubator. All rights reserved.
//

import UIKit
import FirebaseCrashlytics
import Alamofire
import Moya
import SwiftyJSON
import SVProgressHUD

class ViewController: UIViewController{
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        // Do any additional setup after loading the view.
        
        callGetApiKey()
    }
    
    //MARK: - Get Api Key
      
    func callGetApiKey(){
        
        let parameter: Parameters = ["grant_type" : "client_credentials", "client_id" : 1, "client_secret" : "tKhCdAFdtA6JNObaomrk2gzXgQ0Q9N2MiLQh2Ua2"]
        
        print("noti params : \(parameter)")
        
        networkProvider.request(.getappkey(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                let jsonVal = JSON(response.data)
                
                print("Json val : \(jsonVal)")
                
                print(jsonVal["status"].stringValue)
                
                if jsonVal["status"].stringValue == "success"{
                    
                    UserDefaults.standard.setValue(jsonVal["token"].stringValue, forKey: "access_token")
                    
                    UserDefaults.standard.synchronize()
                }
                
                print(UserDefaults.standard.string(forKey: "access_token") ?? "no val")
                
                
            case .failure( _):
                
                break
            }
        })
    }
    
    //MARK: - Signup Action
    
    @IBAction func signUpAction(_ sender: AnyObject) {
     
        let storyBoard          = UIStoryboard(name: "Main", bundle: nil)
        let nextViewController  = storyBoard.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
}

