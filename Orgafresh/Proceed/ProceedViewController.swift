//
//  ProceedViewController.swift
//  Orgafresh
//
//  Copyright © 2020 Innovation Incubator. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps
import SVProgressHUD
import Alamofire
import SwiftyJSON
import Razorpay

class ProceedViewController: UIViewController,UITextFieldDelegate, RazorpayPaymentCompletionProtocol {
    
    var productArray = JSON()
    
    
    var ProductValues = [JSON]()
    
    @IBOutlet weak var nameLabel: UITextField!
    @IBOutlet weak var backVwHeight   : NSLayoutConstraint!
    @IBOutlet weak var CartButton: SSBadgeButton!
    @IBOutlet weak var DeliveryaddressButton: UIButton!
    @IBOutlet weak var PromoTextField: UITextField!
    @IBOutlet weak var EmailTextField: UITextField!
    @IBOutlet weak var moblieLabel: UITextField!
    @IBOutlet weak var MapViewArea: GMSMapView!
    
    @IBOutlet weak var PaymentTypeCash: UIButton!
    @IBOutlet weak var PaymentTypeOnline: UIButton!
    @IBOutlet weak var DeliveryLaterButton: UIButton!
    @IBOutlet weak var DeliveryNowButton: UIButton!
    @IBOutlet weak var MoriningButton: UIButton!
    @IBOutlet weak var EveningButton: UIButton!
    
    @IBOutlet weak var deliveryVw: UIView!
    @IBOutlet weak var deliveryVwHeight: NSLayoutConstraint!
    @IBOutlet weak var placeOrderBackgroundView: UIView!
    @IBOutlet weak var placeOrderBackButton: UIButton!
    
    @IBOutlet weak var picker                          : UIDatePicker!
    
    @IBOutlet weak var gradientVwTopConstrain    : NSLayoutConstraint!
    @IBOutlet weak var gradientVwHeightConstrain : NSLayoutConstraint!
    @IBOutlet weak var toatlCartLabel: UILabel!
    @IBOutlet weak var walletBalancelabel: UILabel!
    @IBOutlet weak var totalPaidLabel: UILabel!
    @IBOutlet weak var walletCheckButton: UIButton!
    
    @IBOutlet weak var totalbalanceLabel: UILabel!
    
    @IBOutlet weak var scrollVw: UIScrollView!
    
    var wallet_Amount = "0"
    //appDelegate.grandToatalValue
    
    var toatlPaidValue = 0.0
    var payment_type = ""
    var delivery_date = ""
    var delivery_time = ""
    
    var placed_orderid = Int()
    var razorpayOrderId = String()
    var razorpay: RazorpayCheckout!
    
    var newWalletAmount = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        razorpay = RazorpayCheckout.initWithKey("rzp_test_clnzwd2O9bSVsc", andDelegate: self) //test lkey
        
        razorpay = RazorpayCheckout.initWithKey("rzp_live_asiZQk2E6w9nJ4", andDelegate: self) //live key
        
        if DeviceType.IS_IPHONE_X || DeviceType.IS_IPHONE_XP{
            
            gradientVwTopConstrain.constant = 43
            gradientVwHeightConstrain.constant = 55
            
        }else{
            
            gradientVwTopConstrain.constant = 23
            gradientVwHeightConstrain.constant = 55
        }
        backVwHeight.constant = 1000
        
        DeliveryNowButton.setImage(UIImage(named: "Circlebutton"), for: .normal)
        DeliveryNowButton.setImage(UIImage(named: "Radiobutton"), for: .selected)
        DeliveryLaterButton.setImage(UIImage(named: "Circlebutton"), for: .normal)
        DeliveryLaterButton.setImage(UIImage(named: "Radiobutton"), for: .selected)
        PaymentTypeOnline.setImage(UIImage(named: "Circlebutton"), for: .normal)
        PaymentTypeOnline.setImage(UIImage(named: "Radiobutton"), for: .selected)
        PaymentTypeCash.setImage(UIImage(named: "Circlebutton"), for: .normal)
        PaymentTypeCash.setImage(UIImage(named: "Radiobutton"), for: .selected)
        MoriningButton.setImage(UIImage(named: "Circlebutton"), for: .normal)
        MoriningButton.setImage(UIImage(named: "Radiobutton"), for: .selected)
        EveningButton.setImage(UIImage(named: "Circlebutton"), for: .normal)
        EveningButton.setImage(UIImage(named: "Radiobutton"), for: .selected)
        walletCheckButton.setImage(UIImage(named: "Squarecheck"), for: .normal)
        walletCheckButton.setImage(UIImage(named: "checkbox"), for: .selected)
        
        walletCheckButton.isSelected = true
        
        DeliveryNowButton.tintColor = #colorLiteral(red: 0.2, green: 0.8, blue: 1, alpha: 1)
        DeliveryLaterButton.tintColor = #colorLiteral(red: 0.2, green: 0.8, blue: 1, alpha: 1)
        
        DeliveryLaterButton.isSelected = false
        DeliveryNowButton.isSelected = true
        PaymentTypeCash.isSelected = false
        PaymentTypeOnline.isSelected = false
        MoriningButton.isSelected = true
        EveningButton.isSelected = false
        
        
        deliveryVwHeight.constant = 0
        deliveryVw.isHidden = true
        
        deliveryVwHeight.constant = 0
        deliveryVw.isHidden = true
        
        dueDatePickerView.isHidden = true
        placeOrderBackgroundView.isHidden = true
        placeOrderBackButton.isHidden = true
        
        picker.minimumDate              = Date()
        
        reteriveTempData()
        delivery_date = Date.getCurrentDate()
        delivery_time = Date.getCurrentTime()
        
        getwalletfunc()
        
    }
    
    func getwalletfunc(){
        
        var totalDebit = Double()
        var totalCredit = Double()
        SVProgressHUD.show(withStatus: "Loading...")
        
        networkProvider.request(.getwallet, completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                SVProgressHUD.dismiss()
                
                let jsonVal = JSON(response.data)
                
                if response.statusCode == 200{
                    
                    appDelegate.totalCredit_  = 0
                    appDelegate.totalDebit_   = 0
                    appDelegate.TotalBalance  = 0
                    
                    if jsonVal["status"].stringValue == "success"{
                        
                        let walletValues = jsonVal["wallet"]
                        print("Wallet balance is :",walletValues)
                        for i in 0 ... walletValues.count
                        {
                            if walletValues[i]["transaction_type"].stringValue == "credit"
                            {
                                totalCredit += Double(walletValues[i]["amount"].stringValue) ?? 0
                                
                            }
                            else
                            {
                                totalDebit += Double(walletValues[i]["amount"].stringValue) ?? 0
                                
                            }
                            
                        }
                        appDelegate.totalCredit_  = totalCredit
                        appDelegate.totalDebit_   = totalDebit
                        appDelegate.TotalBalance += appDelegate.totalCredit_
                        appDelegate.TotalBalance -= appDelegate.totalDebit_
                        
                        self.newWalletAmount = "5"//"\(appDelegate.TotalBalance)"
                        
                        self.wallet_Amount        = "5"//"\(appDelegate.TotalBalance)"
                        self.walletBalancelabel.text = "5"//"\(appDelegate.TotalBalance)"
                        
                    }else{
                        
                        
                    }
                    
                }else if response.statusCode == 401{
                    
                    
                    
                }else if jsonVal["message"].stringValue.contains("token"){
                    
                    
                }
                else if response.statusCode == 500{
                    
                    
                }
                else{
                    
                    
                }
                
                
            case .failure( _):
                
                SVProgressHUD.dismiss()
                
                break
            }
        })
    }
    
    
    @IBOutlet weak var DateTimeTextField: UITextField!
    
    @IBOutlet weak var dueDatePickerView: UIView!
    
    override func viewWillAppear(_ animated: Bool) {
        //SVProgressHUD.show(withStatus: "Loading...")
        // GlobalClass.getuserProfileFunc(viewController: self)
        
        if CartCount == "0"
        {
            CartButton.badge = ""
            CartButton.badgeLabel.backgroundColor = UIColor.clear
        }
        else
        {
            CartButton.badge = "\(CartCount)"
        }
        
        self.MapViewArea.clear()
        let position = CLLocationCoordinate2D(latitude: Selected_AddressValues[0]["loc_latitude"].doubleValue, longitude: Selected_AddressValues[0]["loc_longitude"].doubleValue)
        MapViewArea.camera = GMSCameraPosition.camera(withTarget: position, zoom: 12)
        let marker = GMSMarker(position: position)
        marker.map = self.MapViewArea
        
        
        let address = Selected_AddressValues[0]["addres_line_1"].stringValue.trimmingCharacters(in: .whitespacesAndNewlines) + ", "  + Selected_AddressValues[0]["addres_line_2"].stringValue + ", " + Selected_AddressValues[0]["addres_line_3"].stringValue + ", " + Selected_AddressValues[0]["addres_line_4"].stringValue.trimmingCharacters(in: .whitespacesAndNewlines)
        DeliveryaddressButton.setTitle("\(address)", for: .normal)
        
        
        getuserprofile()
    }
    
    @IBAction func mapAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func MapAddressSelectionAction(_ sender: Any) {
        
    }
    
    //MARK: - Picker Action
    
    @IBAction func onValueChangeTime(_ picker : UIDatePicker) {
        
        
        let str = convertDateAndTime(dat: picker.date)
        
        DateTimeTextField.text = str.components(separatedBy: "at").first! + " " + str.components(separatedBy: "at").last!
        delivery_time = str.components(separatedBy: "at").last!
        
        delivery_date = convertDateAndTime2(dat: picker.date)
    }
    
    @IBAction func showPickerVw(_ sender : UIButton) {
        
        dueDatePickerView.isHidden = false
    }
    
    @IBAction func hidePickerVw(_ sender : UIButton) {
        
        dueDatePickerView.isHidden = true
    }
    
    //MARK: - Convert Date
    
    func convertDateAndTime(dat : Date) -> String {
        
        let dateFormatter          = DateFormatter()
        dateFormatter.dateFormat   = "yyyy-MM-dd HH:mm:ss +0000"
        dateFormatter.dateStyle    = .long
        dateFormatter.timeStyle    = .short
        let val                    = dateFormatter.string(from: dat)
        return val
    }
    
    func convertDateAndTime2(dat : Date) -> String {
        
        let dateFormatter          = DateFormatter()
        dateFormatter.dateFormat   = "yyyy-MM-dd HH:mm:ss +0000"
        dateFormatter.dateStyle    = .long
        dateFormatter.timeStyle    = .none
        
        let dateFormat2            = DateFormatter()
        dateFormat2.dateFormat     = "yyyy-MM-dd"
        
        let val                    = dateFormat2.string(from: dat)
        return val
    }
    
    
    //MARK: - User Profile
    
    func getuserprofile() {
        
        SVProgressHUD.show(withStatus: "Loading...")
        
        let parameter: Parameters = ["Token" : appDelegate.access_token]
        print("noti params : \(parameter)")
        
        networkProvider.request(.getuserprofile(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                SVProgressHUD.dismiss()
                
                let jsonVal = JSON(response.data)
                
                print("Json val : \(jsonVal)")
                
                print(jsonVal["status"].stringValue)
                
                if jsonVal["status"].stringValue == "success"{
                    
                    let userProfileValues = jsonVal["userprofile"]
                    
                    appDelegate.UserName = userProfileValues["name"].stringValue
                    appDelegate.loginEmailAddress = userProfileValues["email"].stringValue
                    appDelegate.loginMobileNumber = userProfileValues["phone_number"].stringValue
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                        
                        if appDelegate.loginEmailAddress.isEmpty || appDelegate.UserName.isEmpty || appDelegate.loginMobileNumber.isEmpty
                        {
                            SVProgressHUD.dismiss()
                            
                            let alert = UIAlertController(title: "Profile", message: "Please update your contact details on Profile before proceeding", preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { action in
                                self.navigationController?.popViewController(animated: false)
                            }))
                            
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { action in
                                let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
                                let MyProfileVC  = storyBoard.instantiateViewController(withIdentifier: "MyProfileViewController") as! MyProfileViewController
                                self.navigationController?.pushViewController(MyProfileVC, animated: false)
                            }))
                            self.present(alert, animated: true, completion: nil)
                            
                        }
                        else
                        {
                            self.EmailTextField.text = "  " + appDelegate.loginEmailAddress
                            self.nameLabel.text = "  " + appDelegate.UserName
                            self.moblieLabel.text = " " + appDelegate.loginMobileNumber
                            
                        }
                        
                    })
                    
                }
                
            case .failure( _):
                SVProgressHUD.dismiss()
                
                break
            }
        })
    }
    
    //MARK: Button Action
    
    @IBAction func DeliveryNowAction(_ sender: Any) {
        
        if let button = sender as? UIButton {
            if button.isSelected {
                // set deselected
                button.isSelected = false
                DeliveryLaterButton.isSelected = true
                delivery_date = Date.getCurrentDate()
                delivery_time = Date.getCurrentTime()
                deliveryVwHeight.constant = 150
                deliveryVw.isHidden = false
                
                backVwHeight.constant = 1150
                
            } else {
                // set selected
                button.isSelected = true
                DeliveryLaterButton.isSelected = false
                
                deliveryVwHeight.constant = 0
                deliveryVw.isHidden = true
                
                backVwHeight.constant = 1000
                
            }
        }
    }
    
    @IBAction func DeliveryLaterAction(_ sender: Any) {
        if let button = sender as? UIButton {
            if button.isSelected {
                // set deselected
                button.isSelected = false
                DeliveryNowButton.isSelected = true
                delivery_date = Date.getCurrentDate()
                delivery_time = Date.getCurrentTime()
                deliveryVwHeight.constant = 0
                deliveryVw.isHidden = true
                
                backVwHeight.constant = 1000
                
            } else {
                // set selected
                button.isSelected = true
                DeliveryNowButton.isSelected = false
                
                deliveryVwHeight.constant = 150
                deliveryVw.isHidden = false
                
                backVwHeight.constant = 1150
                
            }
        }
    }
    @IBAction func PaymentTypeOnline(_ sender: Any) {
        if let button = sender as? UIButton {
            if button.isSelected {
                // set deselected
                button.isSelected = false
                PaymentTypeCash.isSelected = true
                payment_type = "COD"
                
            } else {
                // set selected
                button.isSelected = true
                PaymentTypeCash.isSelected = false
                payment_type = "online"
                
            }
        }
    }
    @IBAction func PaymentTypeCash(_ sender: Any) {
        if let button = sender as? UIButton {
            if button.isSelected {
                // set deselected
                button.isSelected = false
                PaymentTypeOnline.isSelected = true
                payment_type = "online"
                
                
            } else {
                // set selected
                button.isSelected = true
                PaymentTypeOnline.isSelected = false
                payment_type = "COD"
            }
        }
    }
    @IBAction func MorningAction(_ sender: Any) {
        if let button = sender as? UIButton {
            if button.isSelected {
                // set deselected
                button.isSelected = false
                EveningButton.isSelected = true
                
            } else {
                // set selected
                button.isSelected = true
                EveningButton.isSelected = false
                
            }
        }
    }
    @IBAction func EveningAction(_ sender: Any) {
        if let button = sender as? UIButton {
            if button.isSelected {
                // set deselected
                button.isSelected = false
                MoriningButton.isSelected = true
                
            } else {
                // set selected
                button.isSelected = true
                MoriningButton.isSelected = false
                
            }
        }
    }
    
    @IBAction func ProceedAction(_ sender: Any) {
        
        if payment_type == ""{
            
            scrollVw.scrollToBottom(animated: true)
            
            GlobalClass.showAlertDialog(message: "Please select payment type", target: self)
        }else{
            
            //appDelegate.TotalBalance = Double(appDelegate.totalCredit_) - Double(appDelegate.totalDebit_)
            
            placeOrderBackgroundView.isHidden = false
            placeOrderBackButton.isHidden = false
            toatlCartLabel.text = appDelegate.grandToatalValue
            
            firstTotalBalance()
        }
        
    }
    
    func firstTotalBalance(){
        
        appDelegate.TotalBalance = Double(newWalletAmount)!
        
        wallet_Amount = newWalletAmount
        
        walletBalancelabel.text = "\(appDelegate.TotalBalance)"
        
         totalbalanceLabel.text = "(Available Balance is \(appDelegate.TotalBalance), Uncheck if you dont want to include Wallet balance)"
                
        let walletAmt = (wallet_Amount as NSString).doubleValue
        let grandTotal = (appDelegate.grandToatalValue as NSString).doubleValue
        
        if walletAmt > grandTotal{
        
            toatlPaidValue = 0.0  //walletAmt - grandTotal
            
            let newVal = walletAmt - grandTotal
            
            appDelegate.TotalBalance = newVal
        
            wallet_Amount = "\(appDelegate.TotalBalance)"
            
            self.walletBalancelabel.text = "\(appDelegate.TotalBalance)"
            
        }else{
            
            self.walletBalancelabel.text = "0.0"
        
            toatlPaidValue = (appDelegate.grandToatalValue as NSString).doubleValue - (wallet_Amount as NSString).doubleValue
        }
        
        print("totalPaidLabel : ", toatlPaidValue)
        totalPaidLabel.text = String(toatlPaidValue)
    }
    
    //MARK: - Total Balance
    
    func totalBalance()
    {
        walletBalancelabel.text = "\(appDelegate.TotalBalance)"
                
        let walletAmt = (wallet_Amount as NSString).doubleValue
        let grandTotal = (appDelegate.grandToatalValue as NSString).doubleValue
        
        if walletAmt > grandTotal{
        
            toatlPaidValue = 0.0  //walletAmt - grandTotal
            
            let newVal = walletAmt - grandTotal
            
            appDelegate.TotalBalance = newVal
        
            wallet_Amount = "\(appDelegate.TotalBalance)"
            
            self.walletBalancelabel.text = "\(appDelegate.TotalBalance)"
            
        }else{
            
            
            self.walletBalancelabel.text = "0.0"
            
            toatlPaidValue = (appDelegate.grandToatalValue as NSString).doubleValue - (wallet_Amount as NSString).doubleValue
        }
        
        print("totalPaidLabel : ", toatlPaidValue)
        totalPaidLabel.text = String(toatlPaidValue)
    }
    
    func totalBalanceUnSelect()
    {
        let walletAmt = (wallet_Amount as NSString).doubleValue
        let grandTotal = (appDelegate.grandToatalValue as NSString).doubleValue
        
        if walletAmt > grandTotal{
        
           // toatlPaidValue = 0.0  //walletAmt - grandTotal
            
            let newVal = walletAmt + grandTotal
            
            appDelegate.TotalBalance = newVal
            
            wallet_Amount = "\(appDelegate.TotalBalance)"
            
            self.walletBalancelabel.text = "\(appDelegate.TotalBalance)"
        
        }else{
            
            self.walletBalancelabel.text = "\(appDelegate.TotalBalance)"
        
           // toatlPaidValue = (appDelegate.grandToatalValue as NSString).doubleValue + (wallet_Amount as NSString).doubleValue
        }
        
        
//        walletBalancelabel.text = "\(appDelegate.TotalBalance)"
        
        print("grandToatalValue 2:", appDelegate.grandToatalValue)
        print("Wallet amount 2:", wallet_Amount)
            
        
        toatlPaidValue = Double(appDelegate.grandToatalValue)!//(appDelegate.grandToatalValue as NSString).doubleValue - (wallet_Amount as NSString).doubleValue
        
        print("totalPaidLabel : ", toatlPaidValue)
        totalPaidLabel.text = String(toatlPaidValue)
    }
    
    @IBAction func placeorderButtonAction(_ sender: Any) {
        
        placeOrderBackgroundView.isHidden = true
        placeOrderBackButton.isHidden = true
        
    }
    
    //MARK: - Wallet Check
    
    @IBAction func walletCheckButtonSction(_ sender: Any) {
                
        if let button = sender as? UIButton {
            if button.isSelected {
                // set deselected
                button.isSelected = false
                //  walletBalancelabel.text = "0"
                wallet_Amount = "\(appDelegate.TotalBalance)"
                
                totalBalanceUnSelect()
                
            } else {
                // set selected
                button.isSelected = true
                //   walletBalancelabel.text = "30"
              //  wallet_Amount = "30"
                wallet_Amount = "\(appDelegate.TotalBalance)"
                
                totalBalance()
            }
        }
        
    }
    
    
    @IBAction func BackAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func AddCartAction(_ sender: Any) {
        if CartCount == "0"
        {
            GlobalClass.showAlertDialog(message: "You dont have any item on your cart", target: self)
            
        }
        else{
            let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
            let MyOrderViewController  = storyBoard.instantiateViewController(withIdentifier: "CartListViewController") as! CartListViewController
            self.navigationController?.pushViewController(MyOrderViewController, animated: false)
        }
    }
    @IBAction func NotificationAction(_ sender: Any) {
        let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
        let NotificationViewController  = storyBoard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(NotificationViewController, animated: false)
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        PromoTextField.resignFirstResponder()
        let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
        let NotificationViewController  = storyBoard.instantiateViewController(withIdentifier: "PromoOffersViewController") as! PromoOffersViewController
        self.navigationController?.pushViewController(NotificationViewController, animated: false)
    }
    
    @IBAction func promoCodeButtonAction(_ sender: Any) {
        
        PromoTextField.resignFirstResponder()
        let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
        let NotificationViewController  = storyBoard.instantiateViewController(withIdentifier: "PromoOffersViewController") as! PromoOffersViewController
        self.navigationController?.pushViewController(NotificationViewController, animated: false)
        
    }
    //MARK: - Payment Form
    internal func showPaymentForm(){
        let options: [String:Any] = [
            "amount": toatlPaidValue, //This is in currency subunits. 100 = 100 paise= INR 1.
            "currency": "INR",//We support more that 92 international currencies.
            "description": "Purchase for order id : \(placed_orderid)",
            "order_id": razorpayOrderId,
            "image": "https://www.linkpicture.com/q/ItunesArtwork@2x.png", //https://ibb.co/TgD50R8
            "name": "Orgafresh",
            "prefill": [
                "contact": appDelegate.loginMobileNumber,
                "email": appDelegate.loginEmailAddress
            ],
            "theme": [
                "color": "#33CCFF"
            ]
        ]
        
        print("options are : ", options)//JSON(options).rawValue
        
        razorpay.open(options)
    }
    
    @IBAction func placeOrderAction(_ sender: Any) {
        
        placeorder()
    }
    
    //MARK: - Razor Pay Action
    
    func onPaymentError(_ code: Int32, description str: String) {
        
        payment(id: "", status: 0)
        
        print("str ", str)
        print("error code ", code)
        
        GlobalClass.showAlertDialog(message: "Payment failed\nPlease try again.", target: self)
    }
    
    func onPaymentSuccess(_ payment_id: String) {
        //Call api success
        payment(id: payment_id, status: 1)
        // GlobalClass.showAlertDialog(message: "Payment success : id - \(payment_id)", target: self)
    }
    
    //MARK: - Place Order
    
    func placeorder(){
        
        SVProgressHUD.show(withStatus: "Loading...")
        
        let parameter: Parameters = ["product" : JSON(ProductValues).rawValue,"addressid" : Selected_AddressValues[0]["id"].intValue , "couponcode" : "0" , "delivery_amount" : "0.0" , "offer_amount" : "0.0" , "tax" : "0" , "delivery_date" : delivery_date , "delivery_time" : delivery_time , "payment_type" : payment_type , "warehouse_id" : warehouse_id , "total_amount" : appDelegate.grandToatalValue , "bill_amount" : toatlPaidValue , "wallet" : wallet_Amount]
        
        print("payment params : \(parameter)")
        
        networkProvider.request(.placeorder(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                let jsonVal = JSON(response.data)
                
                print("Json val : \(jsonVal)")
                
                if jsonVal["status"].stringValue == "success"{
                    
                    SVProgressHUD.dismiss()
                    self.UpdatereteriveTempData()
                    
                    self.placed_orderid = jsonVal["order_id"].intValue
                    self.razorpayOrderId = jsonVal["razorpayOrderId"].stringValue
                    if self.payment_type == "COD"{
                        
                        let storyBoard     = UIStoryboard(name: "Second", bundle: nil)
                        let OrderSucessVC = storyBoard.instantiateViewController(withIdentifier: "OrderSucessViewController") as! OrderSucessViewController
                        OrderSucessVC.place_orderid = self.placed_orderid
                        self.navigationController?.pushViewController(OrderSucessVC, animated: true)
                    }else{
                        
                        self.showPaymentForm()
                    }
                    
                }
                else{
                    
                    GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                }
                
            case .failure( _):
                SVProgressHUD.dismiss()
                
                break
            }
        })
    }
    var Tvals2 = [JSON]()
    
    func UpdatereteriveTempData()
    {
        Tvals2.removeAll()
        
        
        CartButton.badge = ""
        CartButton.badgeLabel.backgroundColor = UIColor.clear
        CartCount = "0"
        
        UserDefaults.standard.setValue(nil, forKey: "CartCount")
        UserDefaults.standard.synchronize()
        UserDefaults.standard.setValue(nil, forKey: "TempCartValues")
        UserDefaults.standard.synchronize()
    }
    
    //MARK: - Payment
    
    func payment(id: String, status: Int){
        
        SVProgressHUD.show(withStatus: "Loading...")
        
        let parameter: Parameters = ["order_id" : appDelegate.order_id ,"amount" : toatlPaidValue , "reference_number" : id , "status" : status , "response" : appDelegate.order_response, "razor_order_id" : self.razorpayOrderId]
        
        print("payment params : \(parameter)")
        
        networkProvider.request(.payment(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                let jsonVal = JSON(response.data)
                
                print("Json val : \(jsonVal)")
                
                SVProgressHUD.dismiss()
                
                if jsonVal["status"].stringValue == "success"{
                    //    self.moblieButton.isHidden = true
                    
                    let storyBoard     = UIStoryboard(name: "Second", bundle: nil)
                    let OrderSucessVC = storyBoard.instantiateViewController(withIdentifier: "OrderSucessViewController") as! OrderSucessViewController
                    OrderSucessVC.place_orderid = self.placed_orderid
                    self.navigationController?.pushViewController(OrderSucessVC, animated: true)
                    
                    //GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                    
                }
                else{
                    
                    GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                }
                
            case .failure( _):
                SVProgressHUD.dismiss()
                
                break
            }
        })
    }
    
    
    func reteriveTempData()
    {
        ProductValues.removeAll()
        
        let vals = UserDefaults.standard.value(forKey: "TempCartValues") as Any
        let retrivedVals = JSON(vals)
        for i in 0 ..< retrivedVals.count{
            
            productArray["productid"] = JSON(retrivedVals[i]["id"].intValue)
            productArray["quantity"] = JSON(retrivedVals[i]["quantity"].stringValue)
            ProductValues.append(productArray)
            
        }
    }
    
}

extension Date {
    
    static func getCurrentDate() -> String {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        return dateFormatter.string(from: Date())
        
    }
    static func getCurrentTime() -> String {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "HH:mm:ss"
        
        return dateFormatter.string(from: Date())
        
    }
}
