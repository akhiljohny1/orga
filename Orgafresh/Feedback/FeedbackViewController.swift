//
//  FeedbackViewController.swift
//  Orgafresh
//
//  Copyright © 2020 Innovation Incubator. All rights reserved.
//

import UIKit
import SVProgressHUD
import SwiftyJSON
import Alamofire

class FeedbackViewController: UIViewController {
@IBOutlet weak var CartButton: SSBadgeButton!

@IBOutlet weak var gradientVwTopConstrain    : NSLayoutConstraint!
@IBOutlet weak var gradientVwHeightConstrain : NSLayoutConstraint!

@IBOutlet weak var feedbackBackgroundView: NSLayoutConstraint!
@IBOutlet weak var messageLabel: UITextField!
@IBOutlet weak var namelabel: UITextField!
override func viewDidLoad() {
    super.viewDidLoad()
    
    if CartCount == "0"
            {
                CartButton.badge = ""
                CartButton.badgeLabel.backgroundColor = UIColor.clear
            }
            else
            {
      CartButton.badge = "\(CartCount)"
      }
    if DeviceType.IS_IPHONE_X || DeviceType.IS_IPHONE_XP{
        
        gradientVwTopConstrain.constant = 43
        gradientVwHeightConstrain.constant = 55
        
    }else{
        
        gradientVwTopConstrain.constant = 21
        gradientVwHeightConstrain.constant = 55
        feedbackBackgroundView.constant = 110
    }
    let leftView1                             = UIView(frame: CGRect(x: 5, y: 0, width: 15, height: 26))
    leftView1.backgroundColor                 = .clear
    
    namelabel.leftView                 = leftView1
    namelabel.leftViewMode             = .always
    namelabel.contentVerticalAlignment = .center
    
    let leftView2                             = UIView(frame: CGRect(x: 5, y: 0, width: 15, height: 26))
    leftView2.backgroundColor                 = .clear
    messageLabel.leftView                 = leftView2
    messageLabel.leftViewMode             = .always
    messageLabel.contentVerticalAlignment = .center
    // Do any additional setup after loading the view.
}
@IBAction func SendFeedbackAction(_ sender: Any) {
    if namelabel.text!.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty || messageLabel.text!.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
           GlobalClass.showAlertDialog(message: "Please enter all fields.", target: self)
                            return
                        }
    
           self.view.endEditing(true)
    postFeedback()
    
}
@IBAction func BackButtonAction(_ sender: Any) {
    
    self.navigationController?.popViewController(animated: true)
}
@IBAction func AddCartAction(_ sender: Any) {
    if CartCount == "0"
              {
               GlobalClass.showAlertDialog(message: "You dont have any item on your cart", target: self)

           }
           else{
    let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
    let MyOrderViewController  = storyBoard.instantiateViewController(withIdentifier: "CartListViewController") as! CartListViewController
    self.navigationController?.pushViewController(MyOrderViewController, animated: false)
    }
}
@IBAction func NotificationAction(_ sender: Any) {
    let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
    let NotificationViewController  = storyBoard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
    self.navigationController?.pushViewController(NotificationViewController, animated: false)
}

func postFeedback() {
    SVProgressHUD.show(withStatus: "Sending...")
    
    let parameter: Parameters = ["name" : namelabel.text!, "message" : messageLabel.text!]
    
    print("noti params : \(parameter)")
    
    networkProvider.request(.addfeedback(parameter: parameter), completion: {(result) in
        
        switch result {
            
        case .success(let response):
            
            SVProgressHUD.dismiss()
            
            let jsonVal = JSON(response.data)
            
            print("Sending val : \(jsonVal)")
            
                
                if jsonVal["status"].stringValue == "success"{
                    
                  let alert = UIAlertController(title: "", message: jsonVal["message"].stringValue, preferredStyle: UIAlertController.Style.alert)
                                     self.present(alert, animated: true, completion: nil)

                                     alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { action in
                                         self.navigationController?.popViewController(animated: true)
                                         
                                         
                                     }))

                    
                }else{
                    
                    GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                }
            
            
            
            
        case .failure( _):
            
            GlobalClass.showAlertDialog(message: "Please try again.", target: self)
            
            SVProgressHUD.dismiss()
            
            break
        }
    })
    
}

}
