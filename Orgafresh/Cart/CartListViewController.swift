//
//  CartListViewController.swift
//  Orgafresh
//
//  Copyright © 2020 Innovation Incubator. All rights reserved.
//

import UIKit
import SwiftyJSON
var addressEditingFlag = 0
var Selected_AddressValues = [JSON]()


var RandomGrandTotal = 0
var randomSavedPriceTotal = 0
var CartCountValueZero = 1

class CartListCell: UITableViewCell{
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var minimumQuantityLabel: UILabel!
    @IBOutlet weak var NormalPriceLabel: UILabel!
    @IBOutlet weak var counterValueButton: UIButton!
    @IBOutlet weak var CartImageView: UIImageView!
    @IBOutlet weak var sellingPriceLabel: UILabel!
    @IBOutlet weak var TotalPriceLabel: UILabel!
    @IBOutlet weak var plusbutton: UIButton!
    @IBOutlet weak var minusButton: UIButton!
    var counterValu = 1
    var sellprice = 1
    var normalprice = 1
    var total = 1
    var noramPricetotal = 1
    var savedPrice = 1
    
    var CartArray = JSON()
    
    
    var TempCartValues = [JSON]()
    
    var Normalprice = ""
    var SellingPriceString = ""
    var ImageString = ""
    var indexValue = 1
    var product_id = 0
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        reteriveTempData()
        
    }
    
    @IBAction func minusButton(_ sender: UIButton) {
        indexValue = sender.tag
        
        if(counterValu != 1){
            counterValu -= 1;
            CartCountValueZero = counterValu
            counterValueButton.setTitle("\(counterValu)", for: .normal)
            total = sellprice * counterValu
            TotalPriceLabel.text = "Total Price: \(total)"
            tempValueSave(tag: indexValue)
            RandomGrandTotal = RandomGrandTotal - sellprice
            
            savedPrice = normalprice - sellprice
            randomSavedPriceTotal = randomSavedPriceTotal - savedPrice
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadTbl"), object: "\(RandomGrandTotal),\(randomSavedPriceTotal)")
        }
        else
        {
            CartCountValueZero = 0
            
        }
    }
    @IBAction func plusButton(_ sender: UIButton) {
        indexValue = sender.tag
        if counterValu >= 5
        {
            
        }else
        {
            counterValu += 1;
            CartCountValueZero = counterValu
            counterValueButton.setTitle("\(counterValu)", for: .normal)
            total = sellprice * counterValu
            TotalPriceLabel.text = "Total Price: \(total)"
            tempValueSave(tag: indexValue)
            RandomGrandTotal = RandomGrandTotal + sellprice
            savedPrice = normalprice - sellprice
            randomSavedPriceTotal = randomSavedPriceTotal + savedPrice
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadTbl"), object: "\(RandomGrandTotal),\(randomSavedPriceTotal)")
        }
    }
    
    func tempValueSave(tag: Int)
    {
        
        CartArray["product_name"] = JSON(productNameLabel.text!)
        CartArray["normal_price"] = JSON(Normalprice)
        CartArray["selling_price"] = JSON(SellingPriceString)
        CartArray["quantity"] = JSON(counterValu)
        CartArray["product_img"] = JSON(ImageString)
        CartArray["minimum_order_quantity"] = JSON(minimumQuantityLabel.text!)
        TempCartValues.remove(at: tag)
        TempCartValues.insert(CartArray, at: tag)
        UserDefaults.standard.setValue(JSON(TempCartValues).rawValue, forKey: "TempCartValues")
        UserDefaults.standard.synchronize()
        
    }
    func reteriveTempData()
    {
        TempCartValues.removeAll()
        
        let vals = UserDefaults.standard.value(forKey: "TempCartValues") as Any
        let retrivedVals = JSON(vals)
        for i in 0 ..< retrivedVals.count{
            
            TempCartValues.append(retrivedVals[i])
        }
    }
}

class CartListViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var cartButton: SSBadgeButton!
    @IBOutlet weak var ItemsTableView: UITableView!
    var grandTotal = 0
    var savedPriceTotal = 0
    
    var flag = 1
    var sum = 0
    @IBOutlet weak var GrandTotalLabel: UILabel!
    @IBOutlet weak var SavedLabel: UILabel!
    var Tvals2 = [JSON]()
    
    @IBOutlet weak var gradientVwTopConstrain    : NSLayoutConstraint!
    @IBOutlet weak var gradientVwHeightConstrain : NSLayoutConstraint!
    
    @IBOutlet weak var viewBottom : NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ItemsTableView.delegate = self
        ItemsTableView.dataSource = self
        
        if DeviceType.IS_IPHONE_X || DeviceType.IS_IPHONE_XP{
            
            gradientVwTopConstrain.constant = 43
            gradientVwHeightConstrain.constant = 55
            
        }else{
            
            gradientVwTopConstrain.constant = 20
            gradientVwHeightConstrain.constant = 55
        }
        
        if DeviceType.IS_IPHONE_6{
            
            //   viewBottom.constant = 250
        }
        
        if DeviceType.IS_IPHONE_6P{
            
            //  viewBottom.constant = 180
        }
        
        if DeviceType.IS_IPHONE_X{
            
            //   viewBottom.constant = 60
        }
        
        if DeviceType.IS_IPHONE_XP{
            
            //  viewBottom.constant = 60
        }
        
        
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.reloadTbl),
            name: NSNotification.Name(rawValue: "reloadTbl"),
            object: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        reteriveTempData()
        
    }
    //MARK: - Trend Post
    
    @objc func reloadTbl(notification: NSNotification){
        
        let vals = notification.object as! String
        GrandTotalLabel.text = "Rs: \(vals.components(separatedBy: ",").first!)"
        SavedLabel.text = "Saved Rs: \(vals.components(separatedBy: ",").last!)"
        appDelegate.grandToatalValue = vals.components(separatedBy: ",").first!
    }
    
    //MARK: - TableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return Tvals2.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let ItemsCell:CartListCell = ItemsTableView.dequeueReusableCell(withIdentifier: "CartListCell") as! CartListCell
        
        ItemsCell.product_id = Tvals2[indexPath.row]["id"].intValue
        
        ItemsCell.productNameLabel.text = Tvals2[indexPath.row]["product_name"].stringValue
        ItemsCell.minimumQuantityLabel.text = Tvals2[indexPath.row]["minimum_order_quantity"].stringValue + Tvals2[indexPath.row]["unit_name"].stringValue
        
        if !Tvals2[indexPath.row]["normal_price"].stringValue.isEmpty
        {
            ItemsCell.NormalPriceLabel.text = "₹" + Tvals2[indexPath.row]["normal_price"].stringValue
        }
        else
        {
            ItemsCell.NormalPriceLabel.text = ""
        }
        ItemsCell.sellingPriceLabel.text = "₹" + Tvals2[indexPath.row]["selling_price"].stringValue
        
        ItemsCell.sellingPriceLabel.text = "₹" + Tvals2[indexPath.row]["selling_price"].stringValue
        ItemsCell.counterValueButton.setTitle("\(Tvals2[indexPath.row]["quantity"].stringValue)", for: .normal)
        ItemsCell.NormalPriceLabel.attributedText = ItemsCell.NormalPriceLabel.text!.strikeThrough()
        let imgVw = ItemsCell.viewWithTag(1) as! UIImageView
        var str = imageBaseUrl + Tvals2[indexPath.row]["product_img"].stringValue
        
        str = str.replacingOccurrences(of: " ", with: "%20")
        imgVw.sd_setImage(with: URL(string: str), placeholderImage: nil)
        ItemsCell.sellprice = (Tvals2[indexPath.row]["selling_price"].stringValue as NSString).integerValue
        ItemsCell.counterValu = (Tvals2[indexPath.row]["quantity"].stringValue as NSString).integerValue
        ItemsCell.total = ItemsCell.sellprice * ItemsCell.counterValu
        ItemsCell.TotalPriceLabel.text = "Total Price: \(ItemsCell.total)"
        
        ItemsCell.normalprice = (Tvals2[indexPath.row]["normal_price"].stringValue as NSString).integerValue
        
        ItemsCell.noramPricetotal = ItemsCell.normalprice * ItemsCell.counterValu
        
        ItemsCell.savedPrice = ItemsCell.noramPricetotal - ItemsCell.total
        savedPriceTotal = savedPriceTotal + ItemsCell.savedPrice
        
        grandTotal += ItemsCell.total
        sum = sum + flag
        
        if Tvals2.count == sum
        {
            if savedPriceTotal > 0
            {
                SavedLabel.text = "Saved Rs: \(savedPriceTotal)"
                
            }
            else
            {
                SavedLabel.text = "Saved Rs: 0"
            }
            
            RandomGrandTotal = grandTotal
            randomSavedPriceTotal = savedPriceTotal
            GrandTotalLabel.text = "Rs: \(grandTotal)"
            appDelegate.grandToatalValue = "\(grandTotal)"
        }
        ItemsCell.minusButton.tag = indexPath.row
        ItemsCell.plusbutton.tag = indexPath.row
        
        ItemsCell.Normalprice = Tvals2[indexPath.row]["normal_price"].stringValue
        ItemsCell.SellingPriceString = Tvals2[indexPath.row]["selling_price"].stringValue
        ItemsCell.ImageString = Tvals2[indexPath.row]["product_img"].stringValue
        
        return ItemsCell
    }
    
    @IBAction func cartMinusAction(_ sender: UIButton) {
        if CartCountValueZero == 0 {
            let alert = UIAlertController(title: "Delete", message: "Are you sure want to delete \(Tvals2[sender.tag]["product_name"].stringValue) from your Cart list", preferredStyle: UIAlertController.Style.alert)
            
            // add the actions (buttons)
            
            alert.addAction(UIAlertAction(title: "NO", style: UIAlertAction.Style.cancel, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
            
            
            alert.addAction(UIAlertAction(title: "YES", style: UIAlertAction.Style.default, handler: { action in
                
                // do something like...
                self.Tvals2.remove(at: sender.tag)
                UserDefaults.standard.setValue(JSON(self.Tvals2).rawValue, forKey: "TempCartValues")
                UserDefaults.standard.synchronize()
                
                self.ItemsTableView.reloadData()
                self.reteriveTempData()
            }))
        }
    }
    
    @IBAction func BackAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func NotificationAction(_ sender: Any) {
        
        let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
        let NotificationViewController  = storyBoard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(NotificationViewController, animated: false)
    }
    
    @IBAction func CheckOutAction(_ sender: Any) {
        if Tvals2.count == 0
        {
            GlobalClass.showAlertDialog(message: "You dont have any item on your cart", target: self)
            
        }
        else{
            
            
            
            let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
            let NotificationViewController  = storyBoard.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
            self.navigationController?.pushViewController(NotificationViewController, animated: true)
        }
    }
    func reteriveTempData()
    {
        Tvals2.removeAll()
        
        let vals = UserDefaults.standard.value(forKey: "TempCartValues") as Any
        
        let retrivedVals = JSON(vals)
        
        print("retrivedVals : ", retrivedVals)
        
        for i in 0 ..< retrivedVals.count{
            Tvals2.append(retrivedVals[i])
        }
        print(Tvals2)
        
        if Tvals2.count == 0
        {
            cartButton.badge = ""
            cartButton.badgeLabel.backgroundColor = UIColor.clear
        }
        else
        {
            cartButton.badge = "\(Tvals2.count)"
        }
        
        CartCount = "\(Tvals2.count)"
        UserDefaults.standard.setValue(JSON("\(Tvals2.count)").rawValue, forKey: "CartCount")
        UserDefaults.standard.synchronize()
        
        
        
    }
}
