//
//  TidyTextField.swift
//  Orgafresh
//
//  Created by ani david on 30/08/20.
//  Copyright © 2020 Innovation Incubator. All rights reserved.
//

import UIKit

class TidyTextField: UITextField {

   @IBInspectable var rightImage: UIImage? = nil
    @IBInspectable var rightPadding: CGFloat = 0
    @IBInspectable var gapPadding: CGFloat = 0
    
    private var textPadding: UIEdgeInsets {
        let p: CGFloat = rightPadding + gapPadding + (leftView?.frame.width ?? 0)
        return UIEdgeInsets(top: 0, left: 5, bottom: 0, right: p)
    }
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: textPadding)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: textPadding)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: textPadding)
    }
override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        var r = super.rightViewRect(forBounds: bounds)
        r.origin.x += rightPadding
        return r
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setup()
    }
    
    private func setup() {
        if let image = rightImage {
            if rightView != nil { return } // critical!
            
            let im = UIImageView()
            im.contentMode = .scaleAspectFit
            im.image = image
            
            rightViewMode = UITextField.ViewMode.always
            rightView = im
            
        } else {
            rightViewMode = UITextField.ViewMode.never
            rightView = nil
        }
    }
}
