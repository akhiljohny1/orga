//
//  SignUpViewController.swift
//  Orgafresh
//
//  Created by Innovation Incubator on 31/07/20.
//  Copyright © 2020 Innovation Incubator. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD

class SignUpViewController: UIViewController {
    
    @IBOutlet weak var usernameField             : UITextField!
    @IBOutlet weak var emailField                : UITextField!
    @IBOutlet weak var mobileField               : UITextField!
    @IBOutlet weak var passwordField             : UITextField!
    @IBOutlet weak var confirmPasswordField      : UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    //MARK: - Signup Action
    
    @IBAction func signUpAction(_ sender: AnyObject) {
        
        self.view.endEditing(true)
        
        
        if usernameField.text!.isEmpty || emailField.text!.isEmpty || mobileField.text!.isEmpty || passwordField.text!.isEmpty || confirmPasswordField.text!.isEmpty{
            GlobalClass.showAlertDialog(message: "All fields are mandatory", target: self)
            return
        }
        if usernameField.text!.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||  emailField.text!.trimmingCharacters(in: .whitespacesAndNewlines) == "" || mobileField.text!.trimmingCharacters(in: .whitespacesAndNewlines) == "" || passwordField.text!.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||  confirmPasswordField.text!.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
            GlobalClass.showAlertDialog(message: "All fields are mandatory", target: self)
            return
        }
        
        if !GlobalClass.validateEmail(enteredEmail: emailField.text!){
            
            GlobalClass.showAlertDialog(message: "Please enter valid email address.", target: self)
            return
        }
        
        if !GlobalClass.validatePHnumber(enteredNumber: mobileField.text!){
            
            GlobalClass.showAlertDialog(message: "Please enter valid phone number.", target: self)
            return
        }
        
        if passwordField.text!.count < 8{
            
            GlobalClass.showAlertDialog(message: "Please enter minimum 8 digit for password.", target: self)
            return
        }
        
        if passwordField.text != confirmPasswordField.text{
            
            GlobalClass.showAlertDialog(message: "Passwords you entered mismatch.", target: self)
            return
        }
        
        
        callRegister()
    }
    
    //MARK: - Register Api
    
    func callRegister(){
        
        SVProgressHUD.show(withStatus: "Registering...")
        
        let parameter: Parameters = ["name" : usernameField.text!, "email" : emailField.text!, "phone_number" : Int(mobileField.text!) ?? 123, "password" : passwordField.text!, "password_confirmation" : confirmPasswordField.text!, "device" : "ios", "push_notification" : UserDefaults.standard.string(forKey: "PushToken") ?? "user rejected"]
        
        print("noti params : \(parameter)")
        
        networkProvider.request(.register(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                SVProgressHUD.dismiss()
                
                let jsonVal = JSON(response.data)
                
                print("Json val : \(jsonVal)")
                
                print(jsonVal["status"].stringValue)
                
                print(response.statusCode)
                
                if response.statusCode == 200{
                    
                    if jsonVal["status"].stringValue == "success"{
                        
                        UserDefaults.standard.setValue(jsonVal["token"].stringValue, forKey: "access_token")
                        UserDefaults.standard.synchronize()
                        
                        let storyBoard          = UIStoryboard(name: "Main", bundle: nil)
                        let nextViewController  = storyBoard.instantiateViewController(withIdentifier: "VerifyViewController") as! VerifyViewController
                        nextViewController.mobileStr = self.mobileField.text!
                        nextViewController.userID = jsonVal["user_id"].intValue
                        self.navigationController?.pushViewController(nextViewController, animated: true)
                        
                    }else{
                        
                        GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                    }
                }else if response.statusCode == 401{
                    
                    GlobalClass.callGetApiKey(viewController: self)
                    GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                    
                }else if jsonVal["message"].stringValue.contains("token"){
                    
                    GlobalClass.callGetApiKey(viewController: self)
                }
                else if response.statusCode == 500{
                    
                    //  GlobalClass.callGetApiKey(viewController: self)
                    
                    let storyBoard          = UIStoryboard(name: "Main", bundle: nil)
                    let nextViewController  = storyBoard.instantiateViewController(withIdentifier: "VerifyViewController") as! VerifyViewController
                    nextViewController.mobileStr = self.mobileField.text!
                    self.navigationController?.pushViewController(nextViewController, animated: true)
                    
                }
                else{
                    
                    GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                }
                
                
            case .failure( _):
                
                GlobalClass.showAlertDialog(message: "Please try again.", target: self)
                
                SVProgressHUD.dismiss()
                
                break
            }
        })
    }
    
    @IBAction func bacAction(_ sender: AnyObject) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
