//
//  MoreCategoryViewController.swift
//  Orgafresh
//
//  Created by Innovation Incubator on 04/08/20.
//  Copyright © 2020 Innovation Incubator. All rights reserved.
//

import UIKit
import Alamofire
import Moya
import SwiftyJSON
import SVProgressHUD
import SDWebImage

class MoreCategoryViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    
    @IBOutlet weak var collVw             : UICollectionView!
    
    var categoryVals                      = JSON()
    
    @IBOutlet weak var gradientVwTopConstrain    : NSLayoutConstraint!
    @IBOutlet weak var gradientVwHeightConstrain : NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if DeviceType.IS_IPHONE_X || DeviceType.IS_IPHONE_XP{
            
            gradientVwTopConstrain.constant = 0
            gradientVwHeightConstrain.constant = 55
            
        }else{
            
            gradientVwTopConstrain.constant = 0
            gradientVwHeightConstrain.constant = 55
        }
        
        callGetCategory()
        // Do any additional setup after loading the view.
    }
    
    
    
    //MARK: - Call Get Home
    
    func callGetCategory() {
        
        DispatchQueue.main.async {
        
        SVProgressHUD.show(withStatus: "Loading...")
            
        }
        
        networkProvider.request(.getcategory, completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                SVProgressHUD.dismiss()
                
                let jsonVal = JSON(response.data)
                
                print("Category vals : \(jsonVal)")
                
                if response.statusCode == 200{
                    
                    if jsonVal["status"].stringValue == "success"{
                        
                        self.categoryVals = jsonVal["category"]
                        
                    }else{
                        
                        GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                    }
                }else if response.statusCode == 401{
                    
                    GlobalClass.callGetApiKey(viewController: self)
                    GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                    
                }else if jsonVal["message"].stringValue.contains("token"){
                    
                    GlobalClass.callGetApiKey(viewController: self)
                }
                else if response.statusCode == 500{
                    
                    GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                }
                else{
                    
                    GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                }
                
                DispatchQueue.main.async {
                    
                    self.collVw.reloadData()
                    
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                    
                    self.collVw.reloadData()
                })
                
            case .failure( _):
                
                GlobalClass.showAlertDialog(message: "Please try again.", target: self)
                
                SVProgressHUD.dismiss()
                
                break
            }
        })
    }
    
    //MARK: - Collection View
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return categoryVals.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collVw.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! HomeCollectionCell
        
        cell.greenView.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 0)
        cell.greenView.clipsToBounds = true
        
        cell.nameLbl.text = categoryVals[indexPath.row]["category_name"].stringValue.firstUppercased
        
              var str = imageBaseUrl + categoryVals[indexPath.row]["category_image"].stringValue
              str = str.replacingOccurrences(of: " ", with: "%20")
        cell.imgView.sd_setImage(with: URL(string: str), placeholderImage: #imageLiteral(resourceName: "loginVector"))
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
        let SelectedCategoryVC  = storyBoard.instantiateViewController(withIdentifier: "SelectedCategoryViewController") as! SelectedCategoryViewController
        SelectedCategoryVC.values = categoryVals[indexPath.row]
        self.navigationController?.pushViewController(SelectedCategoryVC, animated: true)
        
        //     print(indexPath.row)
        //        let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
        //        let ProductDetailsVC  = storyBoard.instantiateViewController(withIdentifier: "ProductDetailsViewController") as! ProductDetailsViewController
        //
        //        ProductDetailsVC.productValues = categoryVals[indexPath.row]
        //        self.navigationController?.pushViewController(ProductDetailsVC, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let numberOfCellInRow : Int        = 2
        let padding : Int                  = 2
        let collectionCellWidth : CGFloat  = (self.collVw.frame.size.width/CGFloat(numberOfCellInRow)) - CGFloat(padding)
        
        return CGSize(width: collectionCellWidth, height: collectionCellWidth)
    }
    
    //MARK: - Notification Action
    
    @IBAction func notificationAction(_ sender: AnyObject){
        
        let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
        let NotificationViewController  = storyBoard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(NotificationViewController, animated: true)
    }
    
    @IBAction func MyCartAction(_ sender: Any) {
        if CartCount == "0"
                  {
                   GlobalClass.showAlertDialog(message: "You dont have any item on your cart", target: self)

               }
               else{
        let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
        let MyOrderViewController  = storyBoard.instantiateViewController(withIdentifier: "CartListViewController") as! CartListViewController
        self.navigationController?.pushViewController(MyOrderViewController, animated: true)
        }
    }
    
    //MARK: - Button Action
    
    @IBAction func backAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
