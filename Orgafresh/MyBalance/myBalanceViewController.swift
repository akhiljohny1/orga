//
//  MyFavouriteViewController.swift
//  Orgafresh
//
//  Copyright © 2020 Innovation Incubator. All rights reserved.
//

import UIKit
import SwiftyJSON
import  Alamofire
import SVProgressHUD


class myBalanceCell: UITableViewCell{
    
    @IBOutlet weak var DescriptionLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet var walletImage: UIImageView!
    @IBOutlet weak var ArrowIndex: UIButton!
    @IBOutlet weak var UpwardArrow: UIButton!
    
    @IBOutlet weak var backGroundView: UIView!
    
}

class myBalanceCell2: UITableViewCell{
    
    @IBOutlet weak var DescriptionLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var UpdatedTimeLabel: UILabel!
    
    
}

class myBalanceViewController: UIViewController, UITableViewDelegate,UITableViewDataSource {
    
    
    @IBOutlet weak var CartButton: SSBadgeButton!
    
    
    @IBOutlet var myBalanceTableView: UITableView!
    
    @IBOutlet weak var bottomTableConstarint: NSLayoutConstraint!
    
    @IBOutlet weak var gradientVwTopConstrain    : NSLayoutConstraint!
    @IBOutlet weak var gradientVwHeightConstrain : NSLayoutConstraint!
    var walletValues = [JSON]()
    var CLNArray = JSON()
    var CLNValues = [JSON]()
    
    var resultArray = JSON()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if CartCount == "0"
        {
            CartButton.badge = ""
            CartButton.badgeLabel.backgroundColor = UIColor.clear
        }
        else
        {
            CartButton.badge = "\(CartCount)"
        }
        if DeviceType.IS_IPHONE_X || DeviceType.IS_IPHONE_XP{
            
            gradientVwTopConstrain.constant = 0
            gradientVwHeightConstrain.constant = 55
            
        }else{
            
            gradientVwTopConstrain.constant = 0
            gradientVwHeightConstrain.constant = 55
            bottomTableConstarint.constant = 260
        }
        getwallet()
    }
    
    var opened = false
    var section1opened = Int()
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if opened == false{
            return 1
        }
        else
        {
            if section1opened == 0
            {
                if section == 0
                {
                    return walletValues.count + 1
                }
                else
                {
                    return 1
                }
            }
            else if section1opened == 1
            {
                if section == 0
                {
                    return 1
                }
                else
                {
                    return CLNValues.count+1
                }
            }
        }
        return 1
    }
    
    var totalDebit = Int()
    var totalCredit = Double()
    var totalBalance = Double()
    
    
    var totalCLN = Int()
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let myBalancecell = myBalanceTableView.dequeueReusableCell(withIdentifier: "myBalanceCell") as! myBalanceCell
        
        if indexPath.row == 0
        {
            if indexPath.section == 0
            {
                myBalancecell.walletImage.image = #imageLiteral(resourceName: "purse")
                myBalancecell.DescriptionLabel.text = "Total Balance"
                myBalancecell.amountLabel.text = "\(totalBalance)"
                myBalancecell.backGroundView.backgroundColor = #colorLiteral(red: 0.2, green: 0.8, blue: 1, alpha: 1)
            }
            else if indexPath.section == 1
            {
                myBalancecell.DescriptionLabel.text = "Clean And Earn Earnings"
                myBalancecell.walletImage.image = #imageLiteral(resourceName: "dollar")
                myBalancecell.amountLabel.text = "\(totalCredit)"
                myBalancecell.backGroundView.backgroundColor = UIColor.init(hexFromString: "#21D0B3")
            }
        }
        else{
            let myBalancecell2 = myBalanceTableView.dequeueReusableCell(withIdentifier: "myBalanceCell2") as! myBalanceCell2
            
            if indexPath.section == 0
            {
                myBalancecell2.DescriptionLabel.text = walletValues[indexPath.row-1]["description"].stringValue
                let str = convertDateFormater(date: walletValues[indexPath.row-1]["updated_at"].stringValue)
                
                myBalancecell2.UpdatedTimeLabel.text = str
                
                if walletValues[indexPath.row-1]["transaction_type"].stringValue == "credit"
                {
                    myBalancecell2.amountLabel.textColor = UIColor.init(hexFromString: "#21D0B3")
                    myBalancecell2.amountLabel.text = "(+)Rs." + walletValues[indexPath.row-1]["amount"].stringValue
                }
                else
                {
                    myBalancecell2.amountLabel.textColor = UIColor.red
                    myBalancecell2.amountLabel.text = "(-)Rs." + walletValues[indexPath.row-1]["amount"].stringValue
                }
            }
            else if indexPath.section == 1
                
            {
                let str = convertDateFormater(date: CLNValues[indexPath.row-1]["updated_at"].stringValue)
                myBalancecell2.DescriptionLabel.text = CLNValues[indexPath.row-1]["description"].stringValue
                myBalancecell2.UpdatedTimeLabel.text = str
                myBalancecell2.amountLabel.textColor = #colorLiteral(red: 0.2, green: 0.8, blue: 1, alpha: 1)
                myBalancecell2.amountLabel.text = "Rs." + CLNValues[indexPath.row-1]["amount"].stringValue
                
            }
            
            return myBalancecell2
        }
        if opened == true{
            
            if section1opened == 0
            {
                if indexPath.section == 0
                {
                    myBalancecell.UpwardArrow.setImage(UIImage(named:"UpArrow"), for: .normal)
                }
                else
                {
                    myBalancecell.UpwardArrow.setImage(UIImage(named:"DownwardArrow"), for: .normal)
                    
                }
                
            }
            else if section1opened == 1
            {
                if indexPath.section == 1
                {
                    myBalancecell.UpwardArrow.setImage(UIImage(named:"UpArrow"), for: .normal)
                }
                else
                {
                    myBalancecell.UpwardArrow.setImage(UIImage(named:"DownwardArrow"), for: .normal)
                    
                }
            }
            else
            {
                myBalancecell.UpwardArrow.setImage(UIImage(named:"DownwardArrow"), for: .normal)
            }
            
        }
        else
        {
            myBalancecell.UpwardArrow.setImage(UIImage(named:"DownwardArrow"), for: .normal)
            
        }
        
        return myBalancecell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        section1opened = indexPath.section
        if indexPath.row == 0{
            
            if opened == false{
                
                opened = true
            }else{
                opened = false
            }
            
            
            tableView.reloadData()
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if opened == false{
            return 160
        }
        else
        {
            if indexPath.row == 0
            {
                return 160
                
            }
                
            else{
                
                return 80
                
            }
        }
        
    }
    var arrSelectedIndex : [Int] = []
    
    @IBAction func arrowClicked(_ sender: UIButton) {
        
        if sender.tag == 0{
            
            if opened == false{
                
                opened = true
            }else{
                opened = false
            }
            myBalanceTableView.reloadData()
        }
    }
    func convertDateFormater(date: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ"
        guard let date = dateFormatter.date(from: date) else {
            assert(false, "no date from string")
            return ""
        }
        dateFormatter.dateFormat = "MMM dd,yyyy"
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        let timeStamp = dateFormatter.string(from: date)
        
        return timeStamp
    }
    @IBAction func BackAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func AddCartAction(_ sender: Any) {
        if CartCount == "0"
        {
            GlobalClass.showAlertDialog(message: "You dont have any item on your cart", target: self)
            
        }
        else{
            let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
            let MyOrderViewController  = storyBoard.instantiateViewController(withIdentifier: "CartListViewController") as! CartListViewController
            self.navigationController?.pushViewController(MyOrderViewController, animated: false)
        }
    }
    @IBAction func NotificationAction(_ sender: Any) {
        let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
        let NotificationViewController  = storyBoard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(NotificationViewController, animated: false)
    }
    
    //MARK:- Func
    func getwallet(){
        
        DispatchQueue.main.async {
            
            SVProgressHUD.show(withStatus: "Loading...")
        }        
        
        networkProvider.request(.getwallet, completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                
                let jsonVal = JSON(response.data)
                
                if response.statusCode == 200{
                    
                    if jsonVal["status"].stringValue == "success"{
                        self.resultArray = jsonVal["wallet"]
                        self.resultArray.reversed().forEach {
                            
                            self.walletValues.append($1)
                        }
                        if self.walletValues.count > 0
                        {
                            for i in 0 ... self.walletValues.count-1
                                
                            {
                                if self.walletValues[i]["transaction_type"].stringValue == "credit"
                                {
                                    self.totalCredit += Double(self.walletValues[i]["amount"].stringValue) ?? 0
                                    
                                }
                                else
                                {
                                    self.totalDebit += Int(self.walletValues[i]["amount"].stringValue) ?? 0
                                    
                                }
                                if self.walletValues[i]["type"].stringValue == "CLN"
                                {
                                    self.CLNValues.append(self.walletValues[i])
                                    self.totalCLN += Int(self.walletValues[i]["amount"].stringValue) ?? 0
                                }
                                
                            }
                            self.totalBalance = Double(self.totalCredit) - Double(self.totalDebit)
                            appDelegate.TotalBalance = self.totalBalance
                        }
                    }else{
                        
                        GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                    }
                    
                }else if response.statusCode == 401{
                    
                    GlobalClass.callGetApiKey(viewController: self)
                    GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                    
                }else if jsonVal["message"].stringValue.contains("token"){
                    
                    GlobalClass.callGetApiKey(viewController: self)
                }
                else if response.statusCode == 500{
                    
                    GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                }
                else{
                    
                    GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                }
                
                self.myBalanceTableView.reloadData()
                SVProgressHUD.dismiss()
                
                
            case .failure( _):
                
                SVProgressHUD.dismiss()
                
                break
            }
        })
    }
    
}



