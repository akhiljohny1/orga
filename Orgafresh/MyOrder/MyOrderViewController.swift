//
//  MyOrderViewController.swift
//  Orgafresh
//
//  Copyright © 2020 Innovation Incubator. All rights reserved.
//

import UIKit
import SVProgressHUD
import SwiftyJSON

class myOrderCell: UITableViewCell{
    
    @IBOutlet weak var orderDateLabel: UILabel!
    @IBOutlet weak var orderIdLabel: UILabel!
    @IBOutlet weak var orderRateLabel: UILabel!
    @IBOutlet weak var paymentModeLabel: UILabel!
    @IBOutlet weak var paymentstatusLabel: UILabel!
    
}

class MyOrderViewController: UIViewController,UITableViewDataSource,UITableViewDelegate  {
    
    
    @IBOutlet weak var OrderTableView: UITableView!
    @IBOutlet weak var CartButton: SSBadgeButton!
    
    @IBOutlet weak var tableBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var gradientVwTopConstrain    : NSLayoutConstraint!
    @IBOutlet weak var gradientVwHeightConstrain : NSLayoutConstraint!
    var allOrderValues = [JSON]()
    var resultArray = JSON()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if DeviceType.IS_IPHONE_X || DeviceType.IS_IPHONE_XP{
            
            gradientVwTopConstrain.constant = 43
            gradientVwHeightConstrain.constant = 55
            
            
        }else{
            
            gradientVwTopConstrain.constant = 20
            gradientVwHeightConstrain.constant = 55
        }
        
        if CartCount == "0"
        {
            CartButton.badge = ""
            CartButton.badgeLabel.backgroundColor = UIColor.clear
        }
        else
        {
            CartButton.badge = "\(CartCount)"
        }
        OrderTableView.delegate = self
        OrderTableView.dataSource = self
        getallorder()
        
    }
    
    //MARK: - TableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return allOrderValues.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let orderCell:myOrderCell = OrderTableView.dequeueReusableCell(withIdentifier: "myOrderCell") as! myOrderCell
        
        let str = convertDateFormater(date: allOrderValues[indexPath.row]["updated_at"].stringValue)
        
        
        orderCell.orderDateLabel.text = "Order Date : " + str
        orderCell.orderIdLabel.text = "Order # : " + "\(allOrderValues[indexPath.row]["user_id"].intValue)"
        orderCell.orderRateLabel.text = "Rate : Rs." + "\(allOrderValues[indexPath.row]["wallet"].intValue)"
        orderCell.paymentModeLabel.text = "Payment Mode : " + allOrderValues[indexPath.row]["payment_type"].stringValue.uppercased()
        
        if allOrderValues[indexPath.row]["status"].intValue == 0
        {
            orderCell.paymentstatusLabel.text = "Order Placed"
        }
        
        return orderCell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
        let ItemsOrderDetailsVC  = storyBoard.instantiateViewController(withIdentifier: "ItemsOrderDetailsViewController") as! ItemsOrderDetailsViewController
        ItemsOrderDetailsVC.summaryValues = allOrderValues[indexPath.row]
        ItemsOrderDetailsVC.ItemOrderid = "\(allOrderValues[indexPath.row]["id"].intValue)"
        self.navigationController?.pushViewController(ItemsOrderDetailsVC, animated: false)
    }
    
    func convertDateFormater(date: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ"        
        guard let date = dateFormatter.date(from: date) else {
            assert(false, "no date from string")
            return ""
        }
        
        dateFormatter.dateFormat = "MMM dd,yyyy"
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        let timeStamp = dateFormatter.string(from: date)
        
        return timeStamp
    }
    //MARK: - Button
    
    @IBAction func BackButtonAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func NotificationAction(_ sender: Any) {
        let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
        let NotificationViewController  = storyBoard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(NotificationViewController, animated: false)
    }
    @IBAction func AddCartAction(_ sender: Any) {
        if CartCount == "0"
        {
            GlobalClass.showAlertDialog(message: "You dont have any item on your cart", target: self)
            
        }
        else{
            let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
            let MyOrderViewController  = storyBoard.instantiateViewController(withIdentifier: "CartListViewController") as! CartListViewController
            self.navigationController?.pushViewController(MyOrderViewController, animated: false)
        }
    }
    func getallorder() {
        
        DispatchQueue.main.async {
            
        SVProgressHUD.show(withStatus: "Loading...")
            
        }
        
        networkProvider.request(.getallorder, completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                let jsonVal = JSON(response.data)
                
                
                print(jsonVal["order"])
                
                if jsonVal["status"].stringValue == "success"{
                    // self.allOrderValues = jsonVal["order"]
                    self.resultArray = jsonVal["order"]
                    self.resultArray.reversed().forEach {
                        
                        self.allOrderValues.append($1)
                    }
                    if self.allOrderValues.count == 0
                    {
                        self.OrderTableView.isHidden = true
                    }
                    else
                    {
                        self.OrderTableView.isHidden = false
                        DispatchQueue.main.async {
                            self.OrderTableView.reloadData()
                        }
                        
                    }
                    SVProgressHUD.dismiss()

                }
                
            case .failure( _):
                SVProgressHUD.dismiss()
                
                break
            }
        })
    }
    
}
