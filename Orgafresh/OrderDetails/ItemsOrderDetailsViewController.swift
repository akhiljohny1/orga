//
//  ItemsOrderDetailsViewController.swift
//  Orgafresh
//
//

import UIKit
import SwiftyJSON
import SVProgressHUD
import Alamofire


class ItemsTableViewCell: UITableViewCell {

@IBOutlet weak var product_name: UILabel!
@IBOutlet weak var unit_name: UILabel!
@IBOutlet weak var total_amount: UILabel!
@IBOutlet weak var quantity: UILabel!


}

class ItemsOrderDetailsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

var TempCartValues = [JSON]()
var CartArray = JSON()

@IBOutlet weak var CartButton: SSBadgeButton!

@IBOutlet weak var itemUnderLineView: UIView!
@IBOutlet weak var scrollUnderView: UIView!
var ItemOrderid = String()
var OrderValues = JSON()
var summaryValues = JSON()
@IBOutlet weak var summaryButton: UIButton!
@IBOutlet weak var itemButton: UIButton!

@IBOutlet weak var ItemsTableView: UITableView!
@IBOutlet weak var centerTableviewConstraint: NSLayoutConstraint!

@IBOutlet weak var delivery_dateLabel: UILabel!
@IBOutlet weak var responseLabel: UILabel!
@IBOutlet weak var orderPlacedLabel: UILabel!
@IBOutlet weak var address_line1Label: UILabel!
@IBOutlet weak var user_idLabel: UILabel!

@IBOutlet weak var quantityLabel: UILabel!
@IBOutlet weak var payment_typeLabel: UILabel!
@IBOutlet weak var bill_amountLabel: UILabel!
@IBOutlet weak var delivery_amountLabel: UILabel!
@IBOutlet weak var ChargeLabel: UILabel!

@IBOutlet weak var TotalLabel: UILabel!

@IBOutlet weak var summaryView: UIView!
@IBOutlet weak var backVwHeight   : NSLayoutConstraint!

@IBOutlet weak var scrollVwHeight   : NSLayoutConstraint!
@IBOutlet weak var gradientVwHeightConstrain : NSLayoutConstraint!
@IBOutlet weak var gradientVwTopConstrain    : NSLayoutConstraint!

override func viewDidLoad() {
    super.viewDidLoad()
    if DeviceType.IS_IPHONE_X || DeviceType.IS_IPHONE_XP{
       
        gradientVwTopConstrain.constant = 0

       gradientVwHeightConstrain.constant = 55
    }else
    {
        gradientVwTopConstrain.constant = 0

      gradientVwHeightConstrain.constant = 55
    }
    
    ItemsTableView.delegate = self
    ItemsTableView.dataSource = self
    getordrbyorderid()
    
    delivery_dateLabel.text = summaryValues["delivery_date"].stringValue
    responseLabel.text = summaryValues["response"].stringValue
    if summaryValues["status"].stringValue == "0"
    {
        orderPlacedLabel.text = "Order Placed"
    }
    address_line1Label.text = summaryValues["address_line1"].stringValue
    user_idLabel.text = "\(summaryValues["user_id"].intValue)"
    payment_typeLabel.text = summaryValues["payment_type"].stringValue.uppercased()
    quantityLabel.text = "\(summaryValues["quantity"].intValue)"
    bill_amountLabel.text = "\(summaryValues["bill_amount"].intValue)"
    delivery_amountLabel.text = "\(summaryValues["delivery_amount"].intValue)"
    ChargeLabel.text = ""
    TotalLabel.text = "\(summaryValues["total_amount"].intValue)"
    
    summaryView.isHidden = false
    ItemsTableView.isHidden = true
    itemUnderLineView.isHidden = true
    //reteriveTempData()
}

override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    if CartCount == "0"
                         {
                             CartButton.badge = ""
                             CartButton.badgeLabel.backgroundColor = UIColor.clear
                         }
                         else
                         {
                   CartButton.badge = "\(CartCount)"
                   }
}



//MARK: - TableView

func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return OrderValues.count
    
}

func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let ItemsCell:ItemsTableViewCell = ItemsTableView.dequeueReusableCell(withIdentifier: "ItemsTableViewCell") as! ItemsTableViewCell
    ItemsCell.product_name.text = OrderValues[indexPath.row]["product_name"].stringValue.firstUppercased
    ItemsCell.unit_name.text = OrderValues[indexPath.row]["unit_name"].stringValue
    ItemsCell.total_amount.text = "Rs." + OrderValues[indexPath.row]["total_amount"].stringValue
    ItemsCell.quantity.text = "Quantity : " + OrderValues[indexPath.row]["quantity"].stringValue
    let imgVw = ItemsCell.viewWithTag(1) as! UIImageView
    var str = imageBaseUrl + OrderValues[indexPath.row]["product_img"].stringValue

    str = str.replacingOccurrences(of: " ", with: "%20")
          imgVw.sd_setImage(with: URL(string: str), placeholderImage: nil)
  //  imgVw.sd_setImage(with: URL(string: imageBaseUrl + OrderValues[indexPath.row]["product_img"].stringValue), placeholderImage: nil)
    
    return ItemsCell
}
//MARK: - Button Action

@IBAction func SummaryAction(_ sender: UIButton) {
    summaryView.isHidden = false
    ItemsTableView.isHidden = true
    itemUnderLineView.isHidden = true
    scrollUnderView.isHidden = false
    
    summaryButton.setTitleColor(.white, for: .normal)
    itemButton.setTitleColor(.darkGray, for: .normal)
    
    summaryView.transform = CGAffineTransform(translationX: -(self.view.bounds.width), y: 0)
    scrollUnderView.transform = CGAffineTransform(translationX: self.summaryButton.bounds.width, y: 0)
    
    UIView.animate(withDuration:0.7, delay: 0.00 , usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: UIView.AnimationOptions.curveLinear, animations: {
        self.summaryView.transform = CGAffineTransform(translationX: 0, y: 0);
        self.scrollUnderView.transform = CGAffineTransform(translationX: 0, y: 0);
    }, completion: nil)
}

@IBAction func ItemsAction(_ sender: UIButton) {
    summaryView.isHidden = true
    ItemsTableView.isHidden = false
    itemUnderLineView.isHidden = false
    scrollUnderView.isHidden = true
    animateTable()
    summaryButton.setTitleColor(.darkGray, for: .normal)
    itemButton.setTitleColor(.white, for: .normal)
}



func animateTable() {
    ItemsTableView.reloadData()
    let cells = ItemsTableView.visibleCells
    let tableWidth: CGFloat = ItemsTableView.bounds.size.width
    
    for i in cells {
        let cell: UITableViewCell = i as UITableViewCell
        cell.transform = CGAffineTransform(translationX: tableWidth, y: 0)
    }
    itemUnderLineView.transform = CGAffineTransform(translationX: -(self.itemButton.bounds.width), y: 0)
    
    var index = 0
    for a in cells {
        let cell: UITableViewCell = a as UITableViewCell
        UIView.animate(withDuration:0.7, delay: 0.00 , usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: UIView.AnimationOptions.curveLinear, animations: {
            cell.transform = CGAffineTransform(translationX: 0, y: 0);
            //  self.centerTableviewConstraint.constant += self.view.bounds.width
            self.itemUnderLineView.transform = CGAffineTransform(translationX: 0 , y: 0)
            
        }, completion: nil)
        
        index += 1
    }
}

@IBAction func reOrderButtonAction(_ sender: Any) {
    ReOrder()
}

func ReOrder(){
    
    let alert = UIAlertController(title: "Re-Order", message: "Are you sure want to re-order this item ?", preferredStyle: UIAlertController.Style.alert)
    
    // add the actions (buttons)
    
    alert.addAction(UIAlertAction(title: "NO", style: UIAlertAction.Style.cancel, handler: nil))
    
    // show the alert
    self.present(alert, animated: true, completion: nil)
    
    
    alert.addAction(UIAlertAction(title: "YES", style: UIAlertAction.Style.default, handler: { action in
        self.proceedReOrder()
    }))
    
}
func proceedReOrder(){
    let alert = UIAlertController(title: "Re-Order", message: "All available Products were added on your Cart and ready to checkout", preferredStyle: UIAlertController.Style.alert)
    
    // add the actions (buttons)
    
    alert.addAction(UIAlertAction(title: "CANCEL", style: UIAlertAction.Style.cancel, handler: nil))
    
    // show the alert
    self.present(alert, animated: true, completion: nil)
    
    
    alert.addAction(UIAlertAction(title: "PROCEED", style: UIAlertAction.Style.default, handler: { action in
        
        self.addTempCartValues()
        
    }))
}

func addTempCartValues()
{
    print(OrderValues)
    TempCartValues.removeAll()
    for i in 0 ..< OrderValues.count{
        CartArray["product_name"] = JSON(OrderValues[i]["product_name"].stringValue)
        //  CartArray["normal_price"] = JSON(OrderValues[i]["total_amount"].stringValue)
        let selling_price = OrderValues[i]["total_amount"].intValue / OrderValues[i]["quantity"].intValue
        
        CartArray["selling_price"] = JSON(selling_price)
        CartArray["quantity"] = JSON(OrderValues[i]["quantity"].stringValue)
        
        //CartArray["minimum_order_quantity"] = JSON(OrderValues[i]["minimum_order_quantity"].stringValue)
        CartArray["unit_name"] = JSON(OrderValues[i]["unit_name"].stringValue)
        CartArray["product_img"] = JSON(OrderValues[i]["product_img"].stringValue)
        
        CartArray["id"] = JSON(OrderValues[i]["product_id"].stringValue)
        TempCartValues.append(CartArray)
    }
    
    if TempCartValues.count == 0
            {
                CartButton.badge = ""
                CartButton.badgeLabel.backgroundColor = UIColor.clear
            }
            else
            {
      CartButton.badge = "\(TempCartValues.count)"
      }
    
    CartCount = "\(TempCartValues.count)"
    
    // print(JSON(TempCartValues).rawValue)
    
    UserDefaults.standard.setValue(JSON("\(TempCartValues.count)").rawValue, forKey: "CartCount")
    UserDefaults.standard.synchronize()
    appDelegate.order_status = self.summaryValues["status"].stringValue
    appDelegate.order_response = self.summaryValues["response"].stringValue
    appDelegate.order_id = self.summaryValues["id"].stringValue
    
    
    
    
    UserDefaults.standard.setValue(JSON(TempCartValues).rawValue, forKey: "TempCartValues")
    UserDefaults.standard.synchronize()
    
    let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
    let CartListVC = storyBoard.instantiateViewController(withIdentifier: "CartListViewController") as! CartListViewController
    self.navigationController?.pushViewController(CartListVC, animated: false)
    
    
}
func reteriveTempData()
{
    
    let vals = UserDefaults.standard.value(forKey: "TempCartValues") as Any
    
    print("vals : \(JSON(vals))")
    let retrivedVals = JSON(vals)
    
    TempCartValues.removeAll()
    
    for i in 0 ..< retrivedVals.count{
        TempCartValues.append(retrivedVals[i])
    }
}

@IBAction func BackAction(_ sender: Any) {
    
    self.navigationController?.popViewController(animated: true)
}

@IBAction func AddCartAction(_ sender: Any) {
    if CartCount == "0"
    {
        GlobalClass.showAlertDialog(message: "You dont have any item on your cart", target: self)
        
    }
    else{
        let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
        let MyOrderViewController  = storyBoard.instantiateViewController(withIdentifier: "CartListViewController") as! CartListViewController
        self.navigationController?.pushViewController(MyOrderViewController, animated: false)
    }
}
@IBAction func NotificationAction(_ sender: Any) {
    let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
    let NotificationViewController  = storyBoard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
    self.navigationController?.pushViewController(NotificationViewController, animated: false)
}
func getordrbyorderid(){
    
    SVProgressHUD.show(withStatus: "Loading...")
    
    let parameter: Parameters  = ["order_id" : ItemOrderid]
    print("getordrbyorderid params : \(parameter)")
    
    networkProvider.request(.getordrbyorderid(parameter: parameter), completion: {(result) in
        
        switch result {
            
        case .success(let response):
            SVProgressHUD.dismiss()
            
            let jsonVal = JSON(response.data)
            
            print("Json val : \(jsonVal)")
            
            print(jsonVal["status"].stringValue)
            
            if jsonVal["status"].stringValue == "success"{
                self.OrderValues = jsonVal["productdetails"]
                self.ItemsTableView.reloadData()
                
            }
            
        case .failure( _):
            SVProgressHUD.dismiss()
            
            break
        }
    })
}
}
