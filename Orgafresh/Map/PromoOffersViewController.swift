//
//  PromoOffersViewController.swift
//  Orgafresh
//
//  Created by ani david on 06/09/20.
//  Copyright © 2020 Innovation Incubator. All rights reserved.
//

import UIKit

class PromoOffersViewController: UIViewController {
    @IBOutlet weak var CartButton: SSBadgeButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
          
          CartButton.badge = "\(CartCount)"

      }
    @IBAction func BackAction(_ sender: Any) {
          
          self.navigationController?.popViewController(animated: true)
      }
    @IBAction func AddCartAction(_ sender: Any) {
              
              let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
              let MyOrderViewController  = storyBoard.instantiateViewController(withIdentifier: "CartListViewController") as! CartListViewController
          self.navigationController?.pushViewController(MyOrderViewController, animated: false)
          }
        @IBAction func NotificationAction(_ sender: Any) {
               let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
                      let NotificationViewController  = storyBoard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
                      self.navigationController?.pushViewController(NotificationViewController, animated: false)
           }

}
