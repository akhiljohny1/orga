//
//  MapViewController.swift
//  Orgafresh
//
//  Copyright © 2020 Innovation Incubator. All rights reserved.
//

import UIKit
import GooglePlaces
import Contacts
import SwiftyJSON
import GoogleMaps
import CoreLocation
import MapKit
import SVProgressHUD
import Alamofire
import GooglePlaces

var warehouse_id = Int()


class AddSymbolAddressCell: UICollectionViewCell{
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
}

class SavedAddressCell: UICollectionViewCell{
    
    @IBOutlet weak var MarqueeLabel: UILabel!
    @IBOutlet weak var SelectButton: UIButton!
    @IBOutlet weak var NameLabel: UITextField!
    @IBOutlet weak var BuildingLabel: UITextField!
    @IBOutlet weak var PhoneNumberLabel: UITextField!
    private var placesClient: GMSPlacesClient!
    @IBOutlet weak var location_type: UILabel!
    @IBOutlet weak var home_type: UIButton!
    @IBOutlet weak var office_type: UIButton!
    @IBOutlet weak var other_type: UIButton!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        _ = Timer.scheduledTimer(timeInterval: 0.3, target: self, selector: #selector(SavedAddressCell.marquee), userInfo: nil, repeats: true)
    }
}

class MapViewController: UIViewController,UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource,CLLocationManagerDelegate,GMSMapViewDelegate,UITextFieldDelegate {
    var newPlaceCoordinate = CLLocationCoordinate2D()
    
    @IBOutlet weak var AdressCollectionView: UICollectionView!
    @IBOutlet weak var SaveAddressView: UIView!
    @IBOutlet weak var nameLabel: UITextField!
    @IBOutlet weak var AdressLabel: UITextField!
    @IBOutlet weak var MarqueeLabel: UILabel!
    @IBOutlet weak var PhoneNumberLabel: UITextField!
    private var placesClient: GMSPlacesClient!
    @IBOutlet weak var MapViewArea: GMSMapView!
    
    @IBOutlet weak var homeButton: UIButton!
    @IBOutlet weak var officeButton: UIButton!
    @IBOutlet weak var Othersbutton: UIButton!
    
    var pincodeString = ""
    var addres_line_1String = ""
    var addres_line_2String = ""
    var addres_line_3String = ""
    var addres_line_4String = ""
    var location_type = ""
    
    
    
    var AddressArray = JSON()
    var addressSave : String = ""
    //   var TempAddressValues = [JSON]()
    var TempAddressValues = JSON()
    var warehouseValues = JSON()
    var ComparedDistanceValue = 0.00
    
    var locationManager             : CLLocationManager!
    
    var Currentlatitude = 0.0
    var Currentlongitude = 0.0
    
    
    @IBOutlet weak var gradientVwTopConstrain    : NSLayoutConstraint!
    @IBOutlet weak var gradientVwHeightConstrain : NSLayoutConstraint!
    
    var currentMarker = GMSMarker()
    
    
    @IBOutlet weak var backButton: UIButton!
    var resultsViewController: GMSAutocompleteResultsViewController?
    var searchController: UISearchController?
    var resultView: UITextView?
    
    @IBOutlet weak var GradientView: GradientView!
    
    @IBOutlet weak var mapBackgroundView: UIView!
    var availabilityFlag = "no"
    override func viewDidLoad() {
        super.viewDidLoad()
        location_type = "HOME"
        
        if DeviceType.IS_IPHONE_X || DeviceType.IS_IPHONE_XP{
            
            
        }else{
            
        }
        MarqueeLabel.text = " Please select address"
        
        resultsViewController = GMSAutocompleteResultsViewController()
        resultsViewController?.delegate = self
        
        searchController = UISearchController(searchResultsController: resultsViewController)
        searchController?.searchResultsUpdater = resultsViewController
        
        let subView = UIView(frame: CGRect(x: 0, y: 52, width: 400, height: 100))
        
        subView.addSubview((searchController?.searchBar)!)
        mapBackgroundView.addSubview(subView)
        searchController?.searchBar.sizeToFit()
        searchController?.searchBar.searchBarStyle = .minimal
        
        searchController?.searchBar.barTintColor = UIColor.lightGray
        searchController?.hidesNavigationBarDuringPresentation = false
        definesPresentationContext = true
                
    }
    
    override func viewWillAppear(_ animated: Bool) {
        availabilityFlag = "no"
        SaveAddressView.isHidden = true
        AdressCollectionView.isHidden = false
        
        //  reteriveTempData()
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestLocation()
                
        DispatchQueue.main.asyncAfter(deadline: .now() + 3, execute: {
            
            if CLLocationManager.locationServicesEnabled(){
                self.locationManager.startUpdatingLocation()
            
            }else{
                print("location denied")
                self.locationManager.requestLocation()
            }
            
        })
        
        MapViewArea.isMyLocationEnabled = true
        MapViewArea.delegate = self
        
        if appDelegate.isLogined == false
        {
            loginalert()
        }
        else
        {
        getalladress()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
            if !self.hasLocationPermission() {
                let alertController = UIAlertController(title: "Location Permission Required", message: "Please enable location permissions in settings.", preferredStyle: UIAlertController.Style.alert)

                let okAction = UIAlertAction(title: "Settings", style: .default, handler: {(cAlertAction) in
                    //Redirect to Settings app
                    UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
                })

                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
                alertController.addAction(cancelAction)

                alertController.addAction(okAction)

                self.present(alertController, animated: true, completion: nil)
            }
        })
    }
    
    func hasLocationPermission() -> Bool {
        var hasPermission = false
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                hasPermission = false
            case .authorizedAlways, .authorizedWhenInUse:
                hasPermission = true
            }
        } else {
            hasPermission = false
        }

        return hasPermission
    }
    
    // sets the info in the custom info window
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        print("You tapped at \(coordinate.latitude), \(coordinate.longitude)")
        
        Currentlatitude = coordinate.latitude
        Currentlongitude = coordinate.longitude
        newPlaceCoordinate = coordinate
        print(newPlaceCoordinate)
        
        GeoCode()
    }
    
    
    func locationManager(_ manager: CLLocationManager,didFailWithError error: Error) {
        print(error)
    }
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        if (status == CLAuthorizationStatus.authorizedWhenInUse)
            
        {
            MapViewArea.isMyLocationEnabled = true
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("locationManager update")
        let newLocation = locations.last
        let position = GMSCameraUpdate.setTarget(newLocation!.coordinate)
        MapViewArea.moveCamera(position)
        MapViewArea.animate(toZoom: 9)
        Currentlatitude = newLocation!.coordinate.latitude
        Currentlongitude = newLocation!.coordinate.longitude
        newPlaceCoordinate = newLocation!.coordinate
        print(newPlaceCoordinate)
        MapViewArea.settings.myLocationButton = true
        MapViewArea.delegate = self
        GeoCode()
        
    }
    
    //MARK: CollectionView
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return TempAddressValues.count+1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let savedAddressCell = AdressCollectionView.dequeueReusableCell(withReuseIdentifier: "SavedAddressCell", for: indexPath) as! SavedAddressCell
        
        
        if indexPath.row == 0
        {
            let AddSymbolAddresscell = AdressCollectionView.dequeueReusableCell(withReuseIdentifier: "AddSymbolAddressCell", for: indexPath) as! AddSymbolAddressCell
            return AddSymbolAddresscell
        }
        else{
            savedAddressCell.SelectButton.tag = indexPath.row
            savedAddressCell.NameLabel.text =  "  " + TempAddressValues[indexPath.row-1]["addres_line_1"].stringValue
            savedAddressCell.MarqueeLabel.text =  "  " + TempAddressValues[indexPath.row-1]["addres_line_2"].stringValue
            savedAddressCell.BuildingLabel.text =  "  " + TempAddressValues[indexPath.row-1]["addres_line_3"].stringValue
            savedAddressCell.PhoneNumberLabel.text = "  " + TempAddressValues[indexPath.row-1]["addres_line_4"].stringValue
            let location_typecell = TempAddressValues[indexPath.row-1]["location_type"].stringValue.uppercased()
            
            print(location_typecell)
            if location_typecell == "HOME"
            {
                savedAddressCell.home_type.backgroundColor = #colorLiteral(red: 0.2, green: 0.8, blue: 1, alpha: 1)
                savedAddressCell.office_type.backgroundColor = UIColor.clear
                savedAddressCell.other_type.backgroundColor = UIColor.clear
                
            }
            else if location_typecell == "OFFICE"
            {
                savedAddressCell.home_type.backgroundColor = UIColor.clear
                savedAddressCell.office_type.backgroundColor = #colorLiteral(red: 0.2, green: 0.8, blue: 1, alpha: 1)
                savedAddressCell.other_type.backgroundColor = UIColor.clear
            }
            else if location_typecell == "OTHERS"
            {
                savedAddressCell.home_type.backgroundColor = UIColor.clear
                savedAddressCell.office_type.backgroundColor = UIColor.clear
                savedAddressCell.other_type.backgroundColor = #colorLiteral(red: 0.2, green: 0.8, blue: 1, alpha: 1)
            }
        }
        return savedAddressCell
    }
    
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let index = Int(targetContentOffset.pointee.x / view.frame.width)
        print(index)
        self.MapViewArea.clear()
        let position = CLLocationCoordinate2D(latitude: TempAddressValues[index]["loc_latitude"].doubleValue, longitude: TempAddressValues[index]["loc_longitude"].doubleValue)
        MapViewArea.camera = GMSCameraPosition.camera(withTarget: position, zoom: 12)
        
        let marker = GMSMarker(position: position)
        marker.map = self.MapViewArea
    }
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,sizeForItemAt indexPath: IndexPath) -> CGSize {
                
        let numberOfCellInRow : Int        = 1
        let padding : Int                  = 1
        let collectionCellWidth : CGFloat  = (self.AdressCollectionView.frame.size.width/CGFloat(numberOfCellInRow)) - CGFloat(padding)
        
        return CGSize(width: collectionCellWidth, height: 400)
        
    }
    
    @IBAction func plusAddAddressAction(_ sender: Any) {
        SaveAddressView.isHidden = false
        AdressCollectionView.isHidden = true
        // DispatchQueue.main.asyncAfter(deadline: .now() + 3, execute: {
        
        if CLLocationManager.locationServicesEnabled(){
            self.locationManager.startUpdatingLocation()
        }else{
            
            self.locationManager.requestLocation()
        }
        //     })
    }
    var selectedRow = Int()
    @IBAction func SelectAddressAction(_ sender: UIButton) {
        selectedRow = sender.tag-1
        
        distanceSelectedFromArray()
        if availabilityFlag == "yes"
        {
            Selected_AddressValues.insert(TempAddressValues[sender.tag-1], at: 0)
            print(Selected_AddressValues)
            
            let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
            let ProceedViewControlle  = storyBoard.instantiateViewController(withIdentifier: "ProceedViewController") as! ProceedViewController
            self.navigationController?.pushViewController(ProceedViewControlle, animated: true)
            
        }else{
            availabilityFlag = "no"
            GlobalClass.showAlertDialog(message: "Not available in particular location", target: self)
        }
        
        
    }
    
    @IBAction func backButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func SaveAddress(_ sender: Any) {
        distance()
        
        if availabilityFlag == "yes"
        {
            validate()
            
        }else{
            availabilityFlag = "no"
            nameLabel.text = ""
            GlobalClass.showAlertDialog(message: "Not available in particular location", target: self)
        }
        
    }
    @IBAction func homeAddressTypeAction(_ sender: Any) {
        homeButton.backgroundColor = #colorLiteral(red: 0.2, green: 0.8, blue: 1, alpha: 1)
        Othersbutton.backgroundColor = UIColor.clear
        officeButton.backgroundColor = UIColor.clear
        location_type = "home"
        
        
    }
    @IBAction func OfficeAddressTypeAction(_ sender: Any) {
        homeButton.backgroundColor = UIColor.clear
        officeButton.backgroundColor = #colorLiteral(red: 0.2, green: 0.8, blue: 1, alpha: 1)
        Othersbutton.backgroundColor = UIColor.clear
        location_type = "office"
        
    }
    
    @IBAction func OthersAddressTypeAction(_ sender: Any) {
        homeButton.backgroundColor = UIColor.clear
        officeButton.backgroundColor = UIColor.clear
        Othersbutton.backgroundColor = #colorLiteral(red: 0.2, green: 0.8, blue: 1, alpha: 1)
        location_type = "others"
        
    }
    
    //MARK: API
    
    func saveAddressfunc(){
        
           SVProgressHUD.show(withStatus: "Loading...")
        
        let parameter: Parameters = ["location_type" : location_type, "location_name" : location_type, "user_id" : appDelegate.publicUserID,"addres_line_1" : nameLabel.text!.trimmingCharacters(in: .whitespacesAndNewlines), "addres_line_2" : addressSave, "addres_line_3" : AdressLabel.text!.trimmingCharacters(in: .whitespacesAndNewlines),"addres_line_4" : PhoneNumberLabel.text!.trimmingCharacters(in: .whitespacesAndNewlines), "pincode" : pincodeString, "loc_longitude" : Currentlongitude,"loc_latitude" : Currentlatitude]
        
        print("noti params : \(parameter)")
        
        networkProvider.request(.saveaddress(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                let jsonVal = JSON(response.data)
                
                print("Json val : \(jsonVal)")
                
                print(jsonVal["status"].stringValue)
                
                if jsonVal["status"].stringValue == "success"{
                    
                    self.AddressArray["addres_line_2"] = JSON(self.AdressLabel.text!.trimmingCharacters(in: .whitespacesAndNewlines))
                    
                    self.AddressArray["addres_line_4"] = JSON(self.PhoneNumberLabel.text!.trimmingCharacters(in: .whitespacesAndNewlines))
                    self.AddressArray["loc_latitude"] = JSON(self.Currentlatitude)
                    self.AddressArray["loc_longitude"] = JSON(self.Currentlongitude)
                    self.AddressArray["addres_line_1"] = JSON(self.nameLabel.text!.trimmingCharacters(in: .whitespacesAndNewlines))
                    self.AddressArray["addres_line_3"] = JSON(self.AdressLabel.text!.trimmingCharacters(in: .whitespacesAndNewlines))
                    self.AddressArray["pincode"] = JSON(self.pincodeString)
                    self.AddressArray["location_type"] = JSON(self.location_type)
                    self.AddressArray["id"] = JSON(jsonVal["addressid"].stringValue)
                    
                    
                    //  Selected_AddressValues.remove(at: 0)
                    Selected_AddressValues.insert(self.AddressArray, at: 0)
                    print(Selected_AddressValues)
                    
                    self.nameLabel.text! = ""
                    self.PhoneNumberLabel.text! = ""
                    self.AdressLabel.text! = ""
                    
                    SVProgressHUD.dismiss()
                    
                    let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
                    let ProceedViewControlle  = storyBoard.instantiateViewController(withIdentifier: "ProceedViewController") as! ProceedViewController
                    self.navigationController?.pushViewController(ProceedViewControlle, animated: true)
                }
                
            case .failure( _):
                SVProgressHUD.dismiss()
                
                break
            }
        })
    }
    
    func getalladress()
    {
        
        DispatchQueue.main.async {
        
        SVProgressHUD.show(withStatus: "Loading...")
            
        }
        
        networkProvider.request(.getalladdress, completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                SVProgressHUD.dismiss()
                
                let jsonVal = JSON(response.data)
                
                print("Json val : \(jsonVal)")
                
                // print(jsonVal["status"].stringValue)
                
                if response.statusCode == 200{
                    
                    if jsonVal["status"].stringValue == "success"{
                        
                        self.TempAddressValues = jsonVal["address"]
                        self.warehouseValues = jsonVal["warehouse"]
                        print(self.warehouseValues )
                        warehouse_id = self.warehouseValues[0]["id"].intValue
                        
                        //    print(self.warehouseValues)
                        
                    }else{
                        SVProgressHUD.dismiss()
                        
                        GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                    }
                    
                }else{
                    self.loginalert()
                }
//                else if response.statusCode == 401{
//
//                    GlobalClass.callGetApiKey(viewController: self)
//                    if appDelegate.isLogined == false
//                    {
//                        self.loginalert()
//                    }
//                    GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
//
//                }else if jsonVal["message"].stringValue.contains("token"){
//
//                    GlobalClass.callGetApiKey(viewController: self)
//                }
//                else if response.statusCode == 500{
//
//                    GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
//                }
//                else{
//
//                    GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
//                }
                
                
                self.AdressCollectionView.reloadData()
                
            case .failure( _):
                
                SVProgressHUD.dismiss()
                
                break
            }
        })
    }
    
    
    
    func loginalert()
    {
        
        let alert = UIAlertController(title: "Error", message: "Your LoginIn session got expired. Please Login again to proceed", preferredStyle: UIAlertController.Style.alert)
        
        // add the actions (buttons)
        
        alert.addAction(UIAlertAction(title: "CANCEL", style: UIAlertAction.Style.cancel, handler: { action in
            self.navigationController?.popViewController(animated: true)
            
        }))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
        
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { action in
            
            appDelegate.isLogined = false
            appDelegate.UserName = ""
            appDelegate.loginMobileNumber = ""
            appDelegate.loginEmailAddress = ""
            UserDefaults.standard.set("false4", forKey: "isUserLogined")
            UserDefaults.standard.set(nil, forKey: "notiArray")
            UserDefaults.standard.synchronize()
            
            let storyBoard          = UIStoryboard(name: "Main", bundle: nil)
            let LoginViewControlle  = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.navigationController?.pushViewController(LoginViewControlle, animated: false)
        }))
        
    }
    
    //Distance Calculation
    func distance()
    {
        print(Currentlatitude)
        
        let point1 = CLLocation(latitude: Currentlatitude, longitude: Currentlongitude)
        for index in 0 ..< self.warehouseValues.count
        {
            let point2 = CLLocation(latitude: self.warehouseValues[index]["address_latitude"].doubleValue, longitude: self.warehouseValues[index]["address_longitude"].doubleValue)
            
            let distanceInkm = point1.distance(from: point2) / 1000
            ComparedDistanceValue = distanceInkm
            print(distanceInkm)
            let warehouseDistance = Double(self.warehouseValues[index]["distance"].stringValue) ?? 0.00
            if ComparedDistanceValue <= warehouseDistance
            {
                availabilityFlag = "yes"
                warehouse_id = self.warehouseValues[index]["id"].intValue
            }
        }
    }
    func distanceSelectedFromArray()
    {
        
        print(self.warehouseValues)
        print(selectedRow)
        print(TempAddressValues[selectedRow])
        print(TempAddressValues[selectedRow]["loc_latitude"].doubleValue)
        
        let point1 = CLLocation(latitude: TempAddressValues[selectedRow]["loc_latitude"].doubleValue, longitude: TempAddressValues[selectedRow]["loc_longitude"].doubleValue)
        print(point1)
        for index in 0...self.warehouseValues.count-1
        {
            let point2 = CLLocation(latitude: self.warehouseValues[index]["address_latitude"].doubleValue, longitude: self.warehouseValues[index]["address_longitude"].doubleValue)
            print(point2)
            let distanceInkm = point1.distance(from: point2) / 1000
            ComparedDistanceValue = distanceInkm
            print(distanceInkm)
            print("number of warehouse")
            let warehouseDistance = Double(self.warehouseValues[index]["distance"].stringValue) ?? 0.00
            if ComparedDistanceValue <= warehouseDistance
            {
                availabilityFlag = "yes"
                warehouse_id = self.warehouseValues[index]["id"].intValue
            }
        }
        
    }
    //MARK: CurrentLocation GeoCode
    func GeoCode(){
        
        CLGeocoder().reverseGeocodeLocation(CLLocation.init(latitude: Currentlatitude, longitude:Currentlongitude)) { (places, error) in
            if error == nil{
                let placeMark = places! as [CLPlacemark]
                
                if placeMark.count > 0 {
                    let placeMark = places![0]
                    var addressString : String = ""
                    
                    if placeMark.subThoroughfare != nil {
                        addressString = addressString + placeMark.subThoroughfare! + ", "
                    }
                    if placeMark.thoroughfare != nil {
                        addressString = addressString + placeMark.thoroughfare! + ", "
                    }
                    if placeMark.subLocality != nil {
                        addressString = addressString + placeMark.subLocality! + ", "
                        self.addres_line_1String = placeMark.subLocality!
                        print("self.addres_line_1String",self.addres_line_1String)
                    }
                    
                    if placeMark.locality != nil {
                        addressString = addressString + placeMark.locality! + ", "
                        self.addres_line_2String = placeMark.locality!
                        print("self.addres_line_2String",self.addres_line_2String)
                        
                        
                    }
                    if placeMark.administrativeArea != nil {
                        addressString = addressString + placeMark.administrativeArea! + ", "
                        self.addres_line_3String = placeMark.administrativeArea!
                        print("self.addres_line_3String",self.addres_line_3String)
                        
                        
                    }
                    if placeMark.country != nil {
                        addressString = addressString + placeMark.country! + ", "
                        self.addres_line_4String = placeMark.country!
                        
                    }
                    if placeMark.postalCode != nil {
                        addressString = addressString + placeMark.postalCode! + " "
                        self.pincodeString = placeMark.postalCode!
                        
                    }
                    
                    print(addressString)
                    self.MarqueeLabel.text = "        " + addressString
                    self.addressSave = addressString
                    
                    self.MapViewArea.clear()
                    let position = CLLocationCoordinate2D(latitude: self.Currentlatitude, longitude: self.Currentlongitude)
                    let marker = GMSMarker(position: position)
                    marker.title = self.addres_line_4String
                    marker.snippet = self.addres_line_3String + self.addres_line_4String
                    marker.map = self.MapViewArea
                    _ = Timer.scheduledTimer(timeInterval: 0.3, target: self, selector: #selector(self.marquee1), userInfo: nil, repeats: true)
                }
            }
        }
    }
    @objc func marquee1(){
        let str = MarqueeLabel.text!
        let indexFirst = str.index(str.startIndex, offsetBy: 0)
        let indexSecond = str.index(str.startIndex, offsetBy: 1)
        MarqueeLabel.text = String(str.suffix(from: indexSecond)) + String(str[indexFirst])
    }
    
    //MARK: Validate TextField
    
    func validate(){
        
        if nameLabel.text!.isEmpty || AdressLabel.text!.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            
            if nameLabel.text!.isEmpty || AdressLabel.text!.isEmpty{
                
                GlobalClass.showAlertDialog(message: "Name and Address are mandatory", target: self)
                return
            }
            
            if PhoneNumberLabel.text!.count < 7{
                
                GlobalClass.showAlertDialog(message: "Please enter valid phone number.", target: self)
                return
            }
            if !GlobalClass.validatePHnumber(enteredNumber: PhoneNumberLabel.text!){
                
                GlobalClass.showAlertDialog(message: "Please enter valid phone number.", target: self)
                return
            }
            
            saveAddressfunc()
            
        }else{
            
            if !GlobalClass.validatePHnumber(enteredNumber: PhoneNumberLabel.text!){
                
                GlobalClass.showAlertDialog(message: "Please enter valid phone number.", target: self)
                return
            }
            
            saveAddressfunc()
            
        }
        
    }
    
    //UITextField delegate method
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == nameLabel {
            textField.resignFirstResponder()//
            AdressLabel.becomeFirstResponder()//TF2 will respond immediately after TF1 resign.
        } else if textField == AdressLabel  {
            textField.resignFirstResponder()
            PhoneNumberLabel.becomeFirstResponder()//TF3 will respond first
        } else if textField == PhoneNumberLabel {
            textField.resignFirstResponder()
        }
        return true
    }
}
extension SavedAddressCell {
    @objc func marquee(){
        let str = MarqueeLabel.text!
        let indexFirst = str.index(str.startIndex, offsetBy: 0)
        let indexSecond = str.index(str.startIndex, offsetBy: 1)
        MarqueeLabel.text = String(str.suffix(from: indexSecond)) + String(str[indexFirst])
    }
}

// Handle the user's selection.
extension MapViewController: GMSAutocompleteResultsViewControllerDelegate {
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didAutocompleteWith place: GMSPlace) {
        searchController?.isActive = false
        
        Currentlatitude = place.coordinate.latitude
        Currentlongitude = place.coordinate.longitude
        newPlaceCoordinate = place.coordinate
        print(newPlaceCoordinate)
        GeoCode()
        let position = GMSCameraUpdate.setTarget(place.coordinate)
        MapViewArea.moveCamera(position)
        MapViewArea.animate(toZoom: 9)
        MapViewArea.settings.myLocationButton = true
        MapViewArea.delegate = self
        searchController?.searchBar.text = place.name
    }
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didFailAutocompleteWithError error: Error){
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(forResultsController resultsController: GMSAutocompleteResultsViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(forResultsController resultsController: GMSAutocompleteResultsViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}
