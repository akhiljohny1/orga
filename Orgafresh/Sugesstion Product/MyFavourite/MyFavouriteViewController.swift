//
//  MyFavouriteViewController.swift
//  Orgafresh
//
//  Created by ani david on 16/08/20.
//  Copyright © 2020 Innovation Incubator. All rights reserved.
//

import UIKit
import SwiftyJSON

class FavouriteCell: UICollectionViewCell{
    
    @IBOutlet weak var PriceLabel: UILabel!
    @IBOutlet weak var DescriptionLabel: UILabel!

    @IBOutlet weak var GramsLabel: UILabel!
    @IBOutlet weak var sellingPriceLabel: UILabel!
    @IBOutlet weak var CloseButton: UIButton!

    
}
class MyFavouriteViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
   @IBOutlet weak var CartButton: SSBadgeButton!



    @IBOutlet weak var TitleLabel: UILabel!

    @IBOutlet weak var FavoriteCollectionView: UICollectionView!

    
    var vals2 = [JSON]()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        TitleLabel.text = "My Favourite"
        CartButton.badge = "\(CartCount)"

        let vals = UserDefaults.standard.value(forKey: "myfavourite") as Any
                      
                     // print("vals : \(JSON(vals))")
                      let retrivedVals = JSON(vals)

                      
                      
                      for i in 0 ..< retrivedVals.count{
                          
                          vals2.append(retrivedVals[i])
                      }
                      
                      print("vals2 : \(vals2)")
        // Do any additional setup after loading the view.
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return vals2.count

       }
       
       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
              let favouriteCell = FavoriteCollectionView.dequeueReusableCell(withReuseIdentifier: "FavouriteCell", for: indexPath) as! FavouriteCell
       favouriteCell.CloseButton.tag = indexPath.row

        favouriteCell.PriceLabel.text =  "₹" + vals2[indexPath.row]["normal_price"].stringValue
             favouriteCell.DescriptionLabel.text =   vals2[indexPath.row]["product_name"].stringValue
             favouriteCell.sellingPriceLabel.text =  "₹" + vals2[indexPath.row]["selling_price"].stringValue
//             favouriteCell.GramsLabel.text =  vals2[indexPath.row]["minimum_order_quantity"].stringValue + " " + vals2[indexPath.row]["unit_name"].stringValue
             
          
        favouriteCell.PriceLabel.attributedText = favouriteCell.PriceLabel.text!.strikeThrough()
        let imgVw = favouriteCell.viewWithTag(1) as! UIImageView
             imgVw.sd_setImage(with: URL(string: imageBaseUrl + vals2[indexPath.row]["product_img"].stringValue), placeholderImage: nil)
        
           return favouriteCell

       }
    
     func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
print(indexPath.row)
       let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
       let ProductDetailsVC  = storyBoard.instantiateViewController(withIdentifier: "ProductDetailsViewController") as! ProductDetailsViewController
       
       ProductDetailsVC.productValues = vals2[indexPath.row]
       self.navigationController?.pushViewController(ProductDetailsVC, animated: true)
    }
    
    
    
    @IBAction func BackAction(_ sender: Any) {
           
           self.navigationController?.popViewController(animated: true)
       }
    
    @IBAction func AddCartAction(_ sender: Any) {
        
        let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
        let MyOrderViewController  = storyBoard.instantiateViewController(withIdentifier: "CartListViewController") as! CartListViewController
        self.navigationController?.pushViewController(MyOrderViewController, animated: false)
    }
    @IBAction func NotificationAction(_ sender: Any) {
        let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
        let NotificationViewController  = storyBoard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(NotificationViewController, animated: false)
    }
    @IBAction func closeAction(_ sender: UIButton) {
       // GlobalClass.showAlertDialog(message: "Delete", target: self)
       // create the alert
        let alert = UIAlertController(title: "Delete", message: "Are you sure want to delete \(vals2[sender.tag]["product_name"].stringValue) from your favourite list", preferredStyle: UIAlertController.Style.alert)

               // add the actions (buttons)
              
               alert.addAction(UIAlertAction(title: "NO", style: UIAlertAction.Style.cancel, handler: nil))

               // show the alert
               self.present(alert, animated: true, completion: nil)
        
        
        alert.addAction(UIAlertAction(title: "YES", style: UIAlertAction.Style.default, handler: { action in

            // do something like...
            self.vals2.remove(at: sender.tag)
            UserDefaults.standard.setValue(JSON(self.vals2).rawValue, forKey: "myfavourite")
                         UserDefaults.standard.synchronize()
            self.FavoriteCollectionView.reloadData()
        }))
        
      

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
