//
//  SuggestionViewController.swift
//  Orgafresh
//
//  Copyright © 2020 Innovation Incubator. All rights reserved.
//

import UIKit
import SVProgressHUD
import SwiftyJSON
import Alamofire

class SuggestionViewController: UIViewController {
    @IBOutlet weak var CartButton: SSBadgeButton!
    
    @IBOutlet weak var gradientVwTopConstrain    : NSLayoutConstraint!
    @IBOutlet weak var gradientVwHeightConstrain : NSLayoutConstraint!
    
    @IBOutlet weak var ProductMessage: UITextField!
    @IBOutlet weak var productNmae: UITextField!
    @IBOutlet weak var suggestBackgroundView: NSLayoutConstraint!
    
    @IBOutlet weak var NameLabel: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        if CartCount == "0"
        {
            CartButton.badge = ""
            CartButton.badgeLabel.backgroundColor = UIColor.clear
        }
        else
        {
            CartButton.badge = "\(CartCount)"
        }
        if DeviceType.IS_IPHONE_X || DeviceType.IS_IPHONE_XP{
            
            gradientVwTopConstrain.constant = 0
            gradientVwHeightConstrain.constant = 55
            
        }else{
            
            gradientVwTopConstrain.constant = 0
            gradientVwHeightConstrain.constant = 55
            //    suggestBackgroundView.constant = 120
        }
        
        let leftView1                             = UIView(frame: CGRect(x: 5, y: 0, width: 15, height: 26))
        leftView1.backgroundColor                 = .clear
        NameLabel.leftView                 = leftView1
        NameLabel.leftViewMode             = .always
        NameLabel.contentVerticalAlignment = .center
        
        let leftView2                             = UIView(frame: CGRect(x: 5, y: 0, width: 15, height: 26))
        leftView2.backgroundColor                 = .clear
        productNmae.leftView                 = leftView2
        productNmae.leftViewMode             = .always
        productNmae.contentVerticalAlignment = .center
        
        let leftView3                             = UIView(frame: CGRect(x: 5, y: 0, width: 15, height: 26))
        leftView3.backgroundColor                 = .clear
        ProductMessage.leftView                 = leftView3
        ProductMessage.leftViewMode             = .always
        ProductMessage.contentVerticalAlignment = .center
    }
    
    @IBAction func SendFeedbackAction(_ sender: Any) {
        if productNmae.text!.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty || ProductMessage.text!.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            GlobalClass.showAlertDialog(message: "Please enter all fields.", target: self)
            return
        }
        self.view.endEditing(true)
        
        postsugesstion()
        
        
        
    }
    @IBAction func BackButtonAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func AddCartAction(_ sender: Any) {
        if CartCount == "0"
        {
            GlobalClass.showAlertDialog(message: "You dont have any item on your cart", target: self)
            
        }
        else{
            let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
            let MyOrderViewController  = storyBoard.instantiateViewController(withIdentifier: "CartListViewController") as! CartListViewController
            self.navigationController?.pushViewController(MyOrderViewController, animated: false)
        }
    }
    @IBAction func NotificationAction(_ sender: Any) {
        let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
        let NotificationViewController  = storyBoard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(NotificationViewController, animated: false)
    }
    
    func postsugesstion() {
        SVProgressHUD.show(withStatus: "Sending...")
        
        let parameter: Parameters = ["productname" : productNmae.text!, "productdetails" : ProductMessage.text!]
        
        print("noti params : \(parameter)")
        
        networkProvider.request(.addsugesstion(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                
                let jsonVal = JSON(response.data)
                
                print("Sending val : \(jsonVal)")
                    SVProgressHUD.dismiss()

                    if jsonVal["status"].stringValue == "success"{
                        self.productNmae.text = ""
                        self.ProductMessage.text = ""
                        self.NameLabel.text = ""
                //         GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                        let alert = UIAlertController(title: "", message: jsonVal["message"].stringValue, preferredStyle: UIAlertController.Style.alert)
                        self.present(alert, animated: true, completion: nil)

                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { action in
                            self.navigationController?.popViewController(animated: true)
                        }))
                        
                    }else{
                        
                        GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                    }
                    
                
                
            case .failure( _):
                
                GlobalClass.showAlertDialog(message: "Please try again.", target: self)
                
                SVProgressHUD.dismiss()
                
                break
            }
        })
        
    }
    
    
    
}
