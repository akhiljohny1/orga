//
//  SelectedCategoryViewController.swift
//  Orgafresh
//
//  Copyright © 2020 Innovation Incubator. All rights reserved.
//

import UIKit
import SVProgressHUD
import SwiftyJSON
import Alamofire

class CategoryCell: UICollectionViewCell{
    
    @IBOutlet weak var PriceLabel: UILabel!
    @IBOutlet weak var DescriptionLabel: UILabel!
    
    @IBOutlet weak var GramsLabel: UILabel!
    @IBOutlet weak var sellingPriceLabel: UILabel!
    @IBOutlet weak var favoriteButton: UIButton!
    @IBOutlet weak var availabilityLabel: UILabel!
    @IBOutlet weak var CartImageView: UIImageView!
    
    @IBOutlet weak var addCartButton: UIButton!
    @IBOutlet weak var burView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        favoriteButton.setImage(UIImage(named: "Unliked"), for: .normal)
        favoriteButton.setImage(UIImage(named: "like"), for: .selected)
        addCartButton.layer.cornerRadius = addCartButton.frame.size.height/2
        addCartButton.layer.masksToBounds = true
        
    }
}
class SelectedCategoryViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var CartButton: SSBadgeButton!
    
    @IBOutlet weak var TitleLabel: UILabel!
    var categoryValues                      = JSON()
    var values                              = JSON()
    
    var arr2 = JSON()
    var vals2 = [JSON]()
    var TempCartValues = [JSON]()
    var CartArray = JSON()
    @IBOutlet weak var collectionVBottomConstarint: NSLayoutConstraint!
    
    @IBOutlet weak var gradientVwTopConstrain    : NSLayoutConstraint!
    @IBOutlet weak var gradientVwHeightConstrain : NSLayoutConstraint!
    
    @IBOutlet weak var CategoryCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if DeviceType.IS_IPHONE_X || DeviceType.IS_IPHONE_XP{
            
            gradientVwTopConstrain.constant = 43
            gradientVwHeightConstrain.constant = 55
            
        }else{
            
            gradientVwTopConstrain.constant = 20
            gradientVwHeightConstrain.constant = 55
            //    collectionVBottomConstarint.constant = 260
            
        }
        TitleLabel.text = values["title"].stringValue
        let vals = UserDefaults.standard.value(forKey: "myfavourite") as Any
        let retrivedVals = JSON(vals)
        for i in 0 ..< retrivedVals.count{
            self.vals2.append(retrivedVals[i])
        }
        getHomeCategory2()
    }
    override func viewWillAppear(_ animated: Bool) {
        if CartCount == "0"
        {
            CartButton.badge = ""
            CartButton.badgeLabel.backgroundColor = UIColor.clear
        }
        else
        {
            CartButton.badge = "\(CartCount)"
        }
        reteriveTempData()
        
    }
    
    //MARK: - Collection View
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categoryValues.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let categoryCell = CategoryCollectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCell", for: indexPath) as! CategoryCell
        
        categoryCell.favoriteButton.tag = indexPath.row
        categoryCell.addCartButton.tag = indexPath.row
        
        categoryCell.PriceLabel.text =  "₹" + "\(categoryValues[indexPath.row]["normal_price"].intValue)"
        categoryCell.DescriptionLabel.text =   categoryValues[indexPath.row]["product_name"].stringValue
        categoryCell.sellingPriceLabel.text =  "₹" + "\(categoryValues[indexPath.row]["selling_price"].intValue)"
        categoryCell.GramsLabel.text =  "/" + "\(categoryValues[indexPath.row]["minimum_order_quantity"].intValue)" + " " + categoryValues[indexPath.row]["unit_name"].stringValue
        categoryCell.PriceLabel.attributedText = categoryCell.PriceLabel.text!.strikeThrough()
        
        var str = imageBaseUrl + categoryValues[indexPath.row]["product_img"].stringValue
        str = str.replacingOccurrences(of: " ", with: "%20")
        categoryCell.CartImageView.sd_setImage(with: URL(string: str), placeholderImage: nil)
        
        for i in 0 ..< vals2.count
        {
            if categoryValues[indexPath.row]["product_name"].stringValue == vals2[i]["product_name"].stringValue
            {
                categoryCell.favoriteButton.isSelected = true;
                categoryCell.favoriteButton.setImage(UIImage(named: "like"), for: .selected)
            }
        }
        if categoryValues[indexPath.row]["availability"].intValue == 0
        {
            categoryCell.availabilityLabel.isHidden = false
            categoryCell.burView.isHidden = false
        }
        else{
            categoryCell.availabilityLabel.isHidden = true
            categoryCell.burView.isHidden = true
        }
        return categoryCell
    }
    
    
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let numberOfCellInRow : Int        = 2
        let padding : Int                  = 2
        let collectionCellWidth : CGFloat  = (self.CategoryCollectionView.frame.size.width/CGFloat(numberOfCellInRow)) - CGFloat(padding)
        
        return CGSize(width: collectionCellWidth, height: collectionCellWidth)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if categoryValues[indexPath.row]["availability"].intValue != 0
        {
            let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
            let ProductDetailsVC  = storyBoard.instantiateViewController(withIdentifier: "ProductDetailsViewController") as! ProductDetailsViewController
            
            ProductDetailsVC.productValues = categoryValues[indexPath.row]
            self.navigationController?.pushViewController(ProductDetailsVC, animated: true)
        }
        else{
            GlobalClass.showAlertDialog(message: "Product is out of stock", target: self)
        }
    }
    
    //MARK: - Button Action
    var categoryaddcartIndex = Int()
    @IBAction func AddCartValues(_ sender: UIButton) {
        categoryaddcartIndex = sender.tag
        if categoryValues[categoryaddcartIndex]["availability"].intValue == 0
        {
            GlobalClass.showAlertDialog(message: "Product is out of stock", target: self)
            
        }
        else{
            cartadd()
        }
        
    }
    
    func cartadd()
    {
        
        CartArray["product_name"] = JSON(categoryValues[categoryaddcartIndex]["product_name"].stringValue)
        CartArray["normal_price"] = JSON(categoryValues[categoryaddcartIndex]["normal_price"].stringValue)
        CartArray["selling_price"] = JSON(categoryValues[categoryaddcartIndex]["selling_price"].stringValue)
        CartArray["quantity"] = JSON(1)
        
        CartArray["minimum_order_quantity"] = JSON(categoryValues[categoryaddcartIndex]["minimum_order_quantity"].stringValue)
        CartArray["unit_name"] = JSON(categoryValues[categoryaddcartIndex]["unit_name"].stringValue)
        CartArray["product_img"] = JSON(categoryValues[categoryaddcartIndex]["product_img"].stringValue)
        
        CartArray["id"] = JSON(categoryValues[categoryaddcartIndex]["id"].stringValue)
        var valsVal = -1
        
        for i in 0 ..< self.vals2.count{
            
            let val = self.vals2[i]
            
            if val["id"].stringValue == CartArray["id"].stringValue{
                
                valsVal = i
                
            }
        }
        if valsVal != -1{
            
            self.TempCartValues.remove(at: valsVal)
        }
        
        GlobalClass.showAlertDialog(message: "Your Item has been added to Cart.", target: self)
        
        self.TempCartValues.append(CartArray)
        print(self.TempCartValues)
        UserDefaults.standard.setValue(JSON(self.TempCartValues).rawValue, forKey: "TempCartValues")
        UserDefaults.standard.synchronize()
        CartCount = "\(self.TempCartValues.count)"
        CartButton.badge = "\(self.TempCartValues.count)"
        
    }
    
    func reteriveTempData()
    {
        
        let vals = UserDefaults.standard.value(forKey: "TempCartValues") as Any
        
        let retrivedVals = JSON(vals)
        
        TempCartValues.removeAll()
        
        for i in 0 ..< retrivedVals.count{
            TempCartValues.append(retrivedVals[i])
        }
        if TempCartValues.count == 0
        {
            CartButton.badge = ""
            CartButton.badgeLabel.backgroundColor = UIColor.clear
        }
        else
        {
            CartButton.badge = "\(TempCartValues.count)"
        }
        CartCount = "\(TempCartValues.count)"
        UserDefaults.standard.setValue(JSON("\(TempCartValues.count)").rawValue, forKey: "CartCount")
        UserDefaults.standard.synchronize()
        
    }
    
    @IBAction func BackAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func AddCartAction(_ sender: Any) {
        if CartCount == "0"
        {
            GlobalClass.showAlertDialog(message: "You dont have any item on your cart", target: self)
            
        }
        else{
            let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
            let MyOrderViewController  = storyBoard.instantiateViewController(withIdentifier: "CartListViewController") as! CartListViewController
            self.navigationController?.pushViewController(MyOrderViewController, animated: false)
        }
    }
    @IBAction func NotificationAction(_ sender: Any) {
        let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
        let NotificationViewController  = storyBoard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(NotificationViewController, animated: false)
    }
    
    @IBAction func FavoriteAction(_ sender: UIButton) {
        
        print(sender.tag)
        
        if let favoriteButton = sender as? UIButton {
            if favoriteButton.isSelected {
                // set deselected
                favoriteButton.isSelected = false
                print("not selected")
                // favoriteButton.tintColor = UIColor.lightGray
                
                
                print("categoryValues :", categoryValues[sender.tag])
                
                print("all values :",vals2)
                
                var valsVal = -1
                
                for i in 0 ..< vals2.count{
                    
                    print("vals \(i):", vals2[i])
                    
                    if Int(vals2[i]["id"].stringValue) == categoryValues[sender.tag]["id"].intValue{
                        valsVal = i
                    }
                }
                
                if valsVal != -1{
                    
                    vals2.remove(at: valsVal)
                }
                
                UserDefaults.standard.setValue(JSON(vals2).rawValue, forKey: "myfavourite")
                UserDefaults.standard.synchronize()
                
            } else {
                // set selected
                favoriteButton.isSelected = true
                print("selected")
                arr2["product_name"] = JSON(categoryValues[sender.tag]["product_name"].stringValue)
                arr2["normal_price"] = JSON(categoryValues[sender.tag]["normal_price"].stringValue)
                arr2["selling_price"] = JSON(categoryValues[sender.tag]["selling_price"].stringValue)
                arr2["minimum_order_quantity"] = JSON(categoryValues[sender.tag]["minimum_order_quantity"].stringValue)
                arr2["unit_name"] = JSON(categoryValues[sender.tag]["unit_name"].stringValue)
                arr2["product_img"] = JSON(categoryValues[sender.tag]["product_img"].stringValue)
                arr2["availability"] = JSON(categoryValues[sender.tag]["availability"].intValue)
                arr2["product_desc"] = JSON(categoryValues[sender.tag]["product_desc"].stringValue)
                arr2["id"] = JSON(categoryValues[sender.tag]["id"].stringValue)
                arr2["stock"] = JSON(categoryValues[sender.tag]["stock"].intValue)
                arr2["offer"] = JSON(categoryValues[sender.tag]["offer"].intValue)
                arr2["cat_id"] = JSON(categoryValues[sender.tag]["cat_id"].intValue)
                
                
                
                vals2.append(arr2)
                print(vals2)
                
                UserDefaults.standard.setValue(JSON(vals2).rawValue, forKey: "myfavourite")
                UserDefaults.standard.synchronize()
                
                
                
                
            }
        }
    }
    
    
    func getHomeCategory2() {
        
        DispatchQueue.main.async {
            
            SVProgressHUD.show(withStatus: "Loading...")
        }
        
        networkProvider.request(.gethomecat(id: values["id"].intValue, type: values["type"].intValue), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                
                let jsonVal = JSON(response.data)
                
                print("Json val : \(jsonVal)")
                
                if response.statusCode == 200{
                    
                    if jsonVal["status"].stringValue == "success"{
                        
                        self.categoryValues = jsonVal["product"]
                        
                        if self.categoryValues.count == 0
                            
                        {
                            let alert = UIAlertController(title: "Products", message: "Sorry,There is no product available under \(self.values["title"].stringValue)", preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            
                        }
                        
                    }else{
                        
                        GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                    }
                    
                }else if response.statusCode == 401{
                    
                    GlobalClass.callGetApiKey(viewController: self)
                    GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                    
                }else if jsonVal["message"].stringValue.contains("token"){
                    
                    GlobalClass.callGetApiKey(viewController: self)
                }
                else if response.statusCode == 500{
                    
                    GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                }
                else{
                    
                    GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                }
                
                DispatchQueue.main.async {
                    self.CategoryCollectionView.reloadData()
                }
                SVProgressHUD.dismiss()
                
            case .failure( _):
                
                SVProgressHUD.dismiss()
                
                break
            }
        })
    }
    
    
    
}
