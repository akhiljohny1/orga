//
//  Clean&EarnViewController.swift
//  Orgafresh
//
//  Copyright © 2020 Innovation Incubator. All rights reserved.
//

import UIKit
import SVProgressHUD
import SwiftyJSON
import Alamofire

class CleanCell : UICollectionViewCell{
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var imgVw: UIImageView!
    //    override var isSelected: Bool {
    //        didSet {
    //            if self.isSelected {
    //                backgroundColor = UIColor.systemBlue
    //            }
    //            else {
    //                backgroundColor = UIColor.darkGray
    //            }
    //        }
    //    }
}



class Clean_EarnViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    var imagePicker = UIImagePickerController()
    
    @IBOutlet weak var selectImageButtonTextField: UIButton!
    @IBOutlet weak var hiddenSelectImage: UIImageView!
    
    @IBOutlet weak var EarnButton: UIButton!
    
    @IBOutlet weak var SelectImageButtonField: UIButton!
    @IBOutlet var earnSelectDateTextField: UITextField!
    var earn_pickupdate = ""
    @IBOutlet weak var selectedItemLabel: UILabel!
    @IBOutlet weak var PayButton: UIButton!
    @IBOutlet weak var CartButton: SSBadgeButton!
    
    @IBOutlet weak var CleanCollectionView: UICollectionView!
    
    @IBOutlet weak var gradientVwTopConstrain    : NSLayoutConstraint!
    @IBOutlet weak var gradientVwHeightConstrain : NSLayoutConstraint!
    @IBOutlet weak var SubmitButtonBottomConst: NSLayoutConstraint!
    @IBOutlet weak var PayUIView: UIView!
    @IBOutlet weak var CheckButton: UIButton!
    
    @IBOutlet weak var checkButtontopConstaint: NSLayoutConstraint!
    @IBOutlet weak var AgreeStatTopConstaint: NSLayoutConstraint!
    @IBOutlet weak var ContactNumberTopConstaint: NSLayoutConstraint!
    @IBOutlet weak var AgreeContTopConstaint: NSLayoutConstraint!
    @IBOutlet weak var AgreeContinueBackgroundView: UIView!
    @IBOutlet weak var agreeContinueBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var picker                          : UIDatePicker!
    @IBOutlet weak var dueDatePickerView: UIView!
    @IBOutlet weak var dateTimeTextField: UITextField!
    var RecycleandEarnFlag = "RECYCLE AND EARN"
    @IBOutlet weak var contactTextField: UITextField!
    @IBOutlet weak var detailsTextField: UITextField!
    
    @IBOutlet weak var submitViewTopConstant: NSLayoutConstraint!
    @IBOutlet weak var selectedDateViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var submitTopCalendarView: UIView!
    @IBOutlet weak var SubmitBackgroundView: UIView!
    var itemValues = JSON()
    var selectedItemArray = [Int]()
    var selectedItemNameArray = [String]()
    
    @IBOutlet weak var bottomsubmitconstraint: NSLayoutConstraint!
    override func viewDidLoad() {
        
        super.viewDidLoad()
        imagePicker.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
        
        if DeviceType.IS_IPHONE_X || DeviceType.IS_IPHONE_XP{
         gradientVwTopConstrain.constant = 43
        gradientVwHeightConstrain.constant = 55
            
        }else{
            
          gradientVwTopConstrain.constant = 20
        gradientVwHeightConstrain.constant = 55
            checkButtontopConstaint.constant = 40
            AgreeStatTopConstaint.constant = 30
            ContactNumberTopConstaint.constant = 25
         //   AgreeContTopConstaint.constant = 60
    //        agreeContinueBottomConstraint.constant = 300
//            selectedDateViewTopConstraint.constant = 500
//            submitViewTopConstant.constant = 560
          //  bottomsubmitconstraint.constant = 20
        }
        PayUIView.isHidden = true
        AgreeContinueBackgroundView.isHidden = true
        if CartCount == "0"
        {
            CartButton.badge = ""
            CartButton.badgeLabel.backgroundColor = UIColor.clear
        }
        else
        {
            CartButton.badge = "\(CartCount)"
        }
        CheckButton.isSelected = false
        getCleanEarn()
        dueDatePickerView.isHidden = true
        // picker.minimumDate              = Date()
        CleanCollectionView.allowsMultipleSelection = true
        submitTopCalendarView.isHidden = true
        
        let leftView1                             = UIView(frame: CGRect(x: 5, y: 0, width: 15, height: 26))
        leftView1.backgroundColor                 = .clear
        contactTextField.leftView                 = leftView1
        contactTextField.leftViewMode             = .always
        contactTextField.contentVerticalAlignment = .center
        
        let leftView2                             = UIView(frame: CGRect(x: 5, y: 0, width: 15, height: 26))
        leftView2.backgroundColor                 = .clear
        detailsTextField.leftView                 = leftView2
        detailsTextField.leftViewMode             = .always
        detailsTextField.contentVerticalAlignment = .center
        
        CheckButton.setImage(UIImage(named: "Squarecheck"), for: .normal)
        CheckButton.setImage(UIImage(named: "checkbox"), for: .selected)

    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return itemValues.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cleancell = CleanCollectionView.dequeueReusableCell(withReuseIdentifier: "CleanCell", for: indexPath) as! CleanCell
        cleancell.nameLabel.textColor = UIColor.white
        cleancell.nameLabel.text = itemValues[indexPath.row]["item_name"].stringValue.firstUppercased
        cleancell.imgVw.sd_setImage(with: URL(string: imageBaseUrl + itemValues[indexPath.row]["image"].stringValue), placeholderImage: #imageLiteral(resourceName: "loginVector"))
        
        if selectedItemArray.contains(itemValues[indexPath.row]["id"].intValue){
            
            cleancell.backgroundColor = #colorLiteral(red: 0.1176470588, green: 0.8039215686, blue: 0.937254902, alpha: 1)
            
        }else{
            
            cleancell.backgroundColor = #colorLiteral(red: 0.1490196078, green: 0.1490196078, blue: 0.1490196078, alpha: 1)
            
        }
        
        return cleancell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if selectedItemArray.contains(itemValues[indexPath.row]["id"].intValue){
            
            selectedItemArray.removeFirstElementEqual(to: itemValues[indexPath.row]["id"].intValue)
            selectedItemNameArray.removeFirstElementEqual(to: itemValues[indexPath.row]["item_name"].stringValue)
        }else{
            
            selectedItemArray.append(itemValues[indexPath.row]["id"].intValue)
            selectedItemNameArray.append(itemValues[indexPath.row]["item_name"].stringValue)
            
        }
        if selectedItemArray.count == 0
        {
            submitTopCalendarView.isHidden = true
           earnSelectDateTextField.text = ""
        }
        else{
            submitTopCalendarView.isHidden = false
        }
        
        
        print(selectedItemArray)
        selectedItemLabel.text = "Selected items are " + selectedItemNameArray.joined(separator: ",") //itemValues[indexPath.row]["item_name"].stringValue
        
        collectionView.reloadData()
        
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
    }
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let numberOfCellInRow : Int        = 3
        let padding : Int                  = 3
        let collectionCellWidth : CGFloat  = (self.CleanCollectionView.frame.size.width/CGFloat(numberOfCellInRow)) - CGFloat(padding)
        
        return CGSize(width: collectionCellWidth, height: collectionCellWidth)
    }
    
    
    @IBAction func RecycleandEarnButtonAction(_ sender: Any) {
        
        EarnButton.backgroundColor = #colorLiteral(red: 0.168627451, green: 0.7137254902, blue: 0.4549019608, alpha: 1)
        PayButton.backgroundColor = #colorLiteral(red: 0.6980392157, green: 0.6941176471, blue: 0.6941176471, alpha: 1)
        EarnButton.setTitleColor(.black, for: .normal)
        PayButton.setTitleColor(.white, for: .normal)
        PayUIView.isHidden = true
        AgreeContinueBackgroundView.isHidden = true
        RecycleandEarnFlag = "RECYCLE AND EARN"
        
    }
    
    @IBAction func PayAndRecycleButtonAction(_ sender: Any) {
        
        // PayUIView.isHidden = false
        AgreeContinueBackgroundView.isHidden = false
        submitTopCalendarView.isHidden = true
        dueDatePickerView.isHidden = true
        
        
    }
    @IBAction func SelectImageButtonFieldAction(_ sender: Any) {
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender as! UIView
            alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    func imagePickerController (_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        // check if an image was selected,
        // since a images are not the only media type that can be selected
        print(info)
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            hiddenSelectImage.image = pickedImage
            selectImageButtonTextField.setTitle("  F4693-AB69-486EFCA22B.jpeg", for: .normal)
            selectImageButtonTextField.setTitleColor(.white, for: .normal)
            
        }
        //        userProfileButton.setImage(img as! UIImage, for: .normal)
        //
        //    }
        
        self.dismiss(animated: true, completion: { () -> Void in
            
        })
        
    }
    
    @IBAction func CheckAndUncheckAction(_ sender: Any) {
        if let button = sender as? UIButton {
            if button.isSelected {
                // set deselected
                button.isSelected = false
                
            } else {
                // set selected
                button.isSelected = true
            }
        }
    }
    @IBAction func SubmitAction(_ sender: Any) {
        if RecycleandEarnFlag == "PAY AND RECYCLE"
        {
            Validate()
        }
        else if RecycleandEarnFlag == "RECYCLE AND EARN"
        {
            if selectedItemArray.count != 0
           {
            if earnSelectDateTextField.text!.isEmpty  {
                GlobalClass.showAlertDialog(message: "Select Delivery Date", target: self)
                return
            }
          
            placeclearnearnrequest()
            }
            else
            {
                GlobalClass.showAlertDialog(message: "Please select an item and proceed", target: self)

            }
        }
    }
    
    @IBAction func agreeDismissButtonAction(_ sender: Any) {
        
        PayUIView.isHidden = true
        AgreeContinueBackgroundView.isHidden = true
    }
    
    @IBAction func AgreeAndContinueButtonAction(_ sender: Any) {
        PayButton.backgroundColor = #colorLiteral(red: 0.168627451, green: 0.7137254902, blue: 0.4549019608, alpha: 1)
        EarnButton.backgroundColor = #colorLiteral(red: 0.6980392157, green: 0.6941176471, blue: 0.6941176471, alpha: 1)
        PayButton.setTitleColor(.black, for: .normal)
        EarnButton.setTitleColor(.white, for: .normal)
        RecycleandEarnFlag = "PAY AND RECYCLE"
        
        PayUIView.isHidden = false
        AgreeContinueBackgroundView.isHidden = true
        
    }
    //MARK: - Picker Action
    @IBAction func onValueChangeTime(_ picker : UIDatePicker) {
        
        //   print("Picker :", convertDateAndTime(dat: picker.date))
        
        let str = convertDateAndTime(dat: picker.date)
        if RecycleandEarnFlag == "RECYCLE AND EARN"
        {
            earnSelectDateTextField.text = "    " + str.components(separatedBy: "at").first! + " " + str.components(separatedBy: "at").last!
            let earn_pickupdate_str = convertDateAndTime2(dat: picker.date)
            
            earn_pickupdate = earn_pickupdate_str.components(separatedBy: "at").first!
        }
        else if RecycleandEarnFlag == "PAY AND RECYCLE"{
            dateTimeTextField.text = "    " + str.components(separatedBy: "at").first! + " " + str.components(separatedBy: "at").last!
            
            let earn_pickupdate_str = convertDateAndTime2(dat: picker.date)
            
            earn_pickupdate = earn_pickupdate_str.components(separatedBy: "at").first!
        }
    }
    @IBAction func earnButtonDatePickerShow(_ sender: UIButton) {
        dueDatePickerView.isHidden = false
        RecycleandEarnFlag = "RECYCLE AND EARN"
    }
    
    @IBAction func showPickerVw(_ sender : UIButton) {
        
        dueDatePickerView.isHidden = false
        RecycleandEarnFlag = "PAY AND RECYCLE"
        
    }
    
    
    @IBAction func hidePickerVw(_ sender : UIButton) {
        
        dueDatePickerView.isHidden = true
    }
    //MARK: - Convert Date
    
    func convertDateAndTime(dat : Date) -> String {
        
        let dateFormatter          = DateFormatter()
        dateFormatter.dateFormat   = "yyyy-MM-dd HH:mm:ss +0000"
        dateFormatter.dateStyle    = .long
        dateFormatter.timeStyle    = .short
        let val                    = dateFormatter.string(from: dat)
        return val
    }
    
    func convertDateAndTime2(dat : Date) -> String {
        
        let dateFormatter          = DateFormatter()
        dateFormatter.dateFormat   = "yyyy-MM-dd HH:mm:ss +0000"
        dateFormatter.dateStyle    = .long
        dateFormatter.timeStyle    = .none
        
        let dateFormat2            = DateFormatter()
        dateFormat2.dateFormat     = "yyyy-MM-dd"
        
        let val                    = dateFormat2.string(from: dat)
        return val
    }
    
    
    @IBAction func AddCartAction(_ sender: Any) {
        if CartCount == "0"
        {
            GlobalClass.showAlertDialog(message: "You dont have any item on your cart", target: self)
        }
        else{
            let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
            let MyOrderViewController  = storyBoard.instantiateViewController(withIdentifier: "CartListViewController") as! CartListViewController
            self.navigationController?.pushViewController(MyOrderViewController, animated: false)
        }
    }
    @IBAction func NotificationAction(_ sender: Any) {
        let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
        let NotificationViewController  = storyBoard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(NotificationViewController, animated: false)
    }
    @IBAction func BackButtonAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func getCleanEarn() {
        
        DispatchQueue.main.async {
            
            SVProgressHUD.show(withStatus: "Loading...")
        }
                
        networkProvider.request(.getcleanearn, completion: {(result) in
            
            switch result {
                
            case .success(let response):
                                
                let jsonVal = JSON(response.data)
                
                print("Json val : \(jsonVal)")
                
                print(jsonVal["status"].stringValue)
                
                if response.statusCode == 200{
                    
                    if jsonVal["status"].stringValue == "success"{
                        
                        self.itemValues = jsonVal["items"]
                        SVProgressHUD.dismiss()

                    }else{
                        
                        GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                    }
                    
                }else if response.statusCode == 401{
                    
                    GlobalClass.callGetApiKey(viewController: self)
                    GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                    
                }else if jsonVal["message"].stringValue.contains("token"){
                    
                    GlobalClass.callGetApiKey(viewController: self)
                }
                else if response.statusCode == 500{
                    
                    GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                }
                else{
                    
                    GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                }
                
                
                self.CleanCollectionView.reloadData()
                
            case .failure( _):
                
                SVProgressHUD.dismiss()
                
                break
            }
        })
    }
    func placeclearnearnrequest(){
        
          SVProgressHUD.show(withStatus: "Loading...")
        
        let parameter: Parameters = ["itemId" : selectedItemArray , "pickupdate" : earn_pickupdate , "type" : "0" , "address" : ""]
        print("noti params : \(parameter)")
        
        networkProvider.request(.placeclearnearnrequest(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                let jsonVal = JSON(response.data)
                
                print("Json val : \(jsonVal)")
                
                print(jsonVal["status"].stringValue)
                
                if jsonVal["status"].stringValue == "success"{
                    self.selectedItemArray.removeAll()
                    self.selectedItemNameArray.removeAll()
                    self.submitTopCalendarView.isHidden = true
                    self.selectedItemLabel.text = ""
                    self.earnSelectDateTextField.text = ""
                    self.earn_pickupdate = ""
                    SVProgressHUD.dismiss()
                    GlobalClass.showAlertDialog(message: "Your request were sent successfully. Our team will reach you soon", target: self)
                    self.CleanCollectionView.reloadData()
                }
                
            case .failure( _):
                
                break
            }
        })
    }
    
    func Validate(){
        
        //  self.view.endEditing(true)
        if contactTextField.text!.isEmpty || detailsTextField.text!.isEmpty || dateTimeTextField.text!.isEmpty {
            GlobalClass.showAlertDialog(message: "All fields are mandatory", target: self)
            return
        }
        if contactTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||  detailsTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines) == "" || dateTimeTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            GlobalClass.showAlertDialog(message: "All fields are mandatory", target: self)
            return
        }
        if !GlobalClass.validatePHnumber(enteredNumber: contactTextField.text!){
            
            GlobalClass.showAlertDialog(message: "Please enter valid phone number.", target: self)
            return
        }
        if CheckButton.isSelected == false
        {
            GlobalClass.showAlertDialog(message: "Please Agree with our Terms and Conditions.", target: self)
            
            return
        }
        
        uploadImagewithParameter()
        
    }
    
    func uploadImagewithParameter()
    {
        SVProgressHUD.show(withStatus: "Loading...")
        let parameter: Parameters = ["address" : contactTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines) , "desc" : detailsTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines) , "pickupdate" : earn_pickupdate]
        print("noti params : \(parameter)")
        
        
        let urlAlert = URL(string: "http://test.orgafresh.online/api/adduserimage")!
        let token = UserDefaults.standard.string(forKey: "access_token") ?? "not saved"
        
        let headerS: HTTPHeaders = ["content-type":"application/json",
                                    "Accept" : "application/json","Authorization" : token]
        
        AF.upload(
            multipartFormData: { multipartFormData in
                multipartFormData.append(self.hiddenSelectImage.image!.jpegData(compressionQuality: 0.5)!, withName: "image" , fileName: "image.png", mimeType: "image/png")
                for (key, value) in parameter {
                    multipartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key)
                }
        },
            to: urlAlert, method: .post , headers: headerS)
            .response { resp in
                print(resp)
        }
        .uploadProgress(queue: .main, closure: { progress in
            //Current upload progress of file
            print("Upload Progress: \(progress.fractionCompleted)")
            SVProgressHUD.show(withStatus: "Upload..\(progress.fractionCompleted)")
        })
            .responseJSON(completionHandler: { data in
                print(data.result)
                self.selectImageButtonTextField.setTitle("", for: .normal)
                SVProgressHUD.dismiss()
              //  GlobalClass.showAlertDialog(message: "User Image Added Successfully", target: self)
                self.earn_pickupdate = ""
                self.contactTextField.text = ""
                self.detailsTextField.text = ""
                self.selectImageButtonTextField.setTitle("    Select Image", for: .normal)
                self.CheckButton.isSelected = false
                self.selectImageButtonTextField.setTitleColor(.lightGray, for: .normal)
                self.dateTimeTextField.text = ""
//                GlobalClass.showAlertDialog(message: "Your request were sent successfully. Our team will reach you soon", target: self)
                
                let json = JSON(data.data!)
                
                print("json : ", json)
                
                if json["status"].stringValue == "success"{
                    
                    GlobalClass.showAlertDialog(message: "Your request were sent successfully. Our team will reach you soon", target: self)
                    
                }else{
                    
                    GlobalClass.showAlertDialog(message: json["message"].stringValue, target: self)
                }

            })
        
    }
    
}
