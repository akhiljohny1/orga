

import UIKit
import SwiftyJSON
import SDWebImage
import SVProgressHUD
import Alamofire
import FBSDKLoginKit


class MyProfileViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    @IBOutlet weak var userImage: UIImageView!
    var imagePicker = UIImagePickerController()
    
    @IBOutlet weak var gradientVwTopConstrain    : NSLayoutConstraint!
    
    @IBOutlet weak var moblieButton: UIButton!
    @IBOutlet weak var emailButton: UIButton!
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var moblieTextField: UITextField!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var enterTextField: UITextField!
    
    @IBOutlet weak var dismissButton: UIButton!
    @IBOutlet weak var updateView: UIView!
    
    @IBOutlet var userProfileButton: UIButton!
    var userProfileValues = JSON()
    
    var updateFlag = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userImage.layer.masksToBounds = false
        userImage.layer.borderColor = UIColor.white.cgColor
        userImage.layer.cornerRadius = userImage.frame.size.width / 2
        userImage.clipsToBounds = true
        
        imagePicker.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
        if DeviceType.IS_IPHONE_X || DeviceType.IS_IPHONE_XP{
            
            gradientVwTopConstrain.constant = 0
            
        }else{
            
            gradientVwTopConstrain.constant = 0
        }
        getuserprofile()
        
        updateView.isHidden = true
        dismissButton.isHidden = true
        if appDelegate.loginEmailAddress != ""
        {
            emailTextField.text = "  " + appDelegate.loginEmailAddress
            emailButton.isHidden = true
        }
        if appDelegate.loginMobileNumber != ""
        {
            moblieTextField.text = "  " + appDelegate.loginMobileNumber
            moblieButton.isHidden = true
        }
        nameTextField.text = "  " + appDelegate.UserName
        if AccessToken.current != nil{
            self.userImage.sd_setImage(with: URL(string: appDelegate.profilePicture), placeholderImage: #imageLiteral(resourceName: "Avatar"))
        }
        else
        {
        }
    }
    override func viewWillAppear(_ animated: Bool) {
    }
    @IBAction func logoutAction(_ sender: Any) {
        let alert = UIAlertController(title: "Logout", message: "Are you sure want to logout", preferredStyle: UIAlertController.Style.alert)
        
        // add the actions (buttons)
        
        alert.addAction(UIAlertAction(title: "CANCEL", style: UIAlertAction.Style.cancel, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
        
        
        alert.addAction(UIAlertAction(title: "LOGOUT", style: UIAlertAction.Style.default, handler: { action in
            
            let loginManager = LoginManager()
            if let _ = AccessToken.current {
                loginManager.logOut()
            }
            appDelegate.isLogined = false
            appDelegate.UserName = ""
            appDelegate.loginMobileNumber = ""
            appDelegate.loginEmailAddress = ""
            UserDefaults.standard.set("false1", forKey: "isUserLogined")
            UserDefaults.standard.synchronize()
            
        }))
    }
    @IBAction func nameAddAction(_ sender: UIButton) {
        descriptionLabel.text = "Please enter Name to be updated"
        enterTextField.placeholder = "  New Name"
        updateView.isHidden = false
        dismissButton.isHidden = false
        updateFlag = "Name"
        enterTextField.text = nameTextField.text ?? ""
        
    }
    
    @IBAction func emailAddAction(_ sender: UIButton) {
        descriptionLabel.text = "Please enter Email to be updated"
        enterTextField.placeholder = "  Email Address"
        updateView.isHidden = false
        dismissButton.isHidden = false
        updateFlag = "Email"
        enterTextField.text = emailTextField.text ?? ""
        
    }
    @IBAction func phoneAddAction(_ sender: UIButton) {
        descriptionLabel.text = "Please enter Phone Number to be updated"
        enterTextField.placeholder = "  Moblie Number"
        updateView.isHidden = false
        dismissButton.isHidden = false
        updateFlag = "Phone"
        enterTextField.text = moblieTextField.text ?? ""
        
    }
    @IBAction func UpdateAction(_ sender: UIButton) {
        
        
        
        if updateFlag == "Name"
        {
            if enterTextField.text!.count > 2{
                nameTextField.text = "  " + (enterTextField.text ?? "")
                appDelegate.UserName = enterTextField.text ?? ""
                updateView.isHidden = true
                dismissButton.isHidden = true
                editname()
            }
        }
        else if updateFlag == "Email"
        {
            if !GlobalClass.validateEmail(enteredEmail: enterTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)){
                
                GlobalClass.showAlertDialog(message: "Please enter valid email address.", target: self)
                return
            }
            emailTextField.text = "  " + (enterTextField.text ?? "")
            appDelegate.loginEmailAddress = enterTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            updateView.isHidden = true
            dismissButton.isHidden = true
            editemail()
        }
        else if updateFlag == "Phone"
        {
            if !GlobalClass.validatePHnumber(enteredNumber: enterTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)){
                
                GlobalClass.showAlertDialog(message: "Please enter valid Moblie Number.", target: self)
                return
            }
            moblieTextField.text = "  " + enterTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            appDelegate.loginMobileNumber = enterTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            updateView.isHidden = true
            dismissButton.isHidden = true
            editphonenumber()
        }
        enterTextField.text = ""
        
    }
    @IBAction func updateDismissAction(_ sender: Any) {
        updateView.isHidden = true
        dismissButton.isHidden = true
        enterTextField.text = ""
    }
    
    @IBAction func imagePickButtonAction(_ sender: Any) {
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender as! UIView
            alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    func imagePickerController (_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        // check if an image was selected,
        // since a images are not the only media type that can be selected
        print(info)
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            userImage.contentMode = .scaleToFill
            userImage.image = pickedImage
            uploadimage()
        }
        
        
        self.dismiss(animated: true, completion: { () -> Void in
            
        })
        
    }
    
    //MARK: - Image Upload
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    // func requestWith(imageData: Data?, onError: ((Error?) -> Void)? = nil){
    
    
    @IBAction func BackAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    func editemail(){
        
        self.view.endEditing(true)
        
        SVProgressHUD.show(withStatus: "Loading...")
        
        let parameter: Parameters = ["Token" : appDelegate.access_token , "email" : appDelegate.loginEmailAddress]
        print("noti params : \(parameter)")
        
        networkProvider.request(.editemail(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                SVProgressHUD.dismiss()
                
                let jsonVal = JSON(response.data)
                
                print("Json val : \(jsonVal)")
                
                print(jsonVal["status"].stringValue)
                
                if jsonVal["status"].stringValue == "success"{
                    self.emailButton.isHidden = true
                    
                    GlobalClass.showAlertDialog(message: "Your email has been successfully updated", target: self)
                    
                }
                
            case .failure( _):
                SVProgressHUD.dismiss()
                
                break
            }
        })
    }
    func editname(){
        
        SVProgressHUD.show(withStatus: "Loading...")
        
        let parameter: Parameters = ["Token" : appDelegate.access_token , "name" : appDelegate.UserName]
        print("noti params : \(parameter)")
        
        networkProvider.request(.editname(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                SVProgressHUD.dismiss()
                
                let jsonVal = JSON(response.data)
                
                print("Json val : \(jsonVal)")
                
                print(jsonVal["status"].stringValue)
                
                if jsonVal["status"].stringValue == "success"{
                    GlobalClass.showAlertDialog(message: "Your name has been successfully updated", target: self)
                    
                }
                
            case .failure( _):
                SVProgressHUD.dismiss()
                
                break
            }
        })
    }
    func editphonenumber() {
        
        SVProgressHUD.show(withStatus: "Loading...")
        
        let parameter: Parameters = ["Token" : appDelegate.access_token ,"phone_number" : appDelegate.loginMobileNumber]
        print("editphonenumber params : \(parameter)")
        
        networkProvider.request(.editphonenumber(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                let jsonVal = JSON(response.data)
                
                SVProgressHUD.dismiss()
                
                print("Json val : \(jsonVal)")
                
                print(jsonVal["status"].stringValue)
                
                if jsonVal["status"].stringValue == "success"{
                    self.moblieButton.isHidden = true
                    
                    GlobalClass.showAlertDialog(message: "Your phone number has been successfully updated", target: self)
                    
                }
                
            case .failure( _):
                SVProgressHUD.dismiss()
                
                break
            }
        })
    }
    
    //MARK: - User Profile
    
    func getuserprofile() {
        
        SVProgressHUD.show(withStatus: "Loading...")
        
        let parameter: Parameters = ["Token" : appDelegate.access_token]
        print("noti params : \(parameter)")
        
        networkProvider.request(.getuserprofile(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                SVProgressHUD.dismiss()
                
                let jsonVal = JSON(response.data)
                
                print("Json val : \(jsonVal)")
                
                print(jsonVal["status"].stringValue)
                
                if jsonVal["status"].stringValue == "success"{
                    
                    self.userProfileValues = jsonVal["userprofile"]
                    
                    appDelegate.UserName = self.userProfileValues["name"].stringValue
                    appDelegate.loginEmailAddress = self.userProfileValues["email"].stringValue
                    appDelegate.loginMobileNumber = self.userProfileValues["phone_number"].stringValue
                    
                    self.nameTextField.text = "  " + self.userProfileValues["name"].stringValue
                    self.emailTextField.text = "  " + self.userProfileValues["email"].stringValue
                    self.moblieTextField.text = "  " + self.userProfileValues["phone_number"].stringValue
                    if self.userProfileValues["image"].stringValue.contains("http://") {
                        print("exists")
                        self.userImage.sd_setImage(with: URL(string: self.userProfileValues["image"].stringValue), placeholderImage: #imageLiteral(resourceName: "Avatar"))
                    }
                    else
                    {
                        self.userImage.sd_setImage(with: URL(string: imageBaseUrl + self.userProfileValues["image"].stringValue), placeholderImage: #imageLiteral(resourceName: "Avatar"))
                    }
                    
                    if self.userProfileValues["phone_number"].stringValue.isEmpty
                    {
                        self.moblieButton.isHidden = false
                        
                        return
                    }
                    else
                    {
                        appDelegate.loginMobileNumber = self.userProfileValues["phone_number"].stringValue
                        
                        self.moblieButton.isHidden = true
                    }
                    if self.userProfileValues["email"].stringValue.isEmpty
                    {
                        self.emailButton.isHidden = false
                        
                        return
                    }
                    else
                    {
                        appDelegate.loginEmailAddress = self.userProfileValues["email"].stringValue
                        self.emailButton.isHidden = true
                    }
                    if appDelegate.loginEmailAddress.isEmpty || appDelegate.loginMobileNumber.isEmpty
                    {
                        GlobalClass.showAlertDialog(message: "We couldnt find your Email on our records. Update as soon as possible for better communication", target: self)
                        
                    }
                    if AccessToken.current != nil{
                        self.userImage.sd_setImage(with: URL(string: appDelegate.profilePicture), placeholderImage: #imageLiteral(resourceName: "Avatar"))
                        self.nameTextField.text = "  " + appDelegate.UserName

                        if appDelegate.loginEmailAddress != ""
                              {
                                self.emailTextField.text = "  " + appDelegate.loginEmailAddress
                                self.emailButton.isHidden = true
                              }
                              if appDelegate.loginMobileNumber != ""
                              {
                                self.moblieTextField.text = "  " + appDelegate.loginMobileNumber
                                self.moblieButton.isHidden = true
                              }
                    }
                }
                
            case .failure( _):
                SVProgressHUD.dismiss()
                
                break
            }
        })
    }
    
    
    func uploadimage()
    {
        SVProgressHUD.show(withStatus: "Loading...")
        let urlAlert = URL(string: "http://test.orgafresh.online/api/adduserimage")!
        let token = UserDefaults.standard.string(forKey: "access_token") ?? "not saved"
        
        let headerS: HTTPHeaders = ["content-type":"application/json",
                                    "Accept" : "application/json","Authorization" : token]
        let resizedImage = userImage.image!.resizedTo1MB()
        
        AF.upload(
            multipartFormData: { multipartFormData in
                multipartFormData.append(resizedImage!.jpegData(compressionQuality: 0.5)!, withName: "image" , fileName: "image.png", mimeType: "image/png")
        },
            to: urlAlert, method: .post , headers: headerS)
            .response { resp in
                print(resp)
        }
        .uploadProgress(queue: .main, closure: { progress in
            //Current upload progress of file
            print("Upload Progress: \(progress.fractionCompleted)")
        })
            .responseJSON(completionHandler: { data in
                SVProgressHUD.dismiss()
                let json = JSON(data.data!)
                print("json : ", json)
                
                if json["status"].stringValue == "success"{
                    
                    GlobalClass.showAlertDialog(message: "User Image Uploaded Successfully", target: self)
                    
                }else{
                    
                    GlobalClass.showAlertDialog(message: json["message"].stringValue, target: self)
                }
                
            })
    }
}

extension UIImage {
    
    func resized(withPercentage percentage: CGFloat) -> UIImage? {
        let canvasSize = CGSize(width: size.width * percentage, height: size.height * percentage)
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
        defer { UIGraphicsEndImageContext() }
        draw(in: CGRect(origin: .zero, size: canvasSize))
        return UIGraphicsGetImageFromCurrentImageContext()
    }
    
    func resizedTo1MB() -> UIImage? {
        guard let imageData = self.pngData() else { return nil }
        
        var resizingImage = self
        var imageSizeKB = Double(imageData.count) / 1000.0 // ! Or devide for 1024 if you need KB but not kB
        
        while imageSizeKB > 1000 { // ! Or use 1024 if you need KB but not kB
            guard let resizedImage = resizingImage.resized(withPercentage: 0.5),
                let imageData = resizedImage.pngData()
                else { return nil }
            
            resizingImage = resizedImage
            imageSizeKB = Double(imageData.count) / 1500.0 // ! Or devide for 1024 if you need KB but not kB
        }
        
        return resizingImage
    }
}
