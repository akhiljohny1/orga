//
//  MyFavouriteViewController.swift
//  Orgafresh
//
//  Copyright © 2020 Innovation Incubator. All rights reserved.
//

import UIKit
import SwiftyJSON
import  Alamofire
import SVProgressHUD

class myBalanceCell: UITableViewCell{
    
    @IBOutlet weak var DescriptionLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet var walletImage: UIImageView!
    
    
}
class myBalanceViewController: UIViewController, UITableViewDelegate,UITableViewDataSource {
  
    
    @IBOutlet weak var CartButton: SSBadgeButton!
    
    
    @IBOutlet var myBalanceTableView: UITableView!
    
    
    @IBOutlet weak var gradientVwTopConstrain    : NSLayoutConstraint!
    @IBOutlet weak var gradientVwHeightConstrain : NSLayoutConstraint!
    var walletValues = JSON()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if CartCount == "0"
                      {
                          CartButton.badge = ""
                          CartButton.badgeLabel.backgroundColor = UIColor.clear
                      }
                      else
                      {
                CartButton.badge = "\(CartCount)"
                }
        if DeviceType.IS_IPHONE_X || DeviceType.IS_IPHONE_XP{
            
            gradientVwTopConstrain.constant = 0
            gradientVwHeightConstrain.constant = 60
            
        }else{
            
            gradientVwTopConstrain.constant = 0
            gradientVwHeightConstrain.constant = 60
        }
        getwallet()
      }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
                  return 3

      }
      
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let myBalancecell = myBalanceTableView.dequeueReusableCell(withIdentifier: "myBalanceCell", for: indexPath) as! myBalanceCell
                 myBalancecell.amountLabel.text = "0" //self.walletValues[indexPath.row]["amount"].stringValue

                 if indexPath.row == 0
                 {
                    myBalancecell.walletImage.image = #imageLiteral(resourceName: "purse")
                    myBalancecell.DescriptionLabel.text = "Total Balance"

                 }
                 else if indexPath.row == 1
                        {
                           myBalancecell.DescriptionLabel.text = "Referal Bonus"
                         myBalancecell.walletImage.image = #imageLiteral(resourceName: "internet")
                        }
                 else if indexPath.row == 2
                 {
                    myBalancecell.DescriptionLabel.text = "Clean And Earn Earnings"
                     myBalancecell.walletImage.image = #imageLiteral(resourceName: "dollar")

                 }
                 return myBalancecell
      }
 
    
    @IBAction func BackAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func AddCartAction(_ sender: Any) {
        if CartCount == "0"
                  {
                   GlobalClass.showAlertDialog(message: "You dont have any item on your cart", target: self)

               }
               else{
        let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
        let MyOrderViewController  = storyBoard.instantiateViewController(withIdentifier: "CartListViewController") as! CartListViewController
        self.navigationController?.pushViewController(MyOrderViewController, animated: false)
        }
    }
    @IBAction func NotificationAction(_ sender: Any) {
        let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
        let NotificationViewController  = storyBoard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(NotificationViewController, animated: false)
    }
    
    //MARK:- Func
    func getwallet(){
        
        SVProgressHUD.show(withStatus: "Loading...")
        
        networkProvider.request(.getwallet, completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                SVProgressHUD.dismiss()
                
                let jsonVal = JSON(response.data)
                
                print("Json val : \(jsonVal)")
                
                print(jsonVal["status"].stringValue)
                
                if response.statusCode == 200{
                    
                    if jsonVal["status"].stringValue == "success"{
                        
                        self.walletValues = jsonVal["wallet"]
                        
                    }else{
                        
                        GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                    }
                    
                }else if response.statusCode == 401{
                    
                    GlobalClass.callGetApiKey(viewController: self)
                    GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                    
                }else if jsonVal["message"].stringValue.contains("token"){
                    
                    GlobalClass.callGetApiKey(viewController: self)
                }
                else if response.statusCode == 500{
                    
                    GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                }
                else{
                    
                    GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                }
                
                
                self.myBalanceTableView.reloadData()
                
            case .failure( _):
                
                SVProgressHUD.dismiss()
                
                break
            }
        })
    }
    
    }
    
    

