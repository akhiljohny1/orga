//
//  PrivacyPolicyViewController.swift
//  Orgafresh
//
//  Created by Innovation Incubator on 16/11/20.
//  Copyright © 2020 Innovation Incubator. All rights reserved.
//

import UIKit

class PrivacyPolicyViewController: UIViewController {
    
    @IBOutlet weak var txtVw                     : UITextView!
    
    @IBOutlet weak var gradientVwTopConstrain    : NSLayoutConstraint!
    @IBOutlet weak var gradientVwHeightConstrain : NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if DeviceType.IS_IPHONE_X || DeviceType.IS_IPHONE_XP{
            
            gradientVwTopConstrain.constant = 0
            gradientVwHeightConstrain.constant = 55
            
        }else{
            
            gradientVwTopConstrain.constant = 0
            gradientVwHeightConstrain.constant = 55
        }

        // Do any additional setup after loading the view.
    }
    
    @IBAction func backButton(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
