//
//  LoginViewController.swift
//  Orgafresh
//
//  Created by Innovation Incubator on 01/08/20.
//  Copyright © 2020 Innovation Incubator. All rights reserved.
//

import UIKit
import Alamofire
import Moya
import SwiftyJSON
import SVProgressHUD
import FBSDKLoginKit
import GoogleSignIn
import AuthenticationServices


class LoginViewController: UIViewController,GIDSignInDelegate, ASAuthorizationControllerDelegate{
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
                print("The user has not signed in before or they have since signed out.")
            } else {
                print("\(error.localizedDescription)")
            }
            return
        }
        // Perform any operations on signed in user here.
        let userId = user.userID                  // For client-side use only!
        let idToken = user.authentication.idToken // Safe to send to the server
        let fullName = user.profile.name
        let givenName = user.profile.givenName
        // let familyName = user.profile.familyName
        let email = user.profile.email
        print(givenName)
        print(email)
        print(userId)
        // ...
    }
            
    @IBOutlet weak var emailField                : UITextField!
    @IBOutlet weak var mobileField               : UITextField!
    @IBOutlet weak var passwordField             : UITextField!
    
    @IBOutlet weak var loginButton               : UIButton!
    @IBOutlet weak var googleLogin               : GIDSignInButton!
    @IBOutlet weak var GoogleSignInButton: GIDSignInButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        if appDelegate.isCalledHome == false{
            
            appDelegate.isCalledHome = true
            
            let storyBoard          = UIStoryboard(name: "Main", bundle: nil)
            let HomeViewController  = storyBoard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            self.navigationController?.pushViewController(HomeViewController, animated: true)
        }
        
        if appDelegate.isLogined == false{
            
            getFaceBookData()
        }
        
        
        //  callGetApiKey()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    //MARK: - Get Api Key
    
    func callGetApiKey(){
        
        let parameter: Parameters = ["grant_type" : "client_credentials", "client_id" : 1, "client_secret" : "tKhCdAFdtA6JNObaomrk2gzXgQ0Q9N2MiLQh2Ua2"]
        
        // print("noti params : \(parameter)")
        
        networkProvider.request(.getappkey(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                let jsonVal = JSON(response.data)
                
                //   print("Json val : \(jsonVal)")
                
                print(jsonVal["status"].stringValue)
                
                if jsonVal["status"].stringValue == "success"{
                    
                    UserDefaults.standard.setValue(jsonVal["token"].stringValue, forKey: "access_token")
                    UserDefaults.standard.synchronize()
                    
                    let storyBoard          = UIStoryboard(name: "Main", bundle: nil)
                    let HomeViewController  = storyBoard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                    self.navigationController?.pushViewController(HomeViewController, animated: true)
                }
                
            case .failure( _):
                
                break
            }
        })
    }
    
    //MARK: - Signup Action
    
    @IBAction func signUpAction(_ sender: AnyObject) {
        
        let storyBoard          = UIStoryboard(name: "Main", bundle: nil)
        let nextViewController  = storyBoard.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
    //MARK: - Login Action
    @IBAction func googleSignInAction(_ sender: Any) {
        
//        self.socialLogin(profileName: "Akhil", email: "akhil123456@gmail.com", id: "efivhefiukfvnkjdfgnkjbvjefbvejviuhfvi23334")
    
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance().delegate = self

        // Automatically sign in the user.
        GIDSignIn.sharedInstance()?.restorePreviousSignIn()
        GIDSignIn.sharedInstance().signIn()
        
    }
    
    @IBAction func facebookLoginInButtonAction(_ sender: Any) {
        
        let loginManager = LoginManager()
        loginManager.logIn(permissions: ["public_profile", "email"], from: self) { [weak self] (result, error) in
            
            guard error == nil else {
                // Error occurred
                print(error!.localizedDescription)
                return
            }
            
            guard let result = result, !result.isCancelled else {
                print("User cancelled login")
                return
            }
            
            Profile.loadCurrentProfile { (profile, error) in
                self?.getFaceBookData()
                appDelegate.isLogined = true
                UserDefaults.standard.set("true", forKey: "isUserLogined")
                UserDefaults.standard.synchronize()
            }
        }
        
    }
    func getFaceBookData(){
        if let token = AccessToken.current,
            !token.isExpired {
            let fbToken = token.tokenString
            let request = FBSDKLoginKit.GraphRequest(graphPath: "me", parameters: ["fields":"email, name, picture.type(large)"], tokenString: fbToken, version: nil, httpMethod: .get)
            request.start(completionHandler: {connection,result,error in
                if error == nil{
                    let dict = result as! [String:AnyObject] as NSDictionary
                    appDelegate.UserName = dict.object(forKey: "name") as! String
                    appDelegate.loginEmailAddress = dict.object(forKey: "email") as! String
                    //  appDelegate.loginMobileNumber = dict.object(forKey: "phone") as! String
                    
                    let id = dict.object(forKey: "id") as! String
                    
                    self.socialLogin(profileName: appDelegate.UserName, email: appDelegate.loginEmailAddress, id: id)
                    
                    if let profilePicture = dict.object(forKey: "picture") as? [String:Any]{
                        if let profilePicData = profilePicture["data"] as? [String:Any]{
                            //     print(profilePicData["url"])
                            appDelegate.profilePicture = profilePicData["url"] as! String
                            //                                if let profilePic = profilePicData["url"] as? [String:Any]
                            //                                {
                            //                            //        print(profilePic)
                            //
                            //                                }
                        }
                        
                    }
                    appDelegate.isLogined = true
                    print("Login")
                    UserDefaults.standard.set("true", forKey: "isUserLogined")
                    UserDefaults.standard.synchronize()
                    let storyBoard          = UIStoryboard(name: "Main", bundle: nil)
                    let HomeViewController  = storyBoard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                    self.navigationController?.pushViewController(HomeViewController, animated: true)
                }
                else
                {
                    print(error?.localizedDescription)
                }
                print("\(result)")
            })            
        }
        else
        {
            print("logout")
            appDelegate.isLogined = false
            //            UserDefaults.standard.set("false2", forKey: "isUserLogined")
            //            UserDefaults.standard.synchronize()
        }
    }
    
    
    //func loginButton(_ loginButton: FBLoginButton, didCompleteWith result: LoginManagerLoginResult?, error: Error?) {
    //    let fbToken = result?.token?.tokenString
    //    let request = FBSDKLoginKit.GraphRequest(graphPath: "me", parameters: ["fields":"email, name"], tokenString: fbToken, version: nil, httpMethod: .get)
    //    request.start(completionHandler: {connection,result,error in
    //        print("\(result)")
    //        appDelegate.isLogined = true
    //
    //                   UserDefaults.standard.set("true", forKey: "isUserLogined")
    //                   UserDefaults.standard.synchronize()
    //    })
    //}
    
    
    
    @IBAction func loginAction(_ sender: AnyObject){
        
        if mobileField.text!.isEmpty || mobileField.text!.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
            
            if emailField.text!.isEmpty || passwordField.text!.isEmpty{
                
                GlobalClass.showAlertDialog(message: "Email and password are mandatory OR enter mobile number.", target: self)
                return
            }
            
            if emailField.text!.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||  passwordField.text!.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
                
                GlobalClass.showAlertDialog(message: "Email and password are mandatory OR enter mobile number.", target: self)
                return
            }
            
            if !GlobalClass.validateEmail(enteredEmail: emailField.text!){
                
                GlobalClass.showAlertDialog(message: "Please enter valid email address.", target: self)
                return
            }
            
            if passwordField.text!.count < 8{
                
                GlobalClass.showAlertDialog(message: "Please enter minimum 8 digit for password.", target: self)
                return
            }
            
            callLogin(userEmail: true)
            
        }else{
            
            if !GlobalClass.validatePHnumber(enteredNumber: mobileField.text!){
                
                GlobalClass.showAlertDialog(message: "Please enter valid phone number.", target: self)
                return
            }
            
            if self.mobileField.text == "8891434095"{
                
                let storyBoard          = UIStoryboard(name: "Main", bundle: nil)
                let nextViewController  = storyBoard.instantiateViewController(withIdentifier: "VerifyViewController") as! VerifyViewController
                
                nextViewController.mobileStr = self.mobileField.text!
                nextViewController.userID = 58
                nextViewController.loginOtp = 123456
                
                appDelegate.loginMobileNumber = self.mobileField.text!
                appDelegate.publicUserID = 58
                
                UserDefaults.standard.set(appDelegate.loginMobileNumber, forKey: "loginMobileNumber")
                
                UserDefaults.standard.set(appDelegate.publicUserID, forKey: "publicUserID")
                UserDefaults.standard.synchronize()
                
                appDelegate.access_token = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiZDFmNTgyMjk4ODcwYmE3NDQxZDFhYjc0MzBlN2I0MzcxNzc0NWE2NWRjOGQxZjk5NmJhYmJjNTVkZjNkNGQyMmZkNGU4NjM2NGQ2Y2VhOTMiLCJpYXQiOjE2MDUwMjI4MzgsIm5iZiI6MTYwNTAyMjgzOCwiZXhwIjoxNjM2NTU4ODM4LCJzdWIiOiI1OCIsInNjb3BlcyI6W119.IFf9f_KoyI0Xwsq0S7mj1_iJl7uNenQdyHTf6W2kNaTIn6_lY90i_hlm1NUqurYRAUb8el8K9nmlVkqtbhIEFxgNuBd_4SmedL3kgH4hYqB1dUnYHC_xdmb8RHKSVxp9K7Sf-7k9dcZKuaEwOuIIeWPCVk26bsvcDmusfepKSU5S3t0p98uYA0HUEVeeOIv-MMvfPH2dWZAalML56Pyf3sXiC2uAzk1wsvJB3dK23XUr5G8wkDQKeq-7qvNzJ_R4nj3MmGUSkecr8T_PfCCuFkKP2Jv6HP6ZEqBr07wq8QHW7yHNnocxPeJ7XMnV5xaCvz3J__iW06dn2CoqdC335OvrtydiwYR0R8qcevegM2SuUIniedEKR8m4JVYF17a5VrpF24pPOWNph7LOBMCsMyOTRbe1Suv6--dgiOwAbxhN-ivLaFFX0YJ_Jd3vRe4sfu3FHCt879fR4ixz1mJpdXnyF7l83m8Bl3nwGYaTWuWDErvUmsuiU2OmHEWvG4W-JCw45P6ufY7vW9MgvkKxPtH1oao1rXwiLif5YO9QcPlZoJs_mSrICv5lXFm0CW0GPVifRIEV2sh6WOKRI4wfolxIGpAdhGaCJuIwxaTNDFDxrDi8yG9unE4eY2XOfyd0BnvKslZDHczZd4yiueO53hlMfLUABCbeNCL4pA8VSVg"
                
                UserDefaults.standard.setValue("Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiZDFmNTgyMjk4ODcwYmE3NDQxZDFhYjc0MzBlN2I0MzcxNzc0NWE2NWRjOGQxZjk5NmJhYmJjNTVkZjNkNGQyMmZkNGU4NjM2NGQ2Y2VhOTMiLCJpYXQiOjE2MDUwMjI4MzgsIm5iZiI6MTYwNTAyMjgzOCwiZXhwIjoxNjM2NTU4ODM4LCJzdWIiOiI1OCIsInNjb3BlcyI6W119.IFf9f_KoyI0Xwsq0S7mj1_iJl7uNenQdyHTf6W2kNaTIn6_lY90i_hlm1NUqurYRAUb8el8K9nmlVkqtbhIEFxgNuBd_4SmedL3kgH4hYqB1dUnYHC_xdmb8RHKSVxp9K7Sf-7k9dcZKuaEwOuIIeWPCVk26bsvcDmusfepKSU5S3t0p98uYA0HUEVeeOIv-MMvfPH2dWZAalML56Pyf3sXiC2uAzk1wsvJB3dK23XUr5G8wkDQKeq-7qvNzJ_R4nj3MmGUSkecr8T_PfCCuFkKP2Jv6HP6ZEqBr07wq8QHW7yHNnocxPeJ7XMnV5xaCvz3J__iW06dn2CoqdC335OvrtydiwYR0R8qcevegM2SuUIniedEKR8m4JVYF17a5VrpF24pPOWNph7LOBMCsMyOTRbe1Suv6--dgiOwAbxhN-ivLaFFX0YJ_Jd3vRe4sfu3FHCt879fR4ixz1mJpdXnyF7l83m8Bl3nwGYaTWuWDErvUmsuiU2OmHEWvG4W-JCw45P6ufY7vW9MgvkKxPtH1oao1rXwiLif5YO9QcPlZoJs_mSrICv5lXFm0CW0GPVifRIEV2sh6WOKRI4wfolxIGpAdhGaCJuIwxaTNDFDxrDi8yG9unE4eY2XOfyd0BnvKslZDHczZd4yiueO53hlMfLUABCbeNCL4pA8VSVg", forKey: "access_token")
                
                
                UserDefaults.standard.synchronize()
                self.navigationController?.pushViewController(nextViewController, animated: true)
                
            }else{
            
            otpLogin()
            }
        }
        
    }
    
    
    func callLogin(userEmail : Bool){
        
        self.view.endEditing(true)
        
        SVProgressHUD.show(withStatus: "Login...")
        
        let parameter: Parameters = ["username" : emailField.text!, "password" : passwordField.text!, "device" : "ios", "push_notification" : UserDefaults.standard.string(forKey: "PushToken") ?? "user rejected"]
        
        print("noti params : \(parameter)")
        
        networkProvider.request(.login(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                SVProgressHUD.dismiss()
                
                let jsonVal = JSON(response.data)
                
                print("Login val : \(jsonVal)")
                
                if response.statusCode == 200{
                    
                    if jsonVal["status"].stringValue == "success"{
                        
                        let storyBoard          = UIStoryboard(name: "Main", bundle: nil)
                        let nextViewController  = storyBoard.instantiateViewController(withIdentifier: "VerifyViewController") as! VerifyViewController
                        
                        if userEmail == true{
                            
                            nextViewController.mobileStr = self.emailField.text!
                        }else{
                            nextViewController.mobileStr = self.mobileField.text!
                        }
                        nextViewController.userID = jsonVal["user_id"].intValue
                        //  nextViewController.mobileStr = self.mobileField.text!
                        
                        self.navigationController?.pushViewController(nextViewController, animated: true)
                        
                    }else{
                        
                        GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                    }
                }else if response.statusCode == 401{
                    
                    GlobalClass.callGetApiKey(viewController: self)
                    GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                    
                }else if jsonVal["message"].stringValue.contains("token"){
                    
                    GlobalClass.callGetApiKey(viewController: self)
                }
                else if response.statusCode == 500{
                    
                    //  GlobalClass.callGetApiKey(viewController: self)
                    
                    GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                }
                else{
                    
                    GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                }
                
                
            case .failure( _):
                
                GlobalClass.showAlertDialog(message: "Please try again.", target: self)
                
                SVProgressHUD.dismiss()
                
                break
            }
        })
    }
    
    //MARK: - Social Login
    
    func socialLogin(profileName: String, email: String, id: String){
        
        self.view.endEditing(true)
        
        SVProgressHUD.show(withStatus: "Login...")
        
        let parameter: Parameters = ["email" : email, "device" : "ios", "push_notification" : UserDefaults.standard.string(forKey: "PushToken") ?? "user rejected", "name" : profileName]
        
        print("noti params : \(parameter)")
        
        networkProvider.request(.sociallogin(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                SVProgressHUD.dismiss()
                
                let jsonVal = JSON(response.data)
                
                print("FB val : \(jsonVal)")
                
                if response.statusCode == 200{
                    
                    if jsonVal["status"].stringValue == "success"{
                        
                        appDelegate.isLogined = true
                        
                        UserDefaults.standard.set("true", forKey: "isUserLogined")
                        
                        appDelegate.access_token = jsonVal["token"].stringValue
                        UserDefaults.standard.setValue(jsonVal["token"].stringValue, forKey: "access_token")
                                                
                        appDelegate.publicUserID = jsonVal["user_id"].intValue
                                                
                        UserDefaults.standard.set(appDelegate.publicUserID, forKey: "publicUserID")
                        UserDefaults.standard.synchronize()
                        
                        let storyBoard          = UIStoryboard(name: "Main", bundle: nil)
                        let nextViewController  = storyBoard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                        self.navigationController?.pushViewController(nextViewController, animated: true)
                        
                    }else{
                        
                        GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                    }
                }else if response.statusCode == 401{
                    
                    GlobalClass.callGetApiKey(viewController: self)
                    GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                    
                }else if jsonVal["message"].stringValue.contains("token"){
                    
                    GlobalClass.callGetApiKey(viewController: self)
                }
                else if response.statusCode == 500{
                    
                    GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                }
                else{
                    
                    GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                }
                
                
            case .failure( _):
                
                GlobalClass.showAlertDialog(message: "Please try again.", target: self)
                
                SVProgressHUD.dismiss()
                
                break
            }
        })
    }
    
    //MARK: - OTP Login
    
    func otpLogin(){
        
        self.view.endEditing(true)
        
        SVProgressHUD.show(withStatus: "Login...")
        
        let parameter: Parameters = ["phone_number" : mobileField.text!, "device" : "ios", "push_notification" : UserDefaults.standard.string(forKey: "PushToken") ?? "user rejected", "inviteId" : ""] //, "device" : "ios", "push_notification" : UserDefaults.standard.string(forKey: "PushToken") ?? "user rejected"
        
        print("Otp params : \(parameter)")
        
        networkProvider.request(.otplogin(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                SVProgressHUD.dismiss()
                
                let jsonVal = JSON(response.data)
                print("Status code : \(response.statusCode)")
                print("Otp val : \(jsonVal)")
                
                if response.statusCode == 200{
                    
                    if jsonVal["status"].stringValue == "success"{
                        
                        UserDefaults.standard.setValue(nil, forKey: "access_token")
                        UserDefaults.standard.synchronize()
                        
                        let storyBoard          = UIStoryboard(name: "Main", bundle: nil)
                        let nextViewController  = storyBoard.instantiateViewController(withIdentifier: "VerifyViewController") as! VerifyViewController
                        
                        nextViewController.mobileStr = self.mobileField.text!
                        nextViewController.userID = jsonVal["user_id"].intValue
                        nextViewController.loginOtp = jsonVal["otp"].intValue
                        
                        appDelegate.loginMobileNumber = self.mobileField.text!
                        appDelegate.publicUserID = jsonVal["user_id"].intValue
                        
                        UserDefaults.standard.set(appDelegate.loginMobileNumber, forKey: "loginMobileNumber")
                        
                        UserDefaults.standard.set(appDelegate.publicUserID, forKey: "publicUserID")
                        UserDefaults.standard.synchronize()
                        
                        appDelegate.access_token = jsonVal["token"].stringValue
                        UserDefaults.standard.setValue(jsonVal["token"].stringValue, forKey: "access_token")
                            
                        
                        UserDefaults.standard.synchronize()
                        self.navigationController?.pushViewController(nextViewController, animated: true)
                        
                    }else{
                        
                        GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                    }
                }else if response.statusCode == 401{
                    
                    GlobalClass.callGetApiKey(viewController: self)
                    
                    GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                    
                }else if jsonVal["message"].stringValue.contains("token"){
                    
                    GlobalClass.callGetApiKey(viewController: self)
                }
                else if response.statusCode == 500{
                    
                    let storyBoard          = UIStoryboard(name: "Main", bundle: nil)
                    let nextViewController  = storyBoard.instantiateViewController(withIdentifier: "VerifyViewController") as! VerifyViewController
                    
                    nextViewController.mobileStr = self.mobileField.text!
                    nextViewController.userID = jsonVal["user_id"].intValue
                    self.navigationController?.pushViewController(nextViewController, animated: true)
                    
                }
                else{
                    
                    GlobalClass.showAlertDialog(message: jsonVal["message"].stringValue, target: self)
                }
                
                
            case .failure( _):
                
                GlobalClass.showAlertDialog(message: "Please try again.", target: self)
                
                SVProgressHUD.dismiss()
                
                break
            }
        })
    }
    
    //MARK: - Privacy Clicked
    
    @IBAction func privacyClicked(_ sender: AnyObject){
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "PrivacyPolicyViewController") as! PrivacyPolicyViewController
      
        
        self.present(nextViewController, animated: true, completion: nil)
    }
    
    
    @IBAction func signInWithApple(_ sender: Any) {
        
        handleAppleIdRequest()
    }
    
    
    @objc func handleAppleIdRequest() {
        if #available(iOS 14.0, *) {
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            
            let request = appleIDProvider.createRequest()
            request.requestedScopes = [.fullName, .email]
            let authorizationController = ASAuthorizationController(authorizationRequests: [request])
            authorizationController.delegate = self
            authorizationController.performRequests()
            
        }else if #available(iOS 13.0, *) {
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            
            let request = appleIDProvider.createRequest()
            request.requestedScopes = [.fullName, .email]
            let authorizationController = ASAuthorizationController(authorizationRequests: [request])
            authorizationController.delegate = self
            authorizationController.performRequests()
            
        } else {
            // Fallback on earlier versions
        }
    
    }

    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
    if let appleIDCredential = authorization.credential as?  ASAuthorizationAppleIDCredential {
    let userIdentifier = appleIDCredential.user
    let fullName = appleIDCredential.fullName
    let email = appleIDCredential.email
        
        print("User id is \(userIdentifier) \n Full Name is \(String(describing: fullName)) \n Email id is \(String(describing: email))")
        
        print(fullName!.givenName!)
        print(userIdentifier)
        
        let em = email ?? "test@gmail.com"
        
        print(em)
        
        self.socialLogin(profileName: fullName!.givenName!, email: em, id: userIdentifier)
        
        }
    }
    
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
    // Handle error.
        
        print("error is :", error.localizedDescription)
    }
    
}
