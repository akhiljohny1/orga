//
//  NotificattionTableViewCell.swift
//  Orgafresh
//
//  Copyright © 2020 Innovation Incubator. All rights reserved.
//

import UIKit

class NotificattionTableViewCell: UITableViewCell {

    @IBOutlet weak var orderContentLabel: UILabel!
    @IBOutlet weak var orderHeadingLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
