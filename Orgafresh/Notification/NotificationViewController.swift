//
//  NotificationViewController.swift
//  Orgafresh
//
//  Copyright © 2020 Innovation Incubator. All rights reserved.
//

import UIKit
import SwiftyJSON

class NotificationViewController: UIViewController, UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var NotificationTableView: UITableView!
    @IBOutlet weak var CartButton: SSBadgeButton!
    
    @IBOutlet weak var gradientVwTopConstrain    : NSLayoutConstraint!
    @IBOutlet weak var gradientVwHeightConstrain : NSLayoutConstraint!
    
    var resultJson = JSON()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if CartCount == "0"
        {
            CartButton.badge = ""
            CartButton.badgeLabel.backgroundColor = UIColor.clear
        }
        else
        {
            CartButton.badge = "\(CartCount)"
        }
        if DeviceType.IS_IPHONE_X || DeviceType.IS_IPHONE_XP{
            
            gradientVwTopConstrain.constant = 0
            gradientVwHeightConstrain.constant = 55
            
        }else{
            
            gradientVwTopConstrain.constant = 0
            gradientVwHeightConstrain.constant = 55
        }
        
        if UserDefaults.standard.value(forKey: "notiArray") != nil{
            
            resultJson = JSON(UserDefaults.standard.value(forKey: "notiArray") as! NSArray)
        }
        
        print("result is : ", resultJson)
        
        NotificationTableView.delegate = self
        NotificationTableView.dataSource = self
        NotificationTableView.estimatedRowHeight = 80
        NotificationTableView.rowHeight = UITableView.automaticDimension
        // Do any additional setup after loading the view.
    }
    
    //MARK: - Table View
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return resultJson.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let notificationCell:NotificattionTableViewCell = NotificationTableView.dequeueReusableCell(withIdentifier: "cell") as! NotificattionTableViewCell
        
        notificationCell.orderHeadingLabel.text = resultJson[indexPath.row]["title"].stringValue
        
        notificationCell.orderContentLabel.text = resultJson[indexPath.row]["body"].stringValue
            
     /*   // set the text from the data model
        notificationCell.orderHeadingLabel.text = "Order Placed Sucessfully"

        if indexPath.row == 2 || indexPath.row == 4 || indexPath.row == 8{

            notificationCell.orderContentLabel.text = "Dear Anu, A notification is a message that iphone displays outside your apps UI to provide the user with reminders communication from other people."
        }else{

            notificationCell.orderContentLabel.text = "Dear Anu, A notification is a message that iphone displays outside your apps UI to provide the user with reminders communication from other people. bglkjbnklnjbjkfgnjkbnfgnbklfgnklbnrlnvnvnrvnnvibehtviubnivbntvbtubvuibuvbiuvbiubviuebwuivbewruvbeiruwbviuewbvuybewruivbuerwbvuyerwbiueruivbrewuivbueirwbvuyebviubewruivberwiubviurebviuebuvbeirubvuerbvubvbiuvbtubvyiuetbvuiebtvuibtuvbwuetbvuwetbvubwuvbwuivbiuwbviuwbuivbwiubviuwbviubwuivbwubvuwbviubwiuvbetuivbiuetbviuwbetiuvbituebviubviubetwiuvbetuivbutwbvutwbuvbtuvbtubvutbvubtuvbtuvbtubvutbub"

        }
        */
        
        return notificationCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
        let MyOrderVC  = storyBoard.instantiateViewController(withIdentifier: "MyOrderViewController") as! MyOrderViewController
        self.navigationController?.pushViewController(MyOrderVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    @IBAction func backButton(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func AddCartAction(_ sender: Any) {
        if CartCount == "0"
        {
            GlobalClass.showAlertDialog(message: "You dont have any item on your cart", target: self)
            
        }
        else{
            let storyBoard          = UIStoryboard(name: "Second", bundle: nil)
            let MyOrderViewController  = storyBoard.instantiateViewController(withIdentifier: "CartListViewController") as! CartListViewController
            self.navigationController?.pushViewController(MyOrderViewController, animated: false)
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
